/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

// huffman_unittest.cpp : Defines the entry point for the console application.
// C4996
#pragma warning( disable : 4996 )

#include <math.h>

#include "CppUnitTest.h"
#include "huffmanCode.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JpegEncoder_Tests
{
	TEST_CLASS(huffman_tests){
		public:
			TEST_METHOD_INITIALIZE(BEGIN){
				
			}

			TEST_METHOD(huffman_char_generate){
				
				std::string s = std::string("this is an example for huffman encoding");

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(s.c_str(), s.size(), frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies, false, false);
				auto chars = huffmanCode::createHuffmanPair(code);

                Assert::AreEqual( std::string("110"),	std::string(chars[0].second.begin(),	chars[0].second.end()), L"' ' falsch");
                Assert::AreEqual( std::string("1001"),	std::string(chars[1].second.begin(),	chars[1].second.end()), L"'a' falsch");
				Assert::AreEqual( std::string("101010"),std::string(chars[2].second.begin(),	chars[2].second.end()), L"'c' falsch");
                Assert::AreEqual( std::string("10001"), std::string(chars[3].second.begin(),	chars[3].second.end()), L"'d' falsch");
				Assert::AreEqual( std::string("1111"),	std::string(chars[4].second.begin(),	chars[4].second.end()), L"'e' falsch"); 
				Assert::AreEqual( std::string("1011"),	std::string(chars[5].second.begin(),	chars[5].second.end()), L"'f' falsch"); 
				Assert::AreEqual( std::string("101011"),std::string(chars[6].second.begin(),	chars[6].second.end()), L"'g' falsch"); 
				Assert::AreEqual( std::string("0101"),	std::string(chars[7].second.begin(),	chars[7].second.end()), L"'h' falsch"); 
				Assert::AreEqual( std::string("1110"),	std::string(chars[8].second.begin(),	chars[8].second.end()), L"'i' falsch"); 
				Assert::AreEqual( std::string("01110"), std::string(chars[9].second.begin(),	chars[9].second.end()), L"'l' falsch"); 
				Assert::AreEqual( std::string("0011"),	std::string(chars[10].second.begin(),	chars[10].second.end()), L"'m' falsch"); 
				Assert::AreEqual( std::string("000"),	std::string(chars[11].second.begin(),	chars[11].second.end()), L"'n' falsch"); 
				Assert::AreEqual( std::string("0010"),	std::string(chars[12].second.begin(),	chars[12].second.end()), L"'o' falsch"); 
				Assert::AreEqual( std::string("01000"), std::string(chars[13].second.begin(),	chars[13].second.end()), L"'p' falsch"); 
				Assert::AreEqual( std::string("01001"), std::string(chars[14].second.begin(),	chars[14].second.end()), L"'r' falsch"); 
				Assert::AreEqual( std::string("0110"),	std::string(chars[15].second.begin(),	chars[15].second.end()), L"'s' falsch"); 
				Assert::AreEqual( std::string("01111"), std::string(chars[16].second.begin(),	chars[16].second.end()), L"'t' falsch"); 
				Assert::AreEqual( std::string("10100"), std::string(chars[17].second.begin(),	chars[17].second.end()), L"'u' falsch"); 
				Assert::AreEqual( std::string("10000"), std::string(chars[18].second.begin(),	chars[18].second.end()), L"'x' falsch");
			}

            TEST_METHOD(huffman_grow_right){
				std::string s = std::string("this is an example for huffman encoding");

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(s.c_str(), s.size(), frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies, true, false);
				auto chars = huffmanCode::createHuffmanPair(code);

                Assert::AreEqual( std::string("001"),	std::string(chars[0].second.begin(),	chars[0].second.end()), L"' ' falsch");
                Assert::AreEqual( std::string("1011"),	std::string(chars[1].second.begin(),	chars[1].second.end()), L"'a' falsch");
				Assert::AreEqual( std::string("111110"),std::string(chars[2].second.begin(),	chars[2].second.end()), L"'c' falsch");
                Assert::AreEqual( std::string("11101"), std::string(chars[3].second.begin(),	chars[3].second.end()), L"'d' falsch");
				Assert::AreEqual( std::string("0110"),	std::string(chars[4].second.begin(),	chars[4].second.end()), L"'e' falsch"); 
				Assert::AreEqual( std::string("1001"),	std::string(chars[5].second.begin(),	chars[5].second.end()), L"'f' falsch"); 
				Assert::AreEqual( std::string("111111"),std::string(chars[6].second.begin(),	chars[6].second.end()), L"'g' falsch"); 
				Assert::AreEqual( std::string("0111"),	std::string(chars[7].second.begin(),	chars[7].second.end()), L"'h' falsch"); 
				Assert::AreEqual( std::string("1010"),	std::string(chars[8].second.begin(),	chars[8].second.end()), L"'i' falsch"); 
				Assert::AreEqual( std::string("11010"), std::string(chars[9].second.begin(),	chars[9].second.end()), L"'l' falsch"); 
				Assert::AreEqual( std::string("0101"),	std::string(chars[10].second.begin(),	chars[10].second.end()), L"'m' falsch"); 
				Assert::AreEqual( std::string("000"),	std::string(chars[11].second.begin(),	chars[11].second.end()), L"'n' falsch"); 
				Assert::AreEqual( std::string("0100"),	std::string(chars[12].second.begin(),	chars[12].second.end()), L"'o' falsch"); 
				Assert::AreEqual( std::string("11000"), std::string(chars[13].second.begin(),	chars[13].second.end()), L"'p' falsch"); 
				Assert::AreEqual( std::string("11001"), std::string(chars[14].second.begin(),	chars[14].second.end()), L"'r' falsch"); 
				Assert::AreEqual( std::string("1000"),	std::string(chars[15].second.begin(),	chars[15].second.end()), L"'s' falsch"); 
				Assert::AreEqual( std::string("11011"), std::string(chars[16].second.begin(),	chars[16].second.end()), L"'t' falsch"); 
				Assert::AreEqual( std::string("11110"), std::string(chars[17].second.begin(),	chars[17].second.end()), L"'u' falsch"); 
				Assert::AreEqual( std::string("11100"), std::string(chars[18].second.begin(),	chars[18].second.end()), L"'x' falsch");
			}


			TEST_METHOD(huffman_int_generate){
				int length = 100;
				int values[100] = { 0 };

				// Creates some values (0 - 4) several times
				for (int i = 0; i < length; i++){
					values[i] = i % 5;
				}

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(values, length, frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies);
				auto chars = huffmanCode::createHuffmanPair(code);

                Assert::AreEqual( std::string("110"), std::string(chars[0].second.begin(),chars[0].second.end()), L"0. falsch");
                Assert::AreEqual( std::string("10"), std::string(chars[1].second.begin(),chars[1].second.end()), L"1. falsch");
                Assert::AreEqual( std::string("001"), std::string(chars[2].second.begin(),chars[2].second.end()), L"2. falsch");
                Assert::AreEqual( std::string("01"), std::string(chars[3].second.begin(),chars[3].second.end()), L"3. falsch");
				Assert::AreEqual( std::string("000"), std::string(chars[4].second.begin(),chars[4].second.end()), L"4. falsch");
			}

			TEST_METHOD(huffman_limit_5){

				std::string s = std::string("this is an example for huffman encoding");

				int max_depth = 5;

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(s.c_str(), s.size(), frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies, false, false, max_depth);
				auto chars = huffmanCode::createHuffmanPair(code);

				Assert::AreEqual(19, (int)chars.size());

				int longest = 0;
				for (auto it = code.begin(); it != code.end(); ++it){
					longest = std::max(longest, (int)it->second.size());
				}

				Assert::AreEqual(max_depth, longest);

				Assert::AreEqual(std::string("110"), std::string(chars[0].second.begin(), chars[0].second.end()), L"' ' falsch");
				Assert::AreEqual(std::string("10011"), std::string(chars[1].second.begin(), chars[1].second.end()), L"'a' falsch");
				Assert::AreEqual(std::string("10010"), std::string(chars[2].second.begin(), chars[2].second.end()), L"'c' falsch");
				Assert::AreEqual(std::string("10001"), std::string(chars[3].second.begin(), chars[3].second.end()), L"'d' falsch");
				Assert::AreEqual(std::string("1111"), std::string(chars[4].second.begin(), chars[4].second.end()), L"'e' falsch");
				Assert::AreEqual(std::string("1011"), std::string(chars[5].second.begin(), chars[5].second.end()), L"'f' falsch");
				Assert::AreEqual(std::string("10101"), std::string(chars[6].second.begin(), chars[6].second.end()), L"'g' falsch");
				Assert::AreEqual(std::string("0101"), std::string(chars[7].second.begin(), chars[7].second.end()), L"'h' falsch");
				Assert::AreEqual(std::string("1110"), std::string(chars[8].second.begin(), chars[8].second.end()), L"'i' falsch");
				Assert::AreEqual(std::string("01110"), std::string(chars[9].second.begin(), chars[9].second.end()), L"'l' falsch");
				Assert::AreEqual(std::string("0011"), std::string(chars[10].second.begin(), chars[10].second.end()), L"'m' falsch");
				Assert::AreEqual(std::string("000"), std::string(chars[11].second.begin(), chars[11].second.end()), L"'n' falsch");
				Assert::AreEqual(std::string("0010"), std::string(chars[12].second.begin(), chars[12].second.end()), L"'o' falsch");
				Assert::AreEqual(std::string("01000"), std::string(chars[13].second.begin(), chars[13].second.end()), L"'p' falsch");
				Assert::AreEqual(std::string("01001"), std::string(chars[14].second.begin(), chars[14].second.end()), L"'r' falsch");
				Assert::AreEqual(std::string("0110"), std::string(chars[15].second.begin(), chars[15].second.end()), L"'s' falsch");
				Assert::AreEqual(std::string("01111"), std::string(chars[16].second.begin(), chars[16].second.end()), L"'t' falsch");
				Assert::AreEqual(std::string("10100"), std::string(chars[17].second.begin(), chars[17].second.end()), L"'u' falsch");
				Assert::AreEqual(std::string("10000"), std::string(chars[18].second.begin(), chars[18].second.end()), L"'x' falsch");

			}


			TEST_METHOD(huffman_limit_6){

				std::string s = std::string("this is an example for huffman encoding");


				int max_depth = 6;

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(s.c_str(), s.size(), frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies, false, false, max_depth);
				auto chars = huffmanCode::createHuffmanPair(code);

				Assert::AreEqual(19, (int)chars.size());

				int longest = 0;
				for (auto it = code.begin(); it != code.end(); ++it){
					longest = std::max(longest, (int)it->second.size());
				}

				Assert::AreEqual(max_depth, longest);

				Assert::AreEqual(std::string("110"), std::string(chars[0].second.begin(), chars[0].second.end()), L"' ' falsch");
				Assert::AreEqual(std::string("1001"), std::string(chars[1].second.begin(), chars[1].second.end()), L"'a' falsch");
				Assert::AreEqual(std::string("101010"), std::string(chars[2].second.begin(), chars[2].second.end()), L"'c' falsch");
				Assert::AreEqual(std::string("10001"), std::string(chars[3].second.begin(), chars[3].second.end()), L"'d' falsch");
				Assert::AreEqual(std::string("1111"), std::string(chars[4].second.begin(), chars[4].second.end()), L"'e' falsch");
				Assert::AreEqual(std::string("1011"), std::string(chars[5].second.begin(), chars[5].second.end()), L"'f' falsch");
				Assert::AreEqual(std::string("101011"), std::string(chars[6].second.begin(), chars[6].second.end()), L"'g' falsch");
				Assert::AreEqual(std::string("0101"), std::string(chars[7].second.begin(), chars[7].second.end()), L"'h' falsch");
				Assert::AreEqual(std::string("1110"), std::string(chars[8].second.begin(), chars[8].second.end()), L"'i' falsch");
				Assert::AreEqual(std::string("01110"), std::string(chars[9].second.begin(), chars[9].second.end()), L"'l' falsch");
				Assert::AreEqual(std::string("0011"), std::string(chars[10].second.begin(), chars[10].second.end()), L"'m' falsch");
				Assert::AreEqual(std::string("000"), std::string(chars[11].second.begin(), chars[11].second.end()), L"'n' falsch");
				Assert::AreEqual(std::string("0010"), std::string(chars[12].second.begin(), chars[12].second.end()), L"'o' falsch");
				Assert::AreEqual(std::string("01000"), std::string(chars[13].second.begin(), chars[13].second.end()), L"'p' falsch");
				Assert::AreEqual(std::string("01001"), std::string(chars[14].second.begin(), chars[14].second.end()), L"'r' falsch");
				Assert::AreEqual(std::string("0110"), std::string(chars[15].second.begin(), chars[15].second.end()), L"'s' falsch");
				Assert::AreEqual(std::string("01111"), std::string(chars[16].second.begin(), chars[16].second.end()), L"'t' falsch");
				Assert::AreEqual(std::string("10100"), std::string(chars[17].second.begin(), chars[17].second.end()), L"'u' falsch");
				Assert::AreEqual(std::string("10000"), std::string(chars[18].second.begin(), chars[18].second.end()), L"'x' falsch");

			}


			TEST_METHOD(huffman_limit_6_gr_e1){

				std::string s = std::string("this is an example for huffman encoding");


				int max_depth = 6;

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(s.c_str(), s.size(), frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies, true, true, max_depth);
				auto chars = huffmanCode::createHuffmanPair(code);

				Assert::AreEqual(19, (int)chars.size());

				int longest = 0;
				for (auto it = code.begin(); it != code.end(); ++it){
					longest = std::max(longest, (int)it->second.size());
				}

				Assert::AreEqual(max_depth, longest);

				Assert::AreEqual(std::string("001"), std::string(chars[0].second.begin(), chars[0].second.end()), L"' ' falsch");
				Assert::AreEqual(std::string("1011"), std::string(chars[1].second.begin(), chars[1].second.end()), L"'a' falsch");
				Assert::AreEqual(std::string("111110"), std::string(chars[2].second.begin(), chars[2].second.end()), L"'c' falsch");
				Assert::AreEqual(std::string("11101"), std::string(chars[3].second.begin(), chars[3].second.end()), L"'d' falsch");
				Assert::AreEqual(std::string("0110"), std::string(chars[4].second.begin(), chars[4].second.end()), L"'e' falsch");
				Assert::AreEqual(std::string("1001"), std::string(chars[5].second.begin(), chars[5].second.end()), L"'f' falsch");
				Assert::AreEqual(std::string("110001"), std::string(chars[6].second.begin(), chars[6].second.end()), L"'g' falsch");
				Assert::AreEqual(std::string("0111"), std::string(chars[7].second.begin(), chars[7].second.end()), L"'h' falsch");
				Assert::AreEqual(std::string("1010"), std::string(chars[8].second.begin(), chars[8].second.end()), L"'i' falsch");
				Assert::AreEqual(std::string("11010"), std::string(chars[9].second.begin(), chars[9].second.end()), L"'l' falsch");
				Assert::AreEqual(std::string("0101"), std::string(chars[10].second.begin(), chars[10].second.end()), L"'m' falsch");
				Assert::AreEqual(std::string("000"), std::string(chars[11].second.begin(), chars[11].second.end()), L"'n' falsch");
				Assert::AreEqual(std::string("0100"), std::string(chars[12].second.begin(), chars[12].second.end()), L"'o' falsch");
				Assert::AreEqual(std::string("110000"), std::string(chars[13].second.begin(), chars[13].second.end()), L"'p' falsch");
				Assert::AreEqual(std::string("11001"), std::string(chars[14].second.begin(), chars[14].second.end()), L"'r' falsch");
				Assert::AreEqual(std::string("1000"), std::string(chars[15].second.begin(), chars[15].second.end()), L"'s' falsch");
				Assert::AreEqual(std::string("11011"), std::string(chars[16].second.begin(), chars[16].second.end()), L"'t' falsch");
				Assert::AreEqual(std::string("11110"), std::string(chars[17].second.begin(), chars[17].second.end()), L"'u' falsch");
				Assert::AreEqual(std::string("11100"), std::string(chars[18].second.begin(), chars[18].second.end()), L"'x' falsch");

			}


			TEST_METHOD(huffman_limit_5_more_values){

				std::string s = std::string("this is an example for huffman encoding, abcdefghijklmnopqrstuvwxyz1234");


				int max_depth = 5;

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(s.c_str(), s.size(), frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies, false, false, max_depth);
				auto chars = huffmanCode::createHuffmanPair(code);

				Assert::AreEqual(32, (int)chars.size());

				int longest = 0;
				for (auto it = code.begin(); it != code.end(); ++it){
					longest = std::max(longest, (int)it->second.size());
				}

				Assert::AreEqual(max_depth, longest);
			}

			TEST_METHOD(huffman_limit_6_even_more_values){

				std::string s = std::string("this is an example for huffman encoding , ABCDEFGHIJKLMNOPQRSTUVWX abcdefghijklmopqrstuvwxyz1234567890");


				int max_depth = 6;

				std::map<int, int> frequencies;
				huffmanCode::generateFrequencies(s.c_str(), s.size(), frequencies);
				auto code = huffmanCode::createHuffmanCodes(frequencies, true, true, max_depth);
				auto chars = huffmanCode::createHuffmanPair(code);

				Assert::AreEqual(62, (int)chars.size());

				int longest = 0;
				for (auto it = code.begin(); it != code.end(); ++it){
					longest = std::max(longest, (int)it->second.size());
				}

				Assert::AreEqual(max_depth, longest);

			}
	};
}