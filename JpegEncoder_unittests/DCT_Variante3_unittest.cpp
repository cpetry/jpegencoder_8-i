/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

// C4996

#pragma warning( disable : 4996 )

#define _USE_MATH_DEFINES 
#include <math.h>
#include <vector>

#include "CppUnitTest.h"

#include <boost\numeric\ublas\matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/math/special_functions/round.hpp>

#include "picture.h"
#include "ppmFile.h"
#include "DCT_Variante1.h"
#include "DCT_Variante2.h"
#include "DCT_Variante3.h"
#include "iDCT.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace boost::numeric::ublas;
using namespace boost::math;

namespace DCT_Variante3_tests
{
	TEST_CLASS(DCT_Variante3_tests)
	{
	public:
		
		TEST_METHOD(iDCT)
		{
			matrix<float> testm(8, 8);

			const int initialValues[8][8] = {
				{ 581, -143, 56, 17, 15, -6, 25, -8 },
				{ -241, 133, -47, 42, -1, -6, 13, -3 },
				{ 108, -17, -39, 71, -32, 12, 6, -9 },
				{ -55, -92, 48, 19, -7, 7, 6, -1 },
				{ -16, 9, 7, -22, -2, -9, 5, 3 },
				{ 4, 9, -3, -4, 2, 2, -6, 3 },
				{ -8, 7, 8, -5, 5, 12, 2, -4 },
				{ -8, -3, -1, -2, 6, 1, 0, 0 }
			};
			
			const int transformedValues[8][8] = {
				{ 51, 17, 14, 16, 42, 89, 48, 27 },
				{ 61, 42, 35, 40, 66, 88, 41, 26 },
				{ 71, 54, 57, 66, 55, 40, 23, 39 },
				{ 52, 59, 63, 50, 47, 26, 37, 86 },
				{ 32, 27, 33, 28, 37, 50, 81, 147 },
				{ 53, 31, 34, 46, 59, 104, 144, 178 },
				{ 76, 70, 71, 90, 117, 151, 175, 184 },
				{ 101, 105, 114, 124, 135, 168, 173, 180 }
			};


			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = (float)initialValues[j][i];

			iDCT::transform(testm);
			
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				Assert::AreEqual(transformedValues[j][i], static_cast<int>(round(testm(j, i))));
		}

		TEST_METHOD(DCT_Variante3_Example)
		{
			matrix<float> testm(8, 8);

			const BYTE initialValues[8][8] = {
				{ 47, 18, 13, 16, 41, 90, 47, 27 },
				{ 62, 42, 35, 39, 66, 90, 41, 26 },
				{ 71, 55, 56, 67, 55, 40, 22, 39 },
				{ 53, 60, 63, 50, 48, 25, 37, 87 },
				{ 31, 27, 33, 27, 37, 50, 81, 147 },
				{ 54, 31, 33, 46, 58, 104, 144, 179 },
				{ 76, 70, 71, 91, 118, 151, 176, 184 },
				{ 102, 105, 115, 124, 135, 168, 173, 181 }
			};

			const int transformedValues[8][8] = {
				{ 581, -144, 56, 17, 15, -7, 25, -9 },
				{ -242, 133, -48, 42, -2, -7, 13, -4 },
				{ 108, -18, -40, 71, -33, 12, 6, -10 },
				{ -56, -93, 48, 19, -8, 7, 6, -2 },
				{ -17, 9, 7, -23, -3, -10, 5, 3 },
				{ 4, 9, -4, -5, 2, 2, -7, 3 },
				{ -9, 7, 8, -6, 5, 12, 2, -5 },
				{ -9, -4, -2, -3, 6, 1, -1, -1 }
			};


			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = initialValues[j][i];

			DCT_Variante3::transform(testm);

			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j){
				Assert::AreEqual(transformedValues[j][i], static_cast<int>(round(testm(j, i))));
			}

		}

		TEST_METHOD(DCT_Variante3_Picture)
		{
			std::string file = std::string("../../_resources/brian_kernighan128.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 16);

			// fetching a part of the picture
			matrix<int> m(pic.getMatrixY());
			matrix<float> r(m);
			
			DCT_Variante3::transform(r);

			iDCT::transform(r);

			int wrong = 0;
			int error_width = 1;
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
			if ((int)r(j, i) + error_width < m(j, i)
				|| (int)r(j, i) - error_width > m(j, i)){
				float a = r(j, i);
				float b = static_cast<float>(m(j, i));
				wrong++;
			}
			Assert::AreEqual(0, wrong);
		}

		TEST_METHOD(DCT_Variante1_2_3_Picture)
		{
			std::string file = std::string("../../_resources/brian_kernighan128.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 16);

			// fetching a part of the picture
			matrix<int> mr(pic.getMatrixY());
			matrix<float> var1(mr);
			matrix<float> var2(mr);
			matrix<float> var3(mr);
			DCT_Variante1::transform(var1);
			DCT_Variante2::transform(var2);
			DCT_Variante3::transform(var3);
			int wrong = 0;
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j){
				if ((int)var1(j, i) != (int)var2(j, i) || (int)var1(j, i) != (int)var3(j, i)
					|| (int)var2(j, i) != (int)var3(j, i))
					wrong++;
			}
			Assert::AreEqual(0, wrong);
		}

	}; // class
} // namespace