/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

// huffman_unittest.cpp : Defines the entry point for the console application.
// C4996
#pragma warning( disable : 4996 )

#include "CppUnitTest.h"
#include "jpegSegments.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JpegEncoder_Tests
{
	TEST_CLASS(jpegSegments_tests){
	public:
		TEST_METHOD_INITIALIZE(BEGIN){

		}

		TEST_METHOD(Segment_SOI_Test){
			bitstream* bs;
			jpegSegments jpeg;
			jpeg.SOI_test();

			bs = jpeg.getBitstream();

			// SOI Marker
			Assert::AreEqual(bs->getByteAt(0), (BYTE)0xff);
			Assert::AreEqual(bs->getByteAt(1), (BYTE)0xd8);
		}

		TEST_METHOD(Segment_APP0_Test){
			bitstream* bs;
			jpegSegments jpeg;
			jpeg.APP0_test();

			bs = jpeg.getBitstream();

			// APP0 marker
			Assert::AreEqual(bs->getByteAt(0), (BYTE)0xff);
			Assert::AreEqual(bs->getByteAt(1), (BYTE)0xe0);

			// Length of segment (2 Bytes)
			Assert::AreEqual(bs->getByteAt(2), (BYTE)0x00);
			Assert::AreEqual(bs->getByteAt(3), (BYTE)0x10);

			// "JFIF" JPEG File Interchange Format ( 5 Bytes )
			Assert::AreEqual(bs->getByteAt(4), (BYTE)0x4a);
			Assert::AreEqual(bs->getByteAt(5), (BYTE)0x46);
			Assert::AreEqual(bs->getByteAt(6), (BYTE)0x49);
			Assert::AreEqual(bs->getByteAt(7), (BYTE)0x46);
			Assert::AreEqual(bs->getByteAt(8), (BYTE)0x00);
			
			// Major revision number
			Assert::AreEqual(bs->getByteAt(9), (BYTE)0x01);
			
			// Minor revision number
			Assert::AreEqual(bs->getByteAt(10), (BYTE)0x01);
			

			// Size of a pixel
			Assert::AreEqual(bs->getByteAt(11), (BYTE)0x00);

			// x and y density ( each 2 bytes)
			Assert::AreEqual(bs->getByteAt(12), (BYTE)0x00);
			Assert::AreEqual(bs->getByteAt(13), (BYTE)0x48);
			Assert::AreEqual(bs->getByteAt(14), (BYTE)0x00);
			Assert::AreEqual(bs->getByteAt(15), (BYTE)0x48);
			
			// Thumbnail size x/y and amout of bytes
			Assert::AreEqual(bs->getByteAt(16), (BYTE)0x00);
			Assert::AreEqual(bs->getByteAt(17), (BYTE)0x00);
		}

	};
}