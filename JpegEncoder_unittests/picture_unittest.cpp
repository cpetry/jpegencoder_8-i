/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

// picture_unittest.cpp : Defines the entry point for the console application.
// C4996
#pragma warning( disable : 4996 )

#include "CppUnitTest.h"
#include <vector>
#include <boost\numeric\ublas\matrix.hpp>

#include "picture.h"
#include "subSamplingScheme.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JpegEncoder_Tests
{
	TEST_CLASS(picture_tests)
	{
	public:

		TEST_METHOD_INITIALIZE(BEGIN)
		{
			
		}
        TEST_METHOD(picture_constructor)
		{
            picture test = picture(16, 32);
            Assert::AreEqual(static_cast<unsigned int>(16), test.getWidth(), L"Breite ist falsch!");
            Assert::AreEqual(static_cast<unsigned int>(32), test.getHeight(), L"H�he ist falsch!");

            test = picture(64, 96);
            Assert::AreEqual(static_cast<unsigned int>(64), test.getWidth(), L"2.Breite ist falsch!");
            Assert::AreEqual(static_cast<unsigned int>(96), test.getHeight(), L"2.H�he ist falsch!");

            test = picture(64, 128);
            Assert::AreEqual(static_cast<unsigned int>(64), test.getWidth(), L"3.Breite ist falsch!");
            Assert::AreEqual(static_cast<unsigned int>(128), test.getHeight(), L"3.H�he ist falsch!");
        }

        TEST_METHOD(picture_setter)
		{
            picture test = picture(32, 32);
            test.setColor(0, 0, 50, 50, 50);

            Assert::AreEqual(static_cast<BYTE>(50), test.getR(0,0), L"1.Farbe falsch");
            Assert::AreEqual(static_cast<BYTE>(50), test.getG(0,0), L"1.Farbe falsch");
            Assert::AreEqual(static_cast<BYTE>(50), test.getB(0,0), L"1.Farbe falsch");

            // value = value % 256
            test.setColor(0, 0, 257, 50, 50);
            Assert::AreEqual(static_cast<BYTE>(1), test.getR(0,0), L"Farbe falsch");
            
            test.setR_Y(1, 1, 200);
            test.setG_Cb(1, 1, 200);
            test.setB_Cr(1, 1, 200);
            Assert::AreEqual(static_cast<BYTE>(200), test.getR(1,1), L"Farbe falsch");
            Assert::AreEqual(static_cast<BYTE>(200), test.getG(1,1), L"Farbe falsch");
            Assert::AreEqual(static_cast<BYTE>(200), test.getB(1,1), L"Farbe falsch");
        }
		/*
        // Tests per Taschenrechner evaluiert
        TEST_METHOD(picture_getYCbCr)
		{
            picture test = picture(32, 32);
            test.setColor(0, 0, 25, 50, 144);
			test.changeColorSpaceToYCbCr();
            Assert::AreEqual(static_cast<int>(53), test.getY(0,0), L"Y falsch");
			Assert::AreEqual(static_cast<int>(179), test.getCb(0, 0), L"Cb falsch");
			Assert::AreEqual(static_cast<int>(108), test.getCr(0, 0), L"Cr falsch");
        }

		TEST_METHOD(picture_convertYCbCr)
		{
            picture test = picture(32, 32);
            test.setColor(0, 0, 25, 50, 144);
			test.setColor(1, 0, 188, 42, 11);
			test.setColor(2, 0, 73, 111, 21);
			test.changeColorSpaceToYCbCr();

            Assert::AreEqual(static_cast<BYTE>(53), test.getY(0,0), L"Y falsch");
            Assert::AreEqual(static_cast<BYTE>(179), test.getCb(0,0), L"Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(108), test.getCr(0,0), L"Cr falsch");
        }

		TEST_METHOD(picture_subsampling_420){
			picture test = picture(32, 32);
            test.setColor(0, 0, 1, 1, 1);
			test.setColor(1, 1, 50, 100, 50);
			test.setColor(2, 2, 100, 150, 100);
			test.setColor(3, 3, 150, 200, 150);
			test.setColor(4, 4, 200, 250, 200);

			test.changeColorSpaceToYCbCr();
			test.subsampleColorChannels(subSamplingScheme(_420));

			Assert::AreEqual(static_cast<BYTE>(1), test.getY(0,0), L"Y falsch");

			Assert::AreEqual(static_cast<BYTE>(128), test.getCb(0,0), L"Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(128), test.getCb(1,0), L"2.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(111), test.getCb(2,3), L"3.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(111), test.getCb(3,2), L"4.Cb falsch");

            Assert::AreEqual(static_cast<BYTE>(107), test.getCr(5,4), L"Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(107), test.getCr(4,4), L"Cr falsch");
		}

		TEST_METHOD(picture_subsampling_422){
			picture test = picture(16, 16);
            test.setColor(0, 0, 1, 1, 1);		// cb, cr = 128
			test.setColor(0, 1, 50, 100, 50);	//cb = 111, cr = 107
			test.setColor(2, 2, 100, 100, 200); //cb = 178, cr = 119
			test.setColor(4, 3, 150, 50, 255);  //cb = 213, cr = 161
			test.setColor(14, 2, 100, 100, 200); //cb = 178, cr = 119

			test.changeColorSpaceToYCbCr();
			test.subsampleColorChannels(subSamplingScheme(_422));

			Assert::AreEqual(static_cast<BYTE>(1), test.getY(0,0), L"Y falsch");

			Assert::AreEqual(static_cast<BYTE>(128), test.getCb(1,0), L"Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(128), test.getCr(1,0), L"Cr falsch");
			
			Assert::AreEqual(static_cast<BYTE>(111), test.getCb(1,1), L"2.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(107), test.getCr(1,1), L"2.Cr falsch");
			
			Assert::AreEqual(static_cast<BYTE>(178), test.getCb(3,2), L"3.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(120), test.getCr(3,2), L"3.Cr falsch");
			
			Assert::AreEqual(static_cast<BYTE>(214), test.getCb(4,3), L"4.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(161), test.getCr(5,3), L"4.Cr falsch");

			Assert::AreEqual(static_cast<BYTE>(178), test.getCb(14,2), L"5.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(120), test.getCr(15,2), L"5.Cr falsch");
		}

		TEST_METHOD(picture_subsampling_411){
			picture test = picture(16, 16);
            test.setColor(0, 0, 1, 1, 1);		// cb, cr = 128
			test.setColor(0, 1, 50, 100, 50);	//cb = 111, cr = 107
			test.setColor(4, 2, 100, 100, 200); //cb = 178, cr = 119
			test.setColor(8, 3, 150, 50, 255);  //cb = 213, cr = 161
			test.setColor(12, 4, 100, 100, 200);  //cb = 178, cr = 119

			test.changeColorSpaceToYCbCr();
			test.subsampleColorChannels(subSamplingScheme(_411));

			Assert::AreEqual(static_cast<BYTE>(1), test.getY(0,0), L"Y falsch");

			Assert::AreEqual(static_cast<BYTE>(128), test.getCb(1,0), L"Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(128), test.getCr(2,0), L"Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(128), test.getCr(3,0), L"Cr falsch");
			
			Assert::AreEqual(static_cast<BYTE>(111), test.getCb(2,1), L"2.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(107), test.getCr(3,1), L"2.Cr falsch");
			
			Assert::AreEqual(static_cast<BYTE>(178), test.getCb(6,2), L"3.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(120), test.getCr(7,2), L"3.Cr falsch");
			
			Assert::AreEqual(static_cast<BYTE>(214), test.getCb(10,3), L"4.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(161), test.getCr(9,3), L"4.Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(161), test.getCr(11,3), L"4.Cr falsch");

			Assert::AreEqual(static_cast<BYTE>(178), test.getCb(13,4), L"5.Cb falsch");
            Assert::AreEqual(static_cast<BYTE>(120), test.getCr(14,4), L"5.Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(120), test.getCr(15,4), L"5.Cr falsch");
		}
		
		TEST_METHOD(picture_subsampling_422_averaging){
			picture test = picture(32, 32);
            test.setColor(0, 0, 1, 1, 1);		// cb, cr = 128
			test.setColor(1, 0, 50, 100, 50);	//cb = 111, cr = 107
			test.setColor(2, 1, 100, 100, 200); //cb = 178, cr = 119
			test.setColor(3, 1, 150, 50, 255);  //cb = 213, cr = 161

			test.changeColorSpaceToYCbCr();
			test.subsampleColorChannels(subSamplingScheme(_422, true)); // averaging enabled

            Assert::AreEqual(static_cast<BYTE>(117), test.getCr(0,0), L"1.Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(117), test.getCr(1,0), L"2.Cr falsch");

			Assert::AreEqual(static_cast<BYTE>(119), test.getCb(0,0), L"1.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(119), test.getCb(1,0), L"2.Cb falsch");

			Assert::AreEqual(static_cast<BYTE>(196), test.getCb(2,1), L"3.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(140), test.getCr(3,1), L"3.Cr falsch");
		}
		
		TEST_METHOD(picture_subsampling_411_averaging){
			picture test = picture(32, 32);
            test.setColor(0, 0, 50, 100, 50);	//cb = 111, cr = 107
			test.setColor(1, 0, 50, 100, 50);	//cb = 111, cr = 107
			test.setColor(2, 0, 100, 100, 200); //cb = 178, cr = 119
			test.setColor(3, 0, 150, 50, 255);  //cb = 213, cr = 161

			test.changeColorSpaceToYCbCr();
			test.subsampleColorChannels(subSamplingScheme(_411, true)); // averaging enabled

            Assert::AreEqual(static_cast<BYTE>(123), test.getCr(0,0), L"1.Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(153), test.getCb(0,0), L"1.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(123), test.getCr(1,0), L"2.Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(153), test.getCb(1,0), L"2.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(153), test.getCb(2,0), L"3.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(123), test.getCr(3,0), L"3.Cr falsch");
		}

		TEST_METHOD(picture_subsampling_420_averaging){
			picture test = picture(32, 32);
            test.setColor(0, 0, 50, 100, 50);	//cb = 111, cr = 107
			test.setColor(1, 0, 50, 100, 50);	//cb = 111, cr = 107
			test.setColor(0, 1, 100, 100, 200); //cb = 178, cr = 119
			test.setColor(1, 1, 150, 50, 255);  //cb = 213, cr = 161

			test.changeColorSpaceToYCbCr();
			test.subsampleColorChannels(subSamplingScheme(_420, true)); // averaging enabled

            Assert::AreEqual(static_cast<BYTE>(123), test.getCr(0,0), L"1.Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(153), test.getCb(0,0), L"1.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(123), test.getCr(1,0), L"2.Cr falsch");
			Assert::AreEqual(static_cast<BYTE>(153), test.getCb(1,1), L"2.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(153), test.getCb(0,1), L"3.Cb falsch");
			Assert::AreEqual(static_cast<BYTE>(123), test.getCr(0,1), L"3.Cr falsch");
		}
		*/
    };
}