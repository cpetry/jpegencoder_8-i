/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

// C4996

#pragma warning( disable : 4996 )

#define _USE_MATH_DEFINES 
#include <math.h>
#include <vector>

#include "CppUnitTest.h"


#include <boost\numeric\ublas\matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/math/special_functions/round.hpp>

#include "picture.h"
#include "ppmFile.h"
#include "DCT_Variante2.h"
#include "iDCT.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace boost::numeric::ublas;
using namespace boost::math;

//typedef int BYTE;

namespace DCT_Variante2_tests
{
	TEST_CLASS(DCT_Variante2_tests)
	{
	public:
		
		TEST_METHOD(DCT_Variante2_Example)
		{
			matrix<float> testm(8, 8);
			
			const BYTE initialValues[8][8] = {
				{ 47, 18, 13, 16, 41, 90, 47, 27 },
				{ 62, 42, 35, 39, 66, 90, 41, 26 },
				{ 71, 55, 56, 67, 55, 40, 22, 39 },
				{ 53, 60, 63, 50, 48, 25, 37, 87 },
				{ 31, 27, 33, 27, 37, 50, 81, 147 },
				{ 54, 31, 33, 46, 58, 104, 144, 179 },
				{ 76, 70, 71, 91, 118, 151, 176, 184 },
				{ 102, 105, 115, 124, 135, 168, 173, 181 }
			};

			const int transformedValues[8][8] = {
				{ 581, -144, 56, 17, 15, -7, 25, -9 },
				{ -242, 133, -48, 42, -2, -7, 13, -4 },
				{ 108, -18, -40, 71, -33, 12, 6, -10 },
				{ -56, -93, 48, 19, -8, 7, 6, -2 },
				{ -17, 9, 7, -23, -3, -10, 5, 3 },
				{ 4, 9, -4, -5, 2, 2, -7, 3 },
				{ -9, 7, 8, -6, 5, 12, 2, -5 },
				{ -9, -4, -2, -3, 6, 1, -1, -1 }
			};

			
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = initialValues[j][i];
				
			DCT_Variante2::transform(testm);
			
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				Assert::AreEqual(transformedValues[j][i], static_cast<int>(round(testm(j, i))));
			
		}

		TEST_METHOD(DCT_Variante2_Picture)
		{
			std::string file = std::string("../../_resources/brian_kernighan128.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 16);

			// fetching a part of the picture
			matrix<int> mr(pic.getMatrixY());
			matrix<float> r(mr);
			
			DCT_Variante2::transform(r);

			iDCT::transform(r);

			int wrong = 0;
			int error_width = 1;
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
			if (r(j, i) + error_width < mr(j, i)
				|| r(j, i) - error_width > mr(j, i))
				wrong++;

			Assert::AreEqual(0, wrong);
		}

	}; // class
} // namespace