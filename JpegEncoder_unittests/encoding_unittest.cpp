/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

// picture_unittest.cpp : Defines the entry point for the console application.
//
// C4996


#pragma warning( disable : 4996 )

#include "CppUnitTest.h"
#include <vector>
#include <string>


#include "encoding.h"

#include "ppmFile.h"
#include "DCT_Variante3.h"
#include "Quantization.h"

// for quickly assigning arrays
#include <boost/assign/std/vector.hpp>
using namespace boost::assign;


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JpegEncoder_Tests
{
	TEST_CLASS(encoding_tests)
	{
	public:

		TEST_METHOD(encoding_zigzag_Matrix)
		{
			const int quantizedValues[8][8] = {
				{ 12, -3, 1, 0, 0, 0, 1, 0 }, // round(0.5) == 1!
				{ -5, 3, -1, 1, 0, 0, 0, 0 },
				{ 2, 0, -1, 1, -1, 0, 0, 0 },
				{ -1, -2, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 63 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 35 }
			};

			matrix<int> testm(8, 8);
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = quantizedValues[j][i];

			std::vector<int> zigzag_coeff = encoding::zigzagMatrix(testm);

			// Size should be 64 (without 0s at the end)
			Assert::AreEqual((size_t)64, zigzag_coeff.size());

			Assert::AreEqual(12, zigzag_coeff[0]);
			Assert::AreEqual(-3, zigzag_coeff[1]);
			Assert::AreEqual(-5, zigzag_coeff[2]);
			Assert::AreEqual(2, zigzag_coeff[3]);
			Assert::AreEqual(3, zigzag_coeff[4]);
			Assert::AreEqual(1, zigzag_coeff[5]);
			Assert::AreEqual(0, zigzag_coeff[6]);
			Assert::AreEqual(-1, zigzag_coeff[7]);
			Assert::AreEqual(0, zigzag_coeff[8]);
			Assert::AreEqual(-1, zigzag_coeff[9]);
			Assert::AreEqual(0, zigzag_coeff[10]);
			Assert::AreEqual(-2, zigzag_coeff[11]);
			Assert::AreEqual(-1, zigzag_coeff[12]);
			Assert::AreEqual(1, zigzag_coeff[13]);
			Assert::AreEqual(0, zigzag_coeff[14]);
			Assert::AreEqual(0, zigzag_coeff[15]);
			Assert::AreEqual(0, zigzag_coeff[16]);
			Assert::AreEqual(1, zigzag_coeff[17]);
			Assert::AreEqual(1, zigzag_coeff[18]);
			Assert::AreEqual(0, zigzag_coeff[19]);
			Assert::AreEqual(0, zigzag_coeff[20]);
			Assert::AreEqual(0, zigzag_coeff[21]);
			Assert::AreEqual(0, zigzag_coeff[22]);
			Assert::AreEqual(0, zigzag_coeff[23]);
			Assert::AreEqual(0, zigzag_coeff[24]);
			Assert::AreEqual(-1, zigzag_coeff[25]);
			Assert::AreEqual(0, zigzag_coeff[26]);
			Assert::AreEqual(1, zigzag_coeff[27]);

			Assert::AreEqual(63, zigzag_coeff[60]);
			Assert::AreEqual(35, zigzag_coeff[63]);

			zigzag_coeff.erase(zigzag_coeff.begin());
			// Size should be 64 (without 0s at the end)
			Assert::AreEqual((size_t)63, zigzag_coeff.size());
		}

		TEST_METHOD(encoding_zigzag_Coeff)
		{
			const int quantizedValues[8][8] = {
				{ 12, -3, 1, 0, 0, 0, 1, 0 }, // round(0.5) == 1!
				{ -5, 3, -1, 1, 0, 0, 0, 0 },
				{ 2, 0, -1, 1, -1, 0, 0, 0 },
				{ -1, -2, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			matrix<int> testm(8, 8);
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = quantizedValues[j][i];

			std::vector<int> zigzag_coeff = encoding::zigzagMatrix(testm);

			Assert::AreEqual((size_t)64, zigzag_coeff.size());

			Assert::AreEqual(12, zigzag_coeff[0]);
			Assert::AreEqual(-3, zigzag_coeff[1]);
			Assert::AreEqual(-5, zigzag_coeff[2]);
			Assert::AreEqual(2, zigzag_coeff[3]);
			Assert::AreEqual(3, zigzag_coeff[4]);
			Assert::AreEqual(1, zigzag_coeff[5]);
			Assert::AreEqual(0, zigzag_coeff[6]);
			Assert::AreEqual(-1, zigzag_coeff[7]);
			Assert::AreEqual(0, zigzag_coeff[8]);
			Assert::AreEqual(-1, zigzag_coeff[9]);
			Assert::AreEqual(0, zigzag_coeff[10]);
			Assert::AreEqual(-2, zigzag_coeff[11]);
			Assert::AreEqual(-1, zigzag_coeff[12]);
			Assert::AreEqual(1, zigzag_coeff[13]);
			Assert::AreEqual(0, zigzag_coeff[14]);
			Assert::AreEqual(0, zigzag_coeff[15]);
			Assert::AreEqual(0, zigzag_coeff[16]);
			Assert::AreEqual(1, zigzag_coeff[17]);
			Assert::AreEqual(1, zigzag_coeff[18]);
			Assert::AreEqual(0, zigzag_coeff[19]);
			Assert::AreEqual(0, zigzag_coeff[20]);
			Assert::AreEqual(0, zigzag_coeff[21]);
			Assert::AreEqual(0, zigzag_coeff[22]);
			Assert::AreEqual(0, zigzag_coeff[23]);
			Assert::AreEqual(0, zigzag_coeff[24]);
			Assert::AreEqual(-1, zigzag_coeff[25]);
			Assert::AreEqual(0, zigzag_coeff[26]);
			Assert::AreEqual(1, zigzag_coeff[27]);
			Assert::AreEqual(0, zigzag_coeff[28]);
			//...
		}

		TEST_METHOD(encoding_zigzag_complete){
			const int values[8][8] = {
				{ 1, 2, 3, 4, 5, 6, 7, 8 },
				{ 9, 10, 11, 12, 13, 14, 15, 16 },
				{ 17, 18, 19, 20, 21, 22, 23, 24 },
				{ 25, 26, 27, 28, 29, 30, 31, 32 },
				{ 33, 34, 35, 36, 37, 38, 39, 40 },
				{ 41, 42, 43, 44, 45, 46, 47, 48 },
				{ 49, 50, 51, 52, 53, 54, 55, 56 },
				{ 57, 58, 59, 60, 61, 62, 63, 64 } };

			matrix<int> testm(8, 8);
			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = values[j][i];

			std::vector<int> zigzag_coeff = encoding::zigzagMatrix(testm);

			Assert::AreEqual((size_t)64, zigzag_coeff.size());

			int pos = 0;
			Assert::AreEqual(1, zigzag_coeff[pos++]);
			Assert::AreEqual(2, zigzag_coeff[pos++]);
			Assert::AreEqual(9, zigzag_coeff[pos++]);
			Assert::AreEqual(17, zigzag_coeff[pos++]);
			Assert::AreEqual(10, zigzag_coeff[pos++]);
			Assert::AreEqual(3, zigzag_coeff[pos++]);
			Assert::AreEqual(4, zigzag_coeff[pos++]);
			Assert::AreEqual(11, zigzag_coeff[pos++]);

			Assert::AreEqual(18, zigzag_coeff[pos++]);
			Assert::AreEqual(25, zigzag_coeff[pos++]);
			Assert::AreEqual(33, zigzag_coeff[pos++]);
			Assert::AreEqual(26, zigzag_coeff[pos++]);
			Assert::AreEqual(19, zigzag_coeff[pos++]);
			Assert::AreEqual(12, zigzag_coeff[pos++]);
			Assert::AreEqual(5, zigzag_coeff[pos++]);
			Assert::AreEqual(6, zigzag_coeff[pos++]);

			Assert::AreEqual(13, zigzag_coeff[pos++]);
			Assert::AreEqual(20, zigzag_coeff[pos++]);
			Assert::AreEqual(27, zigzag_coeff[pos++]);
			Assert::AreEqual(34, zigzag_coeff[pos++]);
			Assert::AreEqual(41, zigzag_coeff[pos++]);
			Assert::AreEqual(49, zigzag_coeff[pos++]);
			Assert::AreEqual(42, zigzag_coeff[pos++]);
			Assert::AreEqual(35, zigzag_coeff[pos++]);

			Assert::AreEqual(28, zigzag_coeff[pos++]);
			Assert::AreEqual(21, zigzag_coeff[pos++]);
			Assert::AreEqual(14, zigzag_coeff[pos++]);
			Assert::AreEqual(7, zigzag_coeff[pos++]);
			Assert::AreEqual(8, zigzag_coeff[pos++]);
			Assert::AreEqual(15, zigzag_coeff[pos++]);
			Assert::AreEqual(22, zigzag_coeff[pos++]);
			Assert::AreEqual(29, zigzag_coeff[pos++]);

			Assert::AreEqual(36, zigzag_coeff[pos++]);
			Assert::AreEqual(43, zigzag_coeff[pos++]);
			Assert::AreEqual(50, zigzag_coeff[pos++]);
			Assert::AreEqual(57, zigzag_coeff[pos++]);
			Assert::AreEqual(58, zigzag_coeff[pos++]);
			Assert::AreEqual(51, zigzag_coeff[pos++]);
			Assert::AreEqual(44, zigzag_coeff[pos++]);
			Assert::AreEqual(37, zigzag_coeff[pos++]);

			Assert::AreEqual(30, zigzag_coeff[pos++]);
			Assert::AreEqual(23, zigzag_coeff[pos++]);
			Assert::AreEqual(16, zigzag_coeff[pos++]);
			Assert::AreEqual(24, zigzag_coeff[pos++]);
			Assert::AreEqual(31, zigzag_coeff[pos++]);
			Assert::AreEqual(38, zigzag_coeff[pos++]);
			Assert::AreEqual(45, zigzag_coeff[pos++]);
			Assert::AreEqual(52, zigzag_coeff[pos++]);

			Assert::AreEqual(59, zigzag_coeff[pos++]);
			Assert::AreEqual(60, zigzag_coeff[pos++]);
			Assert::AreEqual(53, zigzag_coeff[pos++]);
			Assert::AreEqual(46, zigzag_coeff[pos++]);
			Assert::AreEqual(39, zigzag_coeff[pos++]);
			Assert::AreEqual(32, zigzag_coeff[pos++]);
			Assert::AreEqual(40, zigzag_coeff[pos++]);
			Assert::AreEqual(47, zigzag_coeff[pos++]);

			Assert::AreEqual(54, zigzag_coeff[pos++]);
			Assert::AreEqual(61, zigzag_coeff[pos++]);
			Assert::AreEqual(62, zigzag_coeff[pos++]);
			Assert::AreEqual(55, zigzag_coeff[pos++]);
			Assert::AreEqual(48, zigzag_coeff[pos++]);
			Assert::AreEqual(56, zigzag_coeff[pos++]);
			Assert::AreEqual(63, zigzag_coeff[pos++]);
			Assert::AreEqual(64, zigzag_coeff[pos++]);
		}

		TEST_METHOD(encoding_createZeroNumberPairs){
			std::vector<int> coeffs;
			coeffs += 57, 45, 0, 0, 0, 0, 23, 0, -30, -16, 0, 0, 1;

			std::vector<std::pair<int, int>> runlength = encoding::createZeroNumberPairs(coeffs);

			Assert::AreEqual((size_t)6, runlength.size());

			Assert::AreEqual(0, runlength[0].first);
			Assert::AreEqual(57, runlength[0].second);
			Assert::AreEqual(0, runlength[1].first);
			Assert::AreEqual(45, runlength[1].second);
			Assert::AreEqual(4, runlength[2].first);
			Assert::AreEqual(23, runlength[2].second);
			Assert::AreEqual(1, runlength[3].first);
			Assert::AreEqual(-30, runlength[3].second);
			Assert::AreEqual(0, runlength[4].first);
			Assert::AreEqual(-16, runlength[4].second);
			Assert::AreEqual(2, runlength[5].first);
			Assert::AreEqual(1, runlength[5].second);
		}

		TEST_METHOD(encoding_createZeroNumberPairs_many_zeros){
			std::vector<int> coeffs;
			coeffs += 57, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0, -30, -16, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0;

			std::vector<std::pair<int, int>> runlength = encoding::createZeroNumberPairs(coeffs);

			Assert::AreEqual((size_t)8, runlength.size());

			Assert::AreEqual(0, runlength[0].first);
			Assert::AreEqual(57, runlength[0].second);
			Assert::AreEqual(0, runlength[1].first);
			Assert::AreEqual(45, runlength[1].second);
			Assert::AreEqual(15, runlength[2].first); // 15 \  16 zeros
			Assert::AreEqual(0, runlength[2].second); // 0  /
			Assert::AreEqual(4, runlength[3].first);  // 4     4 zeros => 20 zeros
			Assert::AreEqual(23, runlength[3].second);// 23    number 23
			Assert::AreEqual(1, runlength[4].first);
			Assert::AreEqual(-30, runlength[4].second);
			Assert::AreEqual(0, runlength[5].first);
			Assert::AreEqual(-16, runlength[5].second);
			Assert::AreEqual(2, runlength[6].first);
			Assert::AreEqual(1, runlength[6].second);
			Assert::AreEqual(0, runlength[7].first); // EOB
			Assert::AreEqual(0, runlength[7].second);
		}

		TEST_METHOD(encoding_getCategory){
			Assert::AreEqual(6, encoding::getCategory(57));
			Assert::AreEqual(6, encoding::getCategory(45));
			Assert::AreEqual(5, encoding::getCategory(23));
			Assert::AreEqual(5, encoding::getCategory(-30));
			Assert::AreEqual(4, encoding::getCategory(-8));
			Assert::AreEqual(1, encoding::getCategory(1));
			Assert::AreEqual(1, encoding::getCategory(-1));
			Assert::AreEqual(9, encoding::getCategory(-511));

			Assert::AreEqual(0, encoding::getCategory(0));
			Assert::AreEqual(2, encoding::getCategory(3));
			Assert::AreEqual(2, encoding::getCategory(-3));
			Assert::AreEqual(3, encoding::getCategory(-4));
			Assert::AreEqual(3, encoding::getCategory(4));
			Assert::AreEqual(4, encoding::getCategory(-9));
		}

		TEST_METHOD(encoding_getBitCode){
			std::vector<bool> code57;  code57  += true, true, true, false, false, true;
			std::vector<bool> code45;  code45  += true, false, true, true, false, true;
			std::vector<bool> code23;  code23  += true, false, true, true, true;
			std::vector<bool> codem30; codem30 += false, false, false, false, true;
			std::vector<bool> codem8;  codem8  += false, true, true, true;
			std::vector<bool> code1;   code1   += true;
			std::vector<bool> codem1;  codem1  += false;
			std::vector<bool> codem511; codem511 += false, false, false, false, false, false, false, false, false;
			std::vector<bool> code511; code511 += true, true, true, true, true, true, true, true, true;
			std::vector<bool> codem9; codem9 += false, true, true, false;

			Assert::AreEqual(code57.size(), encoding::getBitCode(57).size());
			for (unsigned int i = 0; i < code57.size(); i++)
				Assert::IsTrue(code57[i] == encoding::getBitCode(57)[i]);
			
			Assert::AreEqual(code45.size(), encoding::getBitCode(45).size());
			for (unsigned int i = 0; i < code45.size(); i++)
				Assert::IsTrue(code45[i] == encoding::getBitCode(45)[i]);
			
			Assert::AreEqual(code23.size(), encoding::getBitCode(23).size());
			for (unsigned int i = 0; i < code23.size(); i++)
				Assert::IsTrue(code23[i] == encoding::getBitCode(23)[i]);
			
			Assert::AreEqual(codem30.size(), encoding::getBitCode(-30).size());
			for (unsigned int i = 0; i < codem30.size(); i++)
				Assert::IsTrue(codem30[i] == encoding::getBitCode(-30)[i]);
			
			Assert::AreEqual(4, (int)codem8.size());
			for (unsigned int i = 0; i < codem8.size(); i++)
				Assert::IsTrue(codem8[i] == encoding::getBitCode(-8)[i]);
			
			Assert::AreEqual(code1.size(), encoding::getBitCode(1).size());
			for (unsigned int i = 0; i < code1.size(); i++)
				Assert::IsTrue(code1[i] == encoding::getBitCode(1)[i]);
			
			Assert::AreEqual(codem1.size(), encoding::getBitCode(-1).size());
			for (unsigned int i = 0; i < codem1.size(); i++)
				Assert::IsTrue(codem1[i] == encoding::getBitCode(-1)[i]);

			Assert::AreEqual(codem511.size(), encoding::getBitCode(-511).size());
			for (unsigned int i = 0; i < codem511.size(); i++)
				Assert::IsTrue(codem511[i] == encoding::getBitCode(-511)[i]);

			Assert::AreEqual(code511.size(), encoding::getBitCode(511).size());
			for (unsigned int i = 0; i < code511.size(); i++)
				Assert::IsTrue(code511[i] == encoding::getBitCode(511)[i]);

			Assert::AreEqual(4, (int)codem9.size());
			for (unsigned int i = 0; i < codem9.size(); i++)
				Assert::IsTrue(codem9[i] == encoding::getBitCode(-9)[i]);
		}

		
		TEST_METHOD(encoding_createDCdeltas){
			std::string file = std::string("../../_resources/16x16.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 8);

			// fetching a part of the picture
			matrix<float> m(pic.getMatrixY());
			DCT_Variante3::transform(m);
			matrix<int> y = Quantization::quantize(m, Quantization::ITUT, Quantization::LUMINANCE);

			std::vector<int> dc_deltas = encoding::createDCdeltas(y);
			
			Assert::AreEqual(4, (int)dc_deltas.size());

			Assert::AreEqual(4, dc_deltas[0]); // should not be 0
		}

		TEST_METHOD(encoding_createACpairs){
			std::string file = std::string("../../_resources/8x8.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 8);

			// fetching a part of the picture
			matrix<float> m(pic.getMatrixY());
			DCT_Variante3::transform(m);
			matrix<int> y = Quantization::quantize(m, Quantization::ITUT, Quantization::LUMINANCE);

			std::vector<std::vector<std::pair<int, int>>> ac_pairs = encoding::createACpairs(y);

			Assert::AreEqual(28, (int)ac_pairs[0].size());
		}

		TEST_METHOD(encoding_generateHuffmanTableDC){
			std::string file = std::string("../../_resources/8x8.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 8);

			// fetching a part of the picture
			matrix<float> m(pic.getMatrixY());
			DCT_Variante3::transform(m);
			matrix<int> y = Quantization::quantize(m, Quantization::ITUT, Quantization::LUMINANCE);

			std::vector<int> dc_deltas = encoding::createDCdeltas(y);

			std::vector<int> dc_categories;
			for (int d : dc_deltas)
				dc_categories.push_back(encoding::getCategory(d));

			std::map<int, int> freqDC_Y = huffmanCode::generateFrequencies(dc_categories);
			
			HuffMap hm = huffmanCode::createHuffmanCodes(freqDC_Y, true, true, 16);

			HuffVect code = hm[encoding::getCategory(dc_deltas[0])];

			Assert::AreEqual(1, (int)code.size());
		}

		TEST_METHOD(encoding_generateHuffmanTableAC){
			std::string file = std::string("../../_resources/8x8.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 8);

			// fetching a part of the picture
			matrix<float> m(pic.getMatrixY());
			DCT_Variante3::transform(m);
			matrix<int> y = Quantization::quantize(m, Quantization::ITUT, Quantization::LUMINANCE);

			std::vector<std::vector<std::pair<int, int>>> ac_pairs = encoding::createACpairs(y);
			std::vector<int> to_encode;
			for (std::vector<std::pair<int, int>> v : ac_pairs){
				for (std::pair<int, int> p : v)
					to_encode.push_back(p.first * 16 + encoding::getCategory(p.second));
			}

			std::map<int, int> freqAC_Y = huffmanCode::generateFrequencies(to_encode);
			HuffMap hm = huffmanCode::createHuffmanCodes(freqAC_Y, true, true, 16);

			HuffVect code = hm[encoding::getCategory(ac_pairs[0][0].second)];

			Assert::AreEqual(2, (int)code.size());
		}

		TEST_METHOD(encoding_createBlockStreams){
			std::string file = std::string("../../_resources/8x8.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 8);

			// fetching a part of the picture
			matrix<float> r(pic.getMatrixY());
			DCT_Variante3::transform(r);
			matrix<int> y = Quantization::quantize(r, Quantization::ITUT, Quantization::LUMINANCE);

			std::vector<int> dc_deltas = encoding::createDCdeltas(y);
			std::vector<int> dc_categories;
			for (int d : dc_deltas)
				dc_categories.push_back(encoding::getCategory(d));
			std::map<int, int> freqDC_Y = huffmanCode::generateFrequencies(dc_categories);


			std::vector<std::vector<std::pair<int, int>>> ac_pairs = encoding::createACpairs(y);
			std::vector<int> to_encode;
			for (std::vector<std::pair<int, int>> v : ac_pairs){
				for (std::pair<int, int> p : v)
					to_encode.push_back(p.first * 16 + encoding::getCategory(p.second));
			}
			std::map<int, int> freqAC_Y = huffmanCode::generateFrequencies(to_encode);

			std::vector<HuffMap> trees;

			trees.push_back(huffmanCode::createHuffmanCodes(freqDC_Y, true, true, 16));
			trees.push_back(huffmanCode::createHuffmanCodes(freqAC_Y, true, true, 16));

			std::vector<bitstream> blockstreams = encoding::createBlockstreams(dc_deltas, ac_pairs, trees[0], trees[1]);

			
		}

		TEST_METHOD(encoding_createYBitstreamsAndTrees){
			const int values[8][8] = {
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};
			matrix<int> testm(8, 8);

			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = values[j][i];

			std::vector<bitstream> y_blocks;
			HuffMap dc_y_tree, ac_y_tree;

			encoding::createYBitstreamsAndTrees(testm, SUBSAMPLINGSCHEME::_444, Quantization::NOCOMPRESSION, dc_y_tree, ac_y_tree, y_blocks);

			Assert::AreEqual(1, (int)y_blocks.size());
			Assert::AreEqual(2, (int)y_blocks[0].size());

			Assert::AreEqual(false, y_blocks[0][0]);
			
			// DC - value
			std::vector<bool> bitCode;
			for (unsigned int i = 1; i < y_blocks[0].size() - 1; i++)
				bitCode += y_blocks[0][i];

			Assert::AreEqual(0, encoding::getIntegerOfBitCode(bitCode));
		}

		TEST_METHOD(encoding_createYBitstreamsAndTreesBW){
			const int values[8][16] = {
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 },
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 },
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 },
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 },
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 },
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 },
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 },
				{ -128, -128, -128, -128, -128, -128, -128, -128, 128, 128, 128, 128, 128, 128, 128, 128 }
			};
			matrix<int> testm(16, 8);

			for (int i = 0; i < 16; ++i)
			for (int j = 0; j < 8; ++j)
				testm(i, j) = values[j][i];

			std::vector<bitstream> y_blocks;
			HuffMap dc_y_tree, ac_y_tree;

			encoding::createYBitstreamsAndTrees(testm, SUBSAMPLINGSCHEME::_444, Quantization::NOCOMPRESSION, dc_y_tree, ac_y_tree, y_blocks);

			// 1.DC - value
			std::vector<bool> bitCode;
			for (unsigned int i = 1; i < y_blocks[0].size() - 1; i++)
				bitCode += y_blocks[0][i];
			Assert::AreEqual(1023, encoding::getIntegerOfBitCode(bitCode));

			// 2.DC - value
			bitCode.clear();
			for (unsigned int i = 1; i < y_blocks[1].size() - 1; i++)
				bitCode += y_blocks[1][i];
			Assert::AreEqual(2048, encoding::getIntegerOfBitCode(bitCode));
		}

		TEST_METHOD(encoding_createCbCrBitstreamsAndTrees){
			const int values[8][8] = {
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};
			matrix<int> testcb(8, 8);
			matrix<int> testcr(8, 8);

			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j){
				testcb(j, i) = values[j][i];
				testcr(j, i) = values[j][i];
			}

			std::vector<bitstream> cb_blocks, cr_blocks;
			HuffMap dc_cbcr_tree, ac_cbcr_tree;

			encoding::createCbCrBitstreamsAndTrees(testcb, testcr, Quantization::NOCOMPRESSION, dc_cbcr_tree, ac_cbcr_tree, cb_blocks, cr_blocks);

			Assert::AreEqual(1, (int)cb_blocks.size());
			Assert::AreEqual(2, (int)cb_blocks[0].size());

			// DC - category
			Assert::AreEqual(0, (int)dc_cbcr_tree.begin()->first);

			// DC - value
			std::vector<bool> bitCode;
			for (unsigned int i = 1; i < cb_blocks[0].size() - 1; i++)
				bitCode += cb_blocks[0][i];

			Assert::AreEqual(0, encoding::getIntegerOfBitCode(bitCode));

			// AC - category
			Assert::AreEqual(0, (int)ac_cbcr_tree.begin()->first);
			
			Assert::AreEqual(1, (int)cr_blocks.size());
			Assert::AreEqual(2, (int)cr_blocks[0].size());

		
			// DC - category
			Assert::AreEqual(0, (int)dc_cbcr_tree.begin()->first);

			// DC - value
			bitCode.clear();
			for (unsigned int i = 1; i < cr_blocks[0].size() - 1; i++)
				bitCode += cr_blocks[0][i];

			Assert::AreEqual(0, encoding::getIntegerOfBitCode(bitCode));


			// AC - category
			Assert::AreEqual(0, (int)ac_cbcr_tree.begin()->first);
		}

		TEST_METHOD(encoding_getIntegerOfBitCode){
			std::vector<bool> bitCode;

			bitCode += false;
			Assert::AreEqual(0, encoding::getIntegerOfBitCode(bitCode));
			bitCode.clear();

			bitCode += true;
			Assert::AreEqual(1, encoding::getIntegerOfBitCode(bitCode));
			bitCode.clear();

			bitCode += true, false, false, false, false, false;
			Assert::AreEqual(32, encoding::getIntegerOfBitCode(bitCode));
			bitCode.clear();

			bitCode += true, false, true, false, false, false, true;
			Assert::AreEqual(64 + 16 + 1, encoding::getIntegerOfBitCode(bitCode));
			bitCode.clear();

		}
		
	};
}