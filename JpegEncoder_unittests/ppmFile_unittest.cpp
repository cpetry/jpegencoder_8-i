/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

// picture_unittest.cpp : Defines the entry point for the console application.
//
// C4996
#pragma warning( disable : 4996 )

#include "CppUnitTest.h"
#include <vector>
#include <string>
#include <boost\numeric\ublas\matrix.hpp>
#include <boost/optional/optional.hpp>

#include "picture.h"
#include "ppmFile.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JpegEncoder_Tests
{
	TEST_CLASS(ppmFile_tests)
	{
	public:

		TEST_METHOD_INITIALIZE(BEGIN)
		{
			
		}

		TEST_METHOD(ppmFileBytes_read_128_pic)
		{
            std::string file = std::string("../../_resources/brian_kernighan128.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 5);

			Assert::AreEqual(static_cast<unsigned int>(130), pic.getWidth());
			Assert::AreEqual(static_cast<unsigned int>(130), pic.getHeight());
			// Top of nose: x:63, y:73 = R:215 G:159 B:138
			Assert::AreEqual(static_cast<BYTE>(215), pic.getR(63,73));
			Assert::AreEqual(static_cast<BYTE>(159), pic.getG(63,73));
			Assert::AreEqual(static_cast<BYTE>(138), pic.getB(63,73));
        }

		TEST_METHOD(ppmFileBytes_read_7x17_pic)
		{
            std::string file = std::string("../../_resources/7x17.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 16);

			Assert::AreEqual(static_cast<unsigned int>(16), pic.getWidth());
			Assert::AreEqual(static_cast<unsigned int>(32), pic.getHeight());
			// Top of nose: x:3, y:8 = R:255 G:103 B:33
			Assert::AreEqual(static_cast<BYTE>(255), pic.getR(3,8));
			Assert::AreEqual(static_cast<BYTE>(103), pic.getG(3,8));
			Assert::AreEqual(static_cast<BYTE>(33), pic.getB(3,8));

			// blue hair: x:6, y:0 = R:102 G:163 B:255
			Assert::AreEqual(static_cast<BYTE>(102), pic.getR(6,0));
			Assert::AreEqual(static_cast<BYTE>(163), pic.getG(6,0));
			Assert::AreEqual(static_cast<BYTE>(255), pic.getB(6,0));

			// end of face: x:7, y:17 = R:255 G:205 B:25
			Assert::AreEqual(static_cast<BYTE>(255), pic.getR(7,17));
			Assert::AreEqual(static_cast<BYTE>(205), pic.getG(7,17));
			Assert::AreEqual(static_cast<BYTE>(25), pic.getB(7,17));
        }

		TEST_METHOD(ppmFileBytes_read_2048_pic)
		{
            std::string file = std::string("../../_resources/brian_kernighan2048.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 16);
        }

		TEST_METHOD(ppmFileBytes_read_1024_pic)
		{
            std::string file = std::string("../../_resources/brian_kernighan1024.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 16);
        }

		TEST_METHOD(ppmFileBytes_read_512_pic)
		{
            std::string file = std::string("../../_resources/brian_kernighan512.ppm");
			picture pic;
			ppmFile::readFileBytes(file, pic, 16);
        }
    };
}