/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

// picture_unittest.cpp : Defines the entry point for the console application.
//
// C4996


#pragma warning( disable : 4996 )

#include "CppUnitTest.h"
#include <vector>
#include <string>


#include "bitstream.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JpegEncoder_Tests
{
	TEST_CLASS(bitstream_tests)
	{
	public:

		TEST_METHOD(bitstream_somebits_write)
		{
			bitstream bs;
			
			// write 5 bit to file
			bs.push_bit(true);
			bs.push_bit(false);
			bs.push_bit(true);
			bs.push_bit(false);
			bs.push_bit(true);

			Assert::AreEqual(5, (int)bs.size());
			
			bs.writeToFile("somebits.test");

			Assert::AreEqual(8, (int)bs.size());
        }

		TEST_METHOD(bitstream_somebits_read)
		{
			bitstream bs;
			
			bs.readFromFile("somebits.test");

			// bit per bit
			Assert::AreEqual(true, bs[0], L"1. bit incorrect");
			Assert::AreEqual(false, bs[1], L"2. bit incorrect");
			Assert::AreEqual(true, bs[2], L"3. bit incorrect");
			Assert::AreEqual(false, bs[3], L"4. bit incorrect");
			Assert::AreEqual(true, bs[4], L"5. bit incorrect");

			// the remaining byte should have 1s 'till its full
			Assert::AreEqual(true, bs[5], L"6. bit incorrect");
			Assert::AreEqual(true, bs[6], L"7. bit incorrect");
			Assert::AreEqual(true, bs[7], L"8. bit incorrect");
        }

		TEST_METHOD(bitstream_millionbits_write)
		{
			bitstream bs;
			
			int million = 1000000;
			for (int i = 0; i < million; i++)
				bs.push_bit(true);
			
			bs.writeToFile("millionbits.test");

			Assert::AreEqual(true, bs[0], L"First bit incorrect");
			Assert::AreEqual(true, bs[500000], L"Middle bit incorrect");
			Assert::AreEqual(true, bs[999999], L"Last bit incorrect");
        }

		TEST_METHOD(bitstream_millionbits_read)
		{
			bitstream bs;

			bs.readFromFile("millionbits.test");

			Assert::AreEqual(true, bs[0], L"First bit incorrect");
			Assert::AreEqual(true, bs[500000], L"Middle bit incorrect");
			Assert::AreEqual(true, bs[999999], L"Last bit incorrect");
		}

		TEST_METHOD(bitstream_somebytes_write)
		{
			bitstream bs;
			
            // t = 74 e = 65 s = 73 t = 74
			for (int i = 0; i < 1; i++){
				bs.push_byte(0x74);
				bs.push_byte(0x65);
				bs.push_byte(0x73);
				bs.push_byte(0x74);
			}		

			bs.writeToFile("somebytes.test");
        }

		TEST_METHOD(bitstream_somebytes_read)
		{
			bitstream bs;

			bs.readFromFile("somebytes.test");

			int charpos = 0;
			// t = 0111 0100
			Assert::AreEqual(false, bs[charpos + 0], L"t 1 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 1], L"t 2 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 2], L"t 3 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 3], L"t 4 bit incorrect");

			Assert::AreEqual(false, bs[charpos + 4], L"t 5 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 5], L"t 6 bit incorrect");
			Assert::AreEqual(false, bs[charpos + 6], L"t 7 bit incorrect");
			Assert::AreEqual(false, bs[charpos + 7], L"t 8 bit incorrect");

			charpos += 8;
			// e = 0110 0101
			Assert::AreEqual(false, bs[charpos + 0], L"e 1 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 1], L"e 2 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 2], L"e 3 bit incorrect");
			Assert::AreEqual(false, bs[charpos + 3], L"e 4 bit incorrect");

			Assert::AreEqual(false, bs[charpos + 4], L"e 5 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 5], L"e 6 bit incorrect");
			Assert::AreEqual(false, bs[charpos + 6], L"e 7 bit incorrect");
			Assert::AreEqual(true,	bs[charpos + 7], L"e 8 bit incorrect");
			/*
			// bit per bit
			for (size_t i = 0; i < bs.size(); i++){
				if (bs[i] == 1)
					Logger::WriteMessage("1");
				else
					Logger::WriteMessage("0");
			}*/
		}

		TEST_METHOD(bitstream_push_hex_and_bit)
		{
			bitstream bs;
			bs.push_bit(true);
			bs.push_bit(false);
			bs.push_byte(8);
			bs.push_bit(true);

			Assert::AreEqual(11, (int)bs.size());

			int pos = 0;
			Assert::AreEqual(true, bs[pos++]);
			Assert::AreEqual(false, bs[pos++]);

			Assert::AreEqual(false, bs[pos++]);
			Assert::AreEqual(false, bs[pos++]);
			Assert::AreEqual(false, bs[pos++]);
			Assert::AreEqual(false, bs[pos++]);

			Assert::AreEqual(true, bs[pos++]);
			Assert::AreEqual(false, bs[pos++]);
			Assert::AreEqual(false, bs[pos++]);
			Assert::AreEqual(false, bs[pos++]);

			Assert::AreEqual(true, bs[pos++]);
		}
		/*
		TEST_METHOD(bitstream_push_stream)
		{
			bitstream bs;
			bitstream bs1;

			bs.push_hex(0xFF);
			bs1.push_stream(bs);

			Assert::AreEqual(16, (int)bs1.size());

			int pos = 0;

			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);

			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);

			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);

			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);

		}

		TEST_METHOD(bitstream_push_stream2)
		{
			bitstream bs;
			bitstream bs1;

			bs.push_hex(0x0F);
			bs.push_hex(0xF0);
			bs1.push_stream(bs);

			Assert::AreEqual(16, (int)bs1.size());

			int pos = 0;

			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);

			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);

			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);
			Assert::AreEqual(true, bs1[pos++]);

			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
			Assert::AreEqual(false, bs1[pos++]);
		}*/
	};
}