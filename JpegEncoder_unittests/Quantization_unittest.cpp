/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

// picture_unittest.cpp : Defines the entry point for the console application.
//
// C4996


#pragma warning( disable : 4996 )

#include "CppUnitTest.h"
#include <vector>
#include <string>

#include "quantization.h"
#include <boost\numeric\ublas\matrix.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace boost::numeric::ublas;
namespace JpegEncoder_Tests
{
	TEST_CLASS(quantization_tests)
	{
	public:

		TEST_METHOD(quantize_example)
		{
			const int initialValues[8][8] = {
				{  581, -144,  56,  17,  15,  -7, 25,  -9 },
				{ -242,  133, -48,  42,  -2,  -7, 13,  -4 },
				{  108,  -18, -40,  71, -33,  12,  6, -10 },
				{  -56,  -93,  48,  19,  -8,   7,  6,  -2 },
				{  -17,    9,   7, -23,  -3, -10,  5,   3 },
				{    4,    9,  -4,  -5,   2,   2, -7,   3 },
				{   -9,    7,   8,  -6,   5,  12,  2,  -5 },
				{   -9,   -4,  -2,  -3,   6,   1, -1,  -1 }
			};

			const int quantizedValues[8][8] = {
				{ 12, -3,  1,  0,  0, 0, 1, 0 }, // round(0.5) == 1!
				{ -5,  3, -1,  1,  0, 0, 0, 0 },
				{  2,  0, -1,  1, -1, 0, 0, 0 },
				{ -1, -2,  1,  0,  0, 0, 0, 0 },
				{  0,  0,  0,  0,  0, 0, 0, 0 },
				{  0,  0,  0,  0,  0, 0, 0, 0 },
				{  0,  0,  0,  0,  0, 0, 0, 0 },
				{  0,  0,  0,  0,  0, 0, 0, 0 }
			};

			matrix<float> testm(8, 8);

			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = (float)initialValues[j][i];

			matrix<int> result = Quantization::quantize(testm, Quantization::Q_Table::EXAMPLE);

			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j){
				Assert::AreEqual(quantizedValues[i][j], result(j, i));
			}
		}


		TEST_METHOD(quantize_ITUT)
		{
			const int initialValues[8][8] = {
				{ -415, -33, -58,  35,  58, -51, -15, -12 },
				{    5, -34,  49,  18,  27,   1,  -5,   3 },
				{  -46,  14,  80, -35, -50,  19,   7, -18 },
				{  -53,  21,  34, -20,   2,  34,  36,  12 },
				{    9,  -2,   9,  -5, -32, -15,  45,  37 },
				{   -8,  15, -16,   7,  -8,  11,   4,   7 },
				{   19, -28,  -2, -26,  -2,   7, -44, -21 },
				{   18,  25, -12, -44,  35,  48, -37,  -3 }
			};

			const int quantizedValues[8][8] = {
				{ -26, -3, -6,  2,  2, -1, 0, 0 },
				{   0, -3,  4,  1,  1,  0, 0, 0 },
				{  -3,  1,  5, -1, -1,  0, 0, 0 },
				{  -4,  1,  2, -1,  0,  0, 0, 0 },
				{   1,  0,  0,  0,  0,  0, 0, 0 },
				{   0,  0,  0,  0,  0,  0, 0, 0 },
				{   0,  0,  0,  0,  0,  0, 0, 0 },
				{   0,  0,  0,  0,  0,  0, 0, 0 }
			};

			matrix<float> testm(8, 8);

			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				testm(j, i) = (float)initialValues[j][i];

			matrix<int> result = Quantization::quantize(testm);

			for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				Assert::AreEqual(quantizedValues[i][j], result(j, i));
		}
	};
}