/********************************************************************************
** Form generated from reading UI file 'main_GUI.ui'
**
** Created by: Qt User Interface Compiler version 5.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAIN_GUI_H
#define UI_MAIN_GUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_main_GUI
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_7;
    QLabel *lbl_file;
    QLineEdit *lineEdit_filename;
    QPushButton *btn_choose;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_6;
    QComboBox *comboBox_stepsize;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_4;
    QLineEdit *lineEdit_dimensionsX;
    QLabel *label_5;
    QLineEdit *lineEdit_dimensionsY;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_2;
    QCheckBox *checkBox_SubsamplingAverage;
    QComboBox *ComboBox_Subsampling;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_3;
    QComboBox *ComboBox_QTable;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label;
    QCheckBox *checkBox_scaleRightImage;
    QWidget *Pictures;
    QHBoxLayout *horizontalLayout_2;
    QWidget *LeftSIde;
    QVBoxLayout *verticalLayout_2;
    QWidget *header_left;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *btn_loadPPM;
    QLabel *lbl_filesize_load;
    QLineEdit *lineEdit_filesize_load;
    QWidget *leftPicture;
    QProgressBar *progressBar_left;
    QWidget *RightSide;
    QVBoxLayout *verticalLayout_3;
    QWidget *header_right;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *btn_Encode;
    QLabel *lbl_filesize_encode;
    QLineEdit *lineEdit_filesize_encode;
    QWidget *rightPicture;
    QProgressBar *progressBar_right;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *main_GUI)
    {
        if (main_GUI->objectName().isEmpty())
            main_GUI->setObjectName(QStringLiteral("main_GUI"));
        main_GUI->resize(592, 502);
        centralWidget = new QWidget(main_GUI);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        groupBox->setMinimumSize(QSize(200, 50));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        lbl_file = new QLabel(groupBox);
        lbl_file->setObjectName(QStringLiteral("lbl_file"));

        horizontalLayout_7->addWidget(lbl_file);

        lineEdit_filename = new QLineEdit(groupBox);
        lineEdit_filename->setObjectName(QStringLiteral("lineEdit_filename"));

        horizontalLayout_7->addWidget(lineEdit_filename);

        btn_choose = new QPushButton(groupBox);
        btn_choose->setObjectName(QStringLiteral("btn_choose"));

        horizontalLayout_7->addWidget(btn_choose);


        verticalLayout_5->addLayout(horizontalLayout_7);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_12->addWidget(label_6);

        comboBox_stepsize = new QComboBox(groupBox);
        comboBox_stepsize->setObjectName(QStringLiteral("comboBox_stepsize"));
        comboBox_stepsize->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_12->addWidget(comboBox_stepsize);


        verticalLayout_5->addLayout(horizontalLayout_12);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_10->addWidget(label_4);

        lineEdit_dimensionsX = new QLineEdit(groupBox);
        lineEdit_dimensionsX->setObjectName(QStringLiteral("lineEdit_dimensionsX"));
        lineEdit_dimensionsX->setEnabled(false);
        lineEdit_dimensionsX->setMaximumSize(QSize(80, 16777215));
        lineEdit_dimensionsX->setReadOnly(true);

        horizontalLayout_10->addWidget(lineEdit_dimensionsX);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_10->addWidget(label_5);

        lineEdit_dimensionsY = new QLineEdit(groupBox);
        lineEdit_dimensionsY->setObjectName(QStringLiteral("lineEdit_dimensionsY"));
        lineEdit_dimensionsY->setEnabled(false);
        lineEdit_dimensionsY->setMaximumSize(QSize(80, 16777215));
        lineEdit_dimensionsY->setReadOnly(true);

        horizontalLayout_10->addWidget(lineEdit_dimensionsY);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer);


        verticalLayout_5->addLayout(horizontalLayout_10);


        horizontalLayout->addLayout(verticalLayout_5);


        horizontalLayout_5->addWidget(groupBox);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_4 = new QVBoxLayout(groupBox_2);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);
        label_2->setMinimumSize(QSize(100, 0));
        label_2->setMaximumSize(QSize(100, 16777215));
        label_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(label_2);

        checkBox_SubsamplingAverage = new QCheckBox(groupBox_2);
        checkBox_SubsamplingAverage->setObjectName(QStringLiteral("checkBox_SubsamplingAverage"));
        checkBox_SubsamplingAverage->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_8->addWidget(checkBox_SubsamplingAverage);

        ComboBox_Subsampling = new QComboBox(groupBox_2);
        ComboBox_Subsampling->setObjectName(QStringLiteral("ComboBox_Subsampling"));
        ComboBox_Subsampling->setMinimumSize(QSize(100, 0));
        ComboBox_Subsampling->setMaximumSize(QSize(100, 20));

        horizontalLayout_8->addWidget(ComboBox_Subsampling);


        verticalLayout_7->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(100, 0));
        label_3->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_9->addWidget(label_3);

        ComboBox_QTable = new QComboBox(groupBox_2);
        ComboBox_QTable->setObjectName(QStringLiteral("ComboBox_QTable"));
        ComboBox_QTable->setMinimumSize(QSize(100, 0));
        ComboBox_QTable->setMaximumSize(QSize(100, 20));

        horizontalLayout_9->addWidget(ComboBox_QTable);


        verticalLayout_7->addLayout(horizontalLayout_9);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setMinimumSize(QSize(100, 0));
        label->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_11->addWidget(label);

        checkBox_scaleRightImage = new QCheckBox(groupBox_2);
        checkBox_scaleRightImage->setObjectName(QStringLiteral("checkBox_scaleRightImage"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(checkBox_scaleRightImage->sizePolicy().hasHeightForWidth());
        checkBox_scaleRightImage->setSizePolicy(sizePolicy1);
        checkBox_scaleRightImage->setMinimumSize(QSize(100, 0));
        checkBox_scaleRightImage->setMaximumSize(QSize(100, 16777215));
        checkBox_scaleRightImage->setChecked(true);

        horizontalLayout_11->addWidget(checkBox_scaleRightImage);


        verticalLayout_7->addLayout(horizontalLayout_11);


        horizontalLayout_6->addLayout(verticalLayout_7);


        verticalLayout_4->addLayout(horizontalLayout_6);


        horizontalLayout_5->addWidget(groupBox_2);


        verticalLayout->addLayout(horizontalLayout_5);

        Pictures = new QWidget(centralWidget);
        Pictures->setObjectName(QStringLiteral("Pictures"));
        horizontalLayout_2 = new QHBoxLayout(Pictures);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        LeftSIde = new QWidget(Pictures);
        LeftSIde->setObjectName(QStringLiteral("LeftSIde"));
        LeftSIde->setMinimumSize(QSize(283, 256));
        verticalLayout_2 = new QVBoxLayout(LeftSIde);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        header_left = new QWidget(LeftSIde);
        header_left->setObjectName(QStringLiteral("header_left"));
        horizontalLayout_3 = new QHBoxLayout(header_left);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(10, 0, 10, 0);
        btn_loadPPM = new QPushButton(header_left);
        btn_loadPPM->setObjectName(QStringLiteral("btn_loadPPM"));
        sizePolicy1.setHeightForWidth(btn_loadPPM->sizePolicy().hasHeightForWidth());
        btn_loadPPM->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(btn_loadPPM);

        lbl_filesize_load = new QLabel(header_left);
        lbl_filesize_load->setObjectName(QStringLiteral("lbl_filesize_load"));
        lbl_filesize_load->setMinimumSize(QSize(100, 0));
        lbl_filesize_load->setMaximumSize(QSize(100, 16777215));
        lbl_filesize_load->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(lbl_filesize_load);

        lineEdit_filesize_load = new QLineEdit(header_left);
        lineEdit_filesize_load->setObjectName(QStringLiteral("lineEdit_filesize_load"));
        lineEdit_filesize_load->setEnabled(false);
        lineEdit_filesize_load->setMaximumSize(QSize(70, 16777215));
        lineEdit_filesize_load->setReadOnly(true);

        horizontalLayout_3->addWidget(lineEdit_filesize_load);


        verticalLayout_2->addWidget(header_left);

        leftPicture = new QWidget(LeftSIde);
        leftPicture->setObjectName(QStringLiteral("leftPicture"));
        sizePolicy1.setHeightForWidth(leftPicture->sizePolicy().hasHeightForWidth());
        leftPicture->setSizePolicy(sizePolicy1);
        leftPicture->setMinimumSize(QSize(256, 256));
        leftPicture->setMaximumSize(QSize(256, 256));

        verticalLayout_2->addWidget(leftPicture, 0, Qt::AlignHCenter);

        progressBar_left = new QProgressBar(LeftSIde);
        progressBar_left->setObjectName(QStringLiteral("progressBar_left"));
        progressBar_left->setValue(0);

        verticalLayout_2->addWidget(progressBar_left);


        horizontalLayout_2->addWidget(LeftSIde);

        RightSide = new QWidget(Pictures);
        RightSide->setObjectName(QStringLiteral("RightSide"));
        RightSide->setMinimumSize(QSize(283, 256));
        verticalLayout_3 = new QVBoxLayout(RightSide);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        header_right = new QWidget(RightSide);
        header_right->setObjectName(QStringLiteral("header_right"));
        horizontalLayout_4 = new QHBoxLayout(header_right);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(10, 0, 10, 0);
        btn_Encode = new QPushButton(header_right);
        btn_Encode->setObjectName(QStringLiteral("btn_Encode"));
        sizePolicy1.setHeightForWidth(btn_Encode->sizePolicy().hasHeightForWidth());
        btn_Encode->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(btn_Encode);

        lbl_filesize_encode = new QLabel(header_right);
        lbl_filesize_encode->setObjectName(QStringLiteral("lbl_filesize_encode"));
        lbl_filesize_encode->setMinimumSize(QSize(100, 0));
        lbl_filesize_encode->setMaximumSize(QSize(100, 16777215));
        lbl_filesize_encode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(lbl_filesize_encode);

        lineEdit_filesize_encode = new QLineEdit(header_right);
        lineEdit_filesize_encode->setObjectName(QStringLiteral("lineEdit_filesize_encode"));
        lineEdit_filesize_encode->setEnabled(false);
        lineEdit_filesize_encode->setMaximumSize(QSize(70, 16777215));
        lineEdit_filesize_encode->setReadOnly(true);

        horizontalLayout_4->addWidget(lineEdit_filesize_encode);


        verticalLayout_3->addWidget(header_right);

        rightPicture = new QWidget(RightSide);
        rightPicture->setObjectName(QStringLiteral("rightPicture"));
        rightPicture->setMinimumSize(QSize(256, 256));
        rightPicture->setMaximumSize(QSize(256, 256));

        verticalLayout_3->addWidget(rightPicture, 0, Qt::AlignHCenter);

        progressBar_right = new QProgressBar(RightSide);
        progressBar_right->setObjectName(QStringLiteral("progressBar_right"));
        progressBar_right->setValue(0);

        verticalLayout_3->addWidget(progressBar_right);


        horizontalLayout_2->addWidget(RightSide);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(1, 1);

        verticalLayout->addWidget(Pictures);

        main_GUI->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(main_GUI);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        main_GUI->setStatusBar(statusBar);

        retranslateUi(main_GUI);

        QMetaObject::connectSlotsByName(main_GUI);
    } // setupUi

    void retranslateUi(QMainWindow *main_GUI)
    {
        main_GUI->setWindowTitle(QApplication::translate("main_GUI", "Qt5Test2", 0));
        groupBox->setTitle(QApplication::translate("main_GUI", "File to Encode", 0));
        lbl_file->setText(QApplication::translate("main_GUI", "File:", 0));
        btn_choose->setText(QApplication::translate("main_GUI", "Choose", 0));
        label_6->setText(QApplication::translate("main_GUI", "Stepsize:", 0));
        label_4->setText(QApplication::translate("main_GUI", "Dimensions:", 0));
        label_5->setText(QApplication::translate("main_GUI", "x", 0));
        groupBox_2->setTitle(QApplication::translate("main_GUI", "Encoding Options", 0));
        label_2->setText(QApplication::translate("main_GUI", "Subsampling", 0));
        checkBox_SubsamplingAverage->setText(QApplication::translate("main_GUI", "average", 0));
        ComboBox_Subsampling->setProperty("currentText", QVariant(QString()));
        label_3->setText(QApplication::translate("main_GUI", "Quantization Table", 0));
#ifndef QT_NO_TOOLTIP
        ComboBox_QTable->setToolTip(QApplication::translate("main_GUI", "No Tooltip!", 0));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("main_GUI", "Scale Images in View", 0));
        checkBox_scaleRightImage->setText(QString());
        btn_loadPPM->setText(QApplication::translate("main_GUI", "Load", 0));
        lbl_filesize_load->setText(QApplication::translate("main_GUI", "File size:", 0));
        btn_Encode->setText(QApplication::translate("main_GUI", "Encode", 0));
        lbl_filesize_encode->setText(QApplication::translate("main_GUI", "File size:", 0));
    } // retranslateUi

};

namespace Ui {
    class main_GUI: public Ui_main_GUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAIN_GUI_H
