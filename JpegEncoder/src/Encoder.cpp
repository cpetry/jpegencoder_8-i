/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include <stdio.h>
#include <iostream>
#include <boost\numeric\ublas\matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

#include "Encoder.h"

#include <chrono>
#include "DCT_Variante1.h"
#include "DCT_Variante2.h"
#include "DCT_Variante3.h"
#include "ppmFile.h"
#include "jpegSegments.h"
#include "subSamplingScheme.h"

void Encoder::encode(std::string file, std::string subsampling, bool averaging){
	picture pic;

	// 1. reading file
	ppmFile::readFileBytes(file, pic, 16);
	
	// 2. changing colorspace
	pic.changeColorSpaceToYCbCr();

	// 3. subsampling
	pic.subsampleColorChannels(subSamplingScheme(subSamplingScheme::stringToEnum(subsampling), false));

	std::string output = file.substr(0, file.find_last_of('.'));
	output += ".jpg";
	jpegSegments jpeg;
	jpeg.writeSegments(pic, output);
}

void Encoder::perfTest(){
	picture pic(256, 256);
	for (int y = 0; y < 256; y++)
	for (int x = 0; x < 256; x++)
		pic.setR_Y(x, y, (x+y*8) % 256);

	// fetching a part of the picture
	matrix<float> r(256, 256);
	r = pic.getMatrixY();

	std::cout << "Testing performance!" << std::endl;
	std::cout << "Size: " << pic.getWidth() << " x " << pic.getHeight() << std::endl << std::endl;

	for (unsigned int i = 1; i <= 3; i++){
		std::cout << "Variante " << i << ": ";

		void(*func_ptr)(matrix<float>&);
		i == 1 ? func_ptr = DCT_Variante1::transform :
			(i == 2 ? func_ptr = DCT_Variante2::transform :
		func_ptr = DCT_Variante3::transform);
		int iter = 0;
		std::chrono::milliseconds duration = (std::chrono::milliseconds)0;
		auto begin = std::chrono::steady_clock::now();
		while (duration < (std::chrono::milliseconds)10000){
			(*func_ptr)(r);
			auto end = std::chrono::steady_clock::now();
			iter++;
			duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
		}
		iter = iter;

		std::cout << duration.count() * 1.0f / iter << "ms (" << iter << " Iterationen)" << std::endl;
	}
	std::cout.flush();
}