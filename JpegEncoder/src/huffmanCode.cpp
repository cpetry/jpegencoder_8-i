/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "huffmanCode.h"

#include "picture.h"
#include <string>
#include "bitstream.h"
#include <queue>

/////////////////
// Huffman functions

HuffMap huffmanCode::createHuffmanCodes(std::map<int, int> frequencies, bool grow_right, bool eleminate_one_star, int length_limit)
{
	const int value_range = 256;

	Node* root = Node::createTree(frequencies, value_range);

	HuffMap code;

	if (length_limit != 0)
		lengthLimit(root, length_limit);

	if (grow_right)
		//huffmanCode::growRight(root);
		growRightPert(root);

	if (eleminate_one_star)
		eleminateOneStar(root);

	Node::generate(root, HuffVect(), code);

	delete root;

	return code;
}

HuffmanPairVector huffmanCode::createHuffmanPair(HuffMap h){
	HuffmanPairVector chars;

	// for every character in the huffman code
	for (auto it = h.begin(); it != h.end(); ++it){
					
		std::vector<char> bits;
		int value = it->first;
		// read every bit of character
		for (auto i = it->second.begin(); i != it->second.end(); ++i){
			if (*i)
				bits.push_back('1');
			else 
				bits.push_back('0');
		}
		std::pair<int, std::vector<char>> p(value,bits);
		chars.push_back(p);
	}
	return chars;
}

void huffmanCode::eleminateOneStar(Node* root){
	InternalNode* parent = dynamic_cast<InternalNode*>(root);
	if (parent == NULL)
		return;

	std::vector<Node*> parent_list;
	Leaf* last_leaf = NULL;
	parent_list.push_back(root);
	
	while (InternalNode* intNode = dynamic_cast<InternalNode*>(parent->getRight())){
		parent_list = createChildList(parent_list);
		for (Node* n : parent_list)
			if (dynamic_cast<Leaf*>(n))
				last_leaf = dynamic_cast<Leaf*>(n);
		parent = intNode;
	}
	
	/*
	Leaf* rightmost = dynamic_cast<Leaf*>(parent->getRight());
	if (last_leaf){
		InternalNode* p = last_leaf->getParent();
		InternalNode* new_node = new InternalNode(last_leaf, new Leaf(rightmost->getFrequency(), rightmost->getValue()));
		last_leaf == p->getLeft() ? p->setLeft(new_node) : p->setRight(new_node);
		parent->setRight(NULL);
	}*/
	//else if (parent->getRight()){
		Leaf* not_anymore_right_most = new Leaf(dynamic_cast<Leaf*>(parent->getRight())->getFrequency(), dynamic_cast<Leaf*>(parent->getRight())->getValue());
		InternalNode* p = new InternalNode(not_anymore_right_most, NULL);
		not_anymore_right_most->setParent(p);
		parent->setRight(p);
	//}
}

void huffmanCode::growRightPert(Node* root)
{
	if (root == NULL)
		return;
	
	std::vector<Node*> parent_list;
	std::vector<Node*> child_list;
	parent_list.push_back(root);

	do
	{
		// creating child list
		child_list = createChildList(parent_list);

		// searching for a Leaf inside the child list
		for (unsigned int i = 0; i < child_list.size() && !child_list.empty(); ++i)
		{
			// if the child is a leaf -> swap it with the next internal node
			if (dynamic_cast<Leaf*>(child_list[i]))
				searchAndSwapWithInternalNode(parent_list, child_list, i);
		}
		// parent list is now child list
		parent_list = child_list;

	}while (!child_list.empty());
}

void huffmanCode::searchAndSwapWithInternalNode(std::vector<Node*> parent_list, std::vector<Node*> &child_list, int pos){

	// search left of leaf for the next InternalNode
	for (unsigned int j = pos + 1; j<child_list.size(); ++j)
	{
		if (dynamic_cast<InternalNode*>(child_list[j]))
		{
			//Swap nodes

			InternalNode* node1 = child_list[pos]->getParent();
			InternalNode* node2 = child_list[j]->getParent();
			
			child_list[pos] == node1->getLeft() ? node1->setLeft(child_list[j]) : node1->setRight(child_list[j]);
			child_list[j] == node2->getLeft() ? node2->setLeft(child_list[pos]) : node2->setRight(child_list[pos]);

			child_list[j]->setParent(node1);
			child_list[pos]->setParent(node2);

			std::swap(child_list[pos], child_list[j]);

			j = child_list.size();
		}
	}
}

std::vector<Node*> huffmanCode::createChildList(std::vector<Node*> parent_list){
	std::vector<Node*> child_list;
	// builds a list of all children nodes
	for (unsigned int i = 0; i < parent_list.size(); i++)
	{
		InternalNode* current = dynamic_cast<InternalNode*>(parent_list[i]);
		if (current == NULL)
			continue;
		if (current->getRight() != NULL)
			child_list.push_back(current->getRight());
		if (current->getLeft() != NULL)
			child_list.push_back(current->getLeft());
	}
	return child_list;
}

void huffmanCode::growRight(Node* root)
{
    InternalNode* current_parent = dynamic_cast<InternalNode*>(root);
    // if root node is leaf
    if(current_parent == NULL)
        return;
    // else
    int depth = 0;
    std::vector<Node*> parent_list;
    std::vector<Node*> child_list;
    parent_list.push_back(root);

    do
    {
        child_list.clear();

        // builds a list of all children nodes
        for(Node* parent : parent_list)
        {
            InternalNode* current = dynamic_cast<InternalNode*>(parent);
            if(current == NULL)
                continue;
            child_list.push_back(current->getRight());
            child_list.push_back(current->getLeft());
        }

        if(!child_list.empty())
        {
			for (unsigned int i = 0; i<child_list.size(); ++i)
            {
                // if the child is a leaf -> swap it with the next internal node
                if(dynamic_cast<Leaf*>(child_list[i]))
                {
					for (unsigned int j = i + 1; j<child_list.size(); ++j)
                    {
                        if(dynamic_cast<InternalNode*>(child_list[j]))
                        {
                            InternalNode* node1 = dynamic_cast<InternalNode*>(child_list[i]->getParent());
                            InternalNode* node2 = dynamic_cast<InternalNode*>(child_list[j]->getParent());

                            child_list[i] == node1->getLeft() ? node1->setLeft(child_list[j]) : node1->setRight(child_list[j]);
                            child_list[j] == node2->getLeft() ? node2->setLeft(child_list[i]) : node2->setRight(child_list[i]);

                            child_list[j]->setParent(node1);
                            child_list[i]->setParent(node2);

                            std::swap(child_list[i], child_list[j]);

                            j = child_list.size();
                        }
                    }
                }

            }
        }
        parent_list = child_list;


    }while(!child_list.empty());
}

void huffmanCode::lengthLimit(Node* root, int max_depth){
	// if tree has more elements as 2^max_depth
	// OR tree height <= max_depth
	//	just return
	if (root->getHeight() <= max_depth || root->getNumberOfElements() > pow(2, max_depth))
		return;	
	
	// delete_list = leafs that have to be cut
	// free_space = positions where enough space is free (sorted by height in tree)
	std::vector<Node*> parent_list;
	std::vector<Node*> delete_list;
	std::priority_queue<std::pair<int, Node*>, std::vector<std::pair<int, Node*>>, pairCompare> free_space;
	parent_list.push_back(root);
	int depth = 0;
	while (!parent_list.empty()){
		depth++;
		std::vector<Node*> child_list = createChildList(parent_list);
		std::vector<Node*> new_parent_list;

		for (Node* c : child_list){
			if (dynamic_cast<Leaf*>(c)){
				// All leaves above max_depth have some free space
				if (depth < max_depth){
					free_space.push(std::pair<int, Node*>(depth, c));
				}
				// All leaves beneath max_depth are to be cut!
				else if (depth > max_depth){
					delete_list.push_back(c);
				}
			}
			// only internal nodes are parents in next iteration
			if (dynamic_cast<InternalNode*>(c)){
				new_parent_list.push_back(c);
				// InternalNode at max_depth is a free space!
				if (depth == max_depth)
					free_space.push(std::pair<int, Node*>(depth, c));
			}
		}
		parent_list = new_parent_list;
	}

	// relocating leafs, filling up free space
	for (unsigned int i = 0; i < delete_list.size(); i++){
		// Get the lowest free space that exists
		std::pair<int, Node*> node = free_space.top();
		free_space.pop();

		// If free space is a InternalNode -> fill it up
		if (dynamic_cast<InternalNode*>(node.second)){
			InternalNode* p = node.second->getParent();
			node.second == p->getLeft() ? p->setLeft(delete_list[i]) : p->setRight(delete_list[i]);
			// Still free space left? -> put it in the list
			if (node.first < max_depth)
				free_space.push(std::pair<int, Node*>(node.first + 1, delete_list[i]));
		}

		// If free space is a Leaf -> create an InternalNode with it
		else if (dynamic_cast<Leaf*>(node.second)){
			InternalNode* parent = node.second->getParent();
			InternalNode* pair = new InternalNode(delete_list[i], node.second);
			node.second == parent->getLeft() ? parent->setLeft(pair) : parent->setRight(pair);
			// Still free space left? -> put it in the list
			if (node.first + 1 < max_depth){
				free_space.push(std::pair<int, Node*>(node.first + 1, delete_list[i]));
				free_space.push(std::pair<int, Node*>(node.first + 1, node.second));
			}
		}
	}

}

bool pairCompare::operator()(const std::pair<int, Node*>& firstElem, const std::pair<int, Node*>& secondElem) {
	return firstElem.first < secondElem.first;
}

/////////////////
// Node functions

Node* Node::createTree(std::map<int, int> frequencies, int value_range){
	std::priority_queue<Node*, std::vector<Node*>, NodeCompare> tree;

	for (auto it = frequencies.begin(); it != frequencies.end(); it++){
	    int v = it->first;
		tree.push(new Leaf(it->second, v));
	}
	
	while (tree.size() > 1){
		Node* childR = tree.top();
		tree.pop();

		Node* childL = tree.top();
		tree.pop();

		InternalNode* parent = new InternalNode(childR, childL);
		childR->setParent(parent);
		childL->setParent(parent);
		tree.push(parent);
	}
	if (tree.size() > 0)
		return tree.top();
	else 
		return NULL;
}


void Node::generate(Node* node, const HuffVect& prefix, HuffMap& code){
	if (const Leaf* leaf = dynamic_cast<const Leaf*>(node)){
		if (prefix.size() == 0) // if root is a leaf (only one element in huffman tree)
			code[leaf->getValue()] = HuffVect(1);
		else
	        code[leaf->getValue()] = prefix;
	}
	else if (InternalNode* intNode = dynamic_cast<InternalNode*>(node)){
		
        if (intNode->getLeft() != NULL){
            HuffVect leftPrefix = prefix;
		    leftPrefix.push_back(false);
		    generate(intNode->getLeft(), leftPrefix, code);
        }
		if (intNode->getRight() != NULL){
			HuffVect rightPrefix = prefix;
			rightPrefix.push_back(true);
			generate(intNode->getRight(), rightPrefix, code);
		}
	}
}


Node::Node(int f) : f(f) { parent = NULL; }
Node::~Node(){}

void Node::setParent(InternalNode* parent)
{
    this->parent = parent;
}

InternalNode* Node::getParent()
{
    return this->parent;
}

int Node::getFrequency(){
	InternalNode* in = dynamic_cast<InternalNode*>(this);
	Leaf* l = dynamic_cast<Leaf*>(this);
	if (in){
		int freq = 0;
		if (in->getLeft())
			freq += in->getLeft()->getFrequency();
		if (in->getRight())
			freq += in->getRight()->getFrequency();
		return freq;
	}
	if (l)
		return f;
	return f;
}

void Node::setFrequency(int f){
	this->f = f;
}

const int Node::getHeight(){
	std::vector<Node*> parent_list;
	parent_list.push_back(this);

	int depth = 0;

	while (!parent_list.empty()){
		std::vector<Node*> new_parent_list;
		for (Node* parent : parent_list)
		{
			InternalNode* current = dynamic_cast<InternalNode*>(parent);
			if (current == NULL)
				continue;

			if (current->getRight())
				new_parent_list.push_back(current->getRight());
			if (current->getLeft())
				new_parent_list.push_back(current->getLeft());
		}
		if (!new_parent_list.empty())
			depth++;

		parent_list = new_parent_list;
	}

	return depth;
}

const int Node::getNumberOfElements(){
	int number = 0;
	InternalNode* in = dynamic_cast<InternalNode*>(this);
	if (in){
		number += in->getLeft()->getNumberOfElements();
		number += in->getRight()->getNumberOfElements();
		return number;
	}
	else if (dynamic_cast<Leaf*>(this))
		return 1;
	else
		return 1;
}



/////////////////
// InternalNode functions
// ATTENTION! nasty little initializer function :-~
InternalNode::InternalNode(Node* i0, Node* i1)
    : Node(), left(i0), right(i1) {
	f = 0;
	if (i0) {
		i0->setParent(this);
		f += i0->getFrequency();
	}
	if (i1){
		i1->setParent(this);
		f += i1->getFrequency();
	}
	this->setFrequency(f);
}

InternalNode::~InternalNode()
{
    //delete left;
    //delete right;
}

Node* InternalNode::getLeft() { 
	return left; 
}
Node* InternalNode::getRight() { 
	return right; 
}


/////////////////
// Leaf functions
const int Leaf::getValue() const{
    return i;
}

Leaf::Leaf(int f, int i) 
	: Node(f), i(i) {
}


/////////////////
// NodeCompare
bool NodeCompare::operator() (Node* leftNode, Node* rightNode) const {
    return leftNode->getFrequency() > rightNode->getFrequency();
}

