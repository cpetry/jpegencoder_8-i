/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "bitstream.h"
#include "picture.h"
#include <fstream>

void bitstream::alloc_space()
{
    if (bits_used == 0) {
		// push one empty byte to storage
        BYTE push = 0x00;
        storage.push_back(push);
    }
}

void bitstream::push_sos_datastream(bitstream bs){
	int count_1s = 0; // to 
	int byte_count = 0;

	for (unsigned int i = 0; i < bs.size(); i++){
		if (bs[i] == true)
			count_1s++;
		else
			count_1s = 0;

		byte_count++;
		push_bit(bs[i]);
		
		if (count_1s == 8){ // FF!
			for (int j = 0; j < 8; j++)
				push_bit(false);
			count_1s = 0;
		}

		if (byte_count == 8){
			count_1s = 0;
			byte_count = 0;
		}
	}
}

void bitstream::push_stream(bitstream bs){
	for (unsigned int i = 0; i < bs.size(); i++){
		push_bit(bs[i]);
	}
}

void bitstream::push_bit(bool bit)
{
	// test if another byte has to be allocated
    alloc_space();
    storage.back() |= bit << (7 - bits_used++);
}

void bitstream::push_byte(unsigned char byte){
	if (bits_used > 0){
		for(int i = 0; i < 8; i++){
			if (byte >> 7 == 1)
				push_bit(true);
			else
				push_bit(false);
			byte = byte << 1;
		}
	}
	else
		storage.push_back(byte);
}


BYTE *bitstream::get_array()
{
    return &storage.front();
}

bit_size bitstream::size() const
{
	bit_size size = 0;
    if (bits_used == 0)	// if no bit is used
		size = storage.size() * 8;
	else
		size = (storage.size()-1) * 8 + bits_used;

    return std::max(bit_size(0), size);
}

bool bitstream::operator[](bit_size size) const
{
    return static_cast<bool>((storage[size / 8] >> (7 - (size % 8))) & 0x1);
}

BYTE bitstream::getByteAt(unsigned int byte_pos){
	if (byte_pos >= storage.size())
		return 0;

	return storage.at(byte_pos);
}

void bitstream::writeToFile(const char* file){

	// fill the last byte with 1s
	fillByte();

	FILE *f = fopen(file, "wb");
	fwrite(this->get_array(), storage.size(), 1, f);	// the whole storage in one go!
    fflush(f);
	fclose(f);
    
}

void bitstream::readFromFile(char* file){
	std::ifstream input;
	input.open(file);

	// get length of file
	
	long long begin = input.tellg();
	input.seekg (0, std::ios::end);
	long long end = input.tellg();
	long long file_size = end - begin;

	input.seekg(0);
	
	const int chunksize = 32;
	int bytesread = 0;
	char c[chunksize];
	if (input.is_open()){
		while(bytesread + chunksize < file_size){
			input.read(c, chunksize);
			if (input.eof() || input.fail())
				break;

			bytesread += chunksize;
			for (int i = 0; i < chunksize; i++)
				this->push_byte(c[i]);
		}

		char r = 0;
		while(!input.eof() && !input.fail()){
			input.read(&r, 1);
			this->push_byte(r);
		}
	}

	input.close();
}

void bitstream::fillByte(bool bit){
	// fill the last byte with 1s
	while (bits_used != 0){
		int bit_nmb = 0;
		if (bit)
			bit_nmb = 1;
		storage.back() |= bit_nmb << (7 - bits_used++);
	}
}
