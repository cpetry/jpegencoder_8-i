/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/
#include "iDCT.h"


#define _USE_MATH_DEFINES 
#include <math.h>

#include <boost/numeric/ublas/matrix_proxy.hpp> // matrix_range


float sqrt2 = 1.0f / sqrtf(2.0f);
float pi2N = static_cast<float>(M_PI) / 16.0f;

void iDCT::transform(matrix<float> &Y){
	for (unsigned int y = 0; y <= Y.size2() - 8; y += 8)
	for (unsigned int x = 0; x <= Y.size1() - 8; x += 8){
		matrix_range<matrix<float>> mr(Y, range(x, x + 8), range(y, y + 8));
		matrix<float> X(8, 8);

		for (int y = 0; y < 8; y++){
			for (int x = 0; x < 8; x++){

				float value = 0;
				for (int j = 0; j < 8; j++){
					for (int i = 0; i < 8; i++){
						float Ci = (i == 0 ? sqrt2 : 1.0f);
						float Cj = (j == 0 ? sqrt2 : 1.0f);

						float cos1 = cos((2.0f * x + 1) * i * pi2N);
						float cos2 = cos((2.0f * y + 1) * j * pi2N);
						value += (0.25f * Ci * Cj * Y(i, j) * cos1 * cos2);
					}
				}

				X(x, y) = value;
			}
		}
		mr = X;
	}
}