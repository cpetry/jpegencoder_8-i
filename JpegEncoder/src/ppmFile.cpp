/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "ppmFile.h"

#include <fstream>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

void ppmFile::readFileBytes(std::string file, picture &pic, unsigned int stepsize){
	std::ifstream input;
    
	std::vector<std::string> dimensions;
    std::vector<std::string> color;

	const int size = 256;

	input.open(file);
    if(input.is_open()){
		
		char format[2];
		input.read(format, 2);

		if (format[0] != 'P' && format[1] != '3'){
			return;
		}
		ignoreSeperators(&input);
		ignoreComments(&input);
		ignoreSeperators(&input);
		int width = readInt(&input);
		ignoreSeperators(&input);
		int height = readInt(&input);
		ignoreSeperators(&input);

		int absolute_width = (width % stepsize != 0)	? ((width / stepsize) + 1) * stepsize	: width;
		int absolute_height = (height % stepsize != 0)	? ((height / stepsize) + 1) * stepsize	: height;

		int maxbrightness = readInt(&input);
		ignoreSeperators(&input);

		if (maxbrightness != 255)
			return;

		pic = picture(absolute_width, absolute_height);
        int pix_pos = 0;
		int max_pix_pos = width * height - 1;
		/*
		int pos_now = input.tellg();
		input.seekg(pos_now, std::ios::end);
		long file_pix_size = input.tellg();
		input.seekg(pos_now);*/
		long filesizeread = 0;
		const int chunksize = 256;

		
		char color[3];
		int colorpos = 0;
		int colValue = 0;
		int colorCount = 0;
		
		while(input){
			char chunk[chunksize];
			input.read(chunk, sizeof(chunk));
			filesizeread += chunksize;

			
			for (char c : chunk){
				if (c == ' ' || c == '\n'){
					if (colorpos != 0){
						if (colorCount == 0){
							pic.setR_Y(pix_pos % width, pix_pos / width, boost::lexical_cast<int>(color, colorpos));
							colorCount++;
						}
						else if (colorCount == 1){
							pic.setG_Cb(pix_pos % width, pix_pos / width, boost::lexical_cast<int>(color, colorpos));
							colorCount++;
						}
						else if (colorCount == 2){
							pic.setB_Cr(pix_pos % width, pix_pos / width, boost::lexical_cast<int>(color, colorpos));
							colorCount = 0;
							if (max_pix_pos == pix_pos)
								break;
							pix_pos++;
						}
					}
					colorpos = 0;
				}
				else{
					color[colorpos] = c;
					colorpos++;
				}
			} // for
		}; // while input


		// repeating pixel
		// right
		for (int x = width; x < absolute_width; x++)
			for (int y = 0; y < height; y++){
				pic.setR_Y(x, y, pic.getR(width-1, y));
				pic.setG_Cb(x, y, pic.getG(width-1, y));
				pic.setB_Cr(x, y, pic.getB(width-1, y));
			}
		
		// bottom
		for (int y = height; y < absolute_height; y++)
			for (int x = 0; x < width; x++){
				pic.setR_Y(x, y, pic.getR(x, height-1));
				pic.setG_Cb(x, y, pic.getG(x, height-1));
				pic.setB_Cr(x, y, pic.getB(x, height-1));
			}

		// bottomright corner
		for (int y = height; y < absolute_height; y++)
			for (int x = width; x < absolute_width; x++){
				pic.setR_Y(x, y, pic.getR(width-1, height-1));
				pic.setG_Cb(x, y, pic.getG(width-1, height-1));
				pic.setB_Cr(x, y, pic.getB(width-1, height-1));
			}

		return;
	} // if input.open()
	return;
}

void ppmFile::writeFileBytes(std::string file, picture &pic){
   	std::ofstream output;
    boost::replace_last(file, ".", "_encoded.");
    output.open(file);

    output << "P3\n";
    output << pic.getWidth() << " " << pic.getHeight() << "\n";
    output << std::to_string(255) << "\n";
    
    for(unsigned int y = 0; y < pic.getHeight(); y++){
        for(unsigned int x = 0; x < pic.getWidth(); x++){
            output << std::to_string(static_cast<int>(pic.getR(x, y))) << " ";
            output << std::to_string(static_cast<int>(pic.getG(x, y))) << " ";
            output << std::to_string(static_cast<int>(pic.getB(x, y)));
            if (x != pic.getWidth() -1)
                output << " ";
        }
        output << "\n";
    }
    output.close();
}

int ppmFile::readInt(std::ifstream *input){
	char buffer[4];
	char next = input->peek();
	int pos = 0;
	while(next != ' ' && next != '\n'){
		input->get(buffer[pos++]);
		next = input->peek();
	};
	int retValue = boost::lexical_cast<int>(buffer, pos);
	return retValue;
}

void ppmFile::ignoreSeperators(std::ifstream *input){
	char next = input->peek();
	// ignoring all whitespaces
	while((next == ' ' || next == '\n')){
		input->ignore(1);
		next = input->peek();
	};
}

void ppmFile::ignoreComments(std::ifstream *input){
	char next = input->peek();
	// ignoring all whitespaces
	while(next == '#'){
		while(next != '\n'){
			input->ignore(1);
			next = input->peek();
		};
	};
}