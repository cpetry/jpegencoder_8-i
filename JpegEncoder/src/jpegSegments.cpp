/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "jpegSegments.h"

#include <string>
#include <queue>

#include "picture.h"
#include "Quantization.h"

/**
* @brief	is needed to sort the entries
*/
bool sortHuffmanEntries(std::pair<int, BYTE> i, std::pair<int, BYTE> j) { return i.first < j.first; }


void jpegSegments::writeSegments(picture &pic, std::string filename, Quantization::Q_Table q_table){
	picture::COLORSPACE a = pic.getColorSpace();
	std::vector<bitstream> y_blocks, cb_blocks, cr_blocks;
	HuffMap dc_y_tree, ac_y_tree, dc_cbcr_tree, ac_cbcr_tree;
	
	encoding::createYBitstreamsAndTrees(pic.getMatrixY(), pic.getSubSamplingScheme().scheme, q_table, dc_y_tree, ac_y_tree, y_blocks);
	encoding::createCbCrBitstreamsAndTrees(pic.getMatrixCb(), pic.getMatrixCr(), q_table, dc_cbcr_tree, ac_cbcr_tree, cb_blocks, cr_blocks);

	std::vector<HuffMap> trees;
	trees.push_back(dc_y_tree);
	trees.push_back(ac_y_tree);
	trees.push_back(dc_cbcr_tree);
	trees.push_back(ac_cbcr_tree);

    SOI();
    APP0();
	DQT(q_table);
    SOF0(pic);
	DHT(trees);
	SOS(y_blocks, cb_blocks, cr_blocks, pic);
    EOI();
	
    bs.writeToFile(filename.c_str());
}

void jpegSegments::SOI(){
    bs.push_byte(0xff);
    bs.push_byte(0xd8);
}

void jpegSegments::APP0(){
    bs.push_byte(0xff);
    bs.push_byte(0xe0);

    // Length of segment (2 Bytes)
    bs.push_byte(0x00);
    bs.push_byte(0x10);
    

    // "JFIF" JPEG File Interchange Format 
    bs.push_byte(0x4a);
    bs.push_byte(0x46);
    bs.push_byte(0x49);
    bs.push_byte(0x46);
    bs.push_byte(0x00);

    // Major revision number
    bs.push_byte(0x01);
    // Minor revision number
    bs.push_byte(0x01);

    // Size of a pixel
    bs.push_byte(0x00);

    // x and y density ( each 2 bytes)
    bs.push_byte(0x00);
    bs.push_byte(0x48);
    bs.push_byte(0x00);
    bs.push_byte(0x48);

    // Thumbnail size x/y and amout of bytes
    bs.push_byte(0x00);
    bs.push_byte(0x00);

    // byte for thumbnail
    //bs.push_byte(0x00);
}

void jpegSegments::SOF0(picture &pic){
    bs.push_byte(0xff);
    bs.push_byte(0xc0);
    
    // length of segment (2 Bytes) : 8 + 3 * components (Y | Y, Cb, Cr) = 17
    bs.push_byte(0x00);
    bs.push_byte(0x11); // 17 (for three components)

    bs.push_byte(0x08); // 8 bits/sample

    // picture width, height (each 2 byte)
	bs.push_byte(pic.getHeight() / 256);
	bs.push_byte(pic.getHeight() % 256);
    bs.push_byte(pic.getWidth() / 256);
    bs.push_byte(pic.getWidth() % 256);

    // number of components (we choose 3)
    bs.push_byte(0x03);

	int subsampling = pic.getSubSamplingScheme().div_hori * 16 + pic.getSubSamplingScheme().div_vert;

    // Y
    bs.push_byte(0x01); // ID
	bs.push_byte(subsampling); // subsampling (11 = no subsampling)
    bs.push_byte(0x00); // number of quantisizing table
	
	// Cb
    bs.push_byte(0x02); // ID
	bs.push_byte(0x11); // subsampling (11 = no subsampling)
    bs.push_byte(0x01); // number of quantisizing table

    // Cr
    bs.push_byte(0x03); // ID
	bs.push_byte(0x11); // subsampling (11 = no subsampling)
    bs.push_byte(0x01); // number of quantisizing table
	
}

void jpegSegments::DHT(std::vector<HuffMap> &trees){

	// DHT marker ( 2 Bytes )
    bs.push_byte(0xff);
    bs.push_byte(0xc4);

    // length of segment ( 2 Bytes )
    bs.push_byte(0x00);
	// headerinfo + 17 + tree elements
	int treesize = 0;
	for (HuffMap t : trees)
		treesize += 17 + t.size();

	bs.push_byte(2 + treesize);

	for (unsigned int i = 0; i<trees.size(); ++i){
        
        //Huffman table information byte (1 Byte)
		bs.push_byte((i % 2) * 16 + (i / 2));


		// Creating a huffman table with symbols sorted by huffman-bit-length (0-16)
		// int: huffman-bit-length (0-16)
		// vector: symbols with same huffman-bit-length
		std::map<int, std::vector<std::pair<int, BYTE>>> huffman_table;
		for (HuffMap::iterator it = trees[i].begin(); it != trees[i].end(); ++it){
			std::pair<int, BYTE> bitValue_Value = std::pair<int, BYTE>(encoding::getIntegerOfBitCode(it->second), it->first);
			huffman_table[it->second.size()].push_back(bitValue_Value);
		}


		// number of elements with codelength l ( 16 Byte )
		for (int l = 1; l <= 16; ++l){
			bs.push_byte(huffman_table[l].size());
		}

		// symbols with codelength l	( n Byte )
		for (int l = 1; l <= 16; ++l){
			std::sort(huffman_table[l].begin(), huffman_table[l].end(), sortHuffmanEntries);
			for (std::pair<int,BYTE> bitValue_Value : huffman_table[l])
				bs.push_byte(bitValue_Value.second);
		}
			
    }
}

void jpegSegments::DQT(Quantization::Q_Table q_table){
	bs.push_byte(0xff);
	bs.push_byte(0xdb);
	
	// length of segment (2 Bytes) : (1 + 64) * 2
	bs.push_byte(0x00);
	bs.push_byte(0x84); // 132


	/////////////////////
	// Quantization Table LUMINANCE

	// first 4 bits: precision (8 bit = 0, else 16 bit)
	// second 4 bits: number of quantization table
	bs.push_byte(0 * 16 + 0);

	matrix<int> q_matrix = Quantization::getQTable(q_table, Quantization::Q_Channel::LUMINANCE);

	// write table (zig-zaged)
	std::vector<int> zigzag;
	zigzag = encoding::zigzagMatrix(q_matrix);
	for (int v : zigzag)
		bs.push_byte(v);

	///////////////////

	/////////////////////
	// Quantization Table CHROMINANCE
	// first 4 bits: precision (8 bit = 0, else 16 bit)
	// second 4 bits: number of quantization table
	bs.push_byte(0 * 16 + 1);

	q_matrix = Quantization::getQTable(q_table, Quantization::Q_Channel::CHROMINANCE);
	// write table (zig-zaged)
	zigzag = encoding::zigzagMatrix(q_matrix);
	for (int v : zigzag)
		bs.push_byte(v);

	//////////////

}

void jpegSegments::SOS(std::vector<bitstream> &y_bs, std::vector<bitstream> &cb_bs, std::vector<bitstream> &cr_bs, picture &pic){
	bs.push_byte(0xff);
	bs.push_byte(0xda);
	
	// length of segment (2 Bytes) : 6 + 2 * number of components ( Y Cb Cr )
	bs.push_byte(0x00);
	bs.push_byte(0x0C); // C = 12  (3 Components)
		
	//////////////////
	// number of components 
	bs.push_byte(0x03); // y cb cr

	// for each component 2 Byte
	// ID + used HuffmanTable
	// bit 0..3: AC HT (0..3)
	// bit 4..7: DC HT (0..3)
	bs.push_byte(0x01); // y
	bs.push_byte(0x00); // HT: AC = 0, DC = 0   => 0

	bs.push_byte(0x02); // Cb
	bs.push_byte(0x11); // HT: AC = 1, DC = 1   => 16 + 1 = 17

	bs.push_byte(0x03); // Cr
	bs.push_byte(0x11); // HT: AC = 1, DC = 1   => 16 + 1 = 17


	// not important
	bs.push_byte(0x00);
	bs.push_byte(0x3f);
	bs.push_byte(0x00);

	// data
	bitstream data;
	int y_block = 0;
	SUBSAMPLINGSCHEME sss = pic.getSubSamplingScheme().scheme;

	for (unsigned int block = 0; block < cb_bs.size() && block < cr_bs.size(); block++){
		data.push_stream(y_bs[y_block++]);
		if (sss == _422)
			data.push_stream(y_bs[y_block++]);

		else if (sss == _411 || sss == _420){
			data.push_stream(y_bs[y_block++]);
			data.push_stream(y_bs[y_block++]);
			data.push_stream(y_bs[y_block++]);
		}

		data.push_stream(cb_bs[block]);
		data.push_stream(cr_bs[block]);
	}

	data.fillByte(true); // fill up with 1s
	bs.push_sos_datastream(data);
	
		
}

void jpegSegments::EOI(){
    bs.push_byte(0xff);
    bs.push_byte(0xd9);
}


/**
 * Convenience function
 * Creates a 4 bit bool vector out of a int
 */
std::vector<bool> jpegSegments::create4BitVector(int number){
	if (number >= 16)
		return std::vector<bool>(); // return empty vector

	std::vector<bool> k;

	// creating a 4-bit-vector (example: 3 -> 0011)
	while (number){
		if (number & 1) // bitwise AND
			k.push_back(true);
		else
			k.push_back(false);
		number >>= 1; // shift right (next bit)
	}
	for (int j = k.size(); j<4; ++j){
		k.push_back(false);
	}

	return k;
}

