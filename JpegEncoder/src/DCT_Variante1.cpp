/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "DCT_Variante1.h"

#include <thread>

#include <boost/numeric/ublas/matrix_proxy.hpp> // matrix_range

float DCT_Variante1::One_Sqrt = 1.0f / (float)sqrt(2);
float DCT_Variante1::pi_16 = (float)M_PI / 16.0f;

float DCT_Variante1::c_i;
float DCT_Variante1::c_j;
matrix<float> DCT_Variante1::z(8, 8);
float DCT_Variante1::value;
float DCT_Variante1::i_pi_16;
float DCT_Variante1::j_pi_16;

void DCT_Variante1::transform(matrix<float> &m){

	std::thread t0, t1, t2, t3;

	for (unsigned int y = 0; y <= m.size2() - 8; y += 8)
	for (unsigned int x = 0; x <= m.size1() - 8; x += 8){
		matrix_range<matrix<float>> mr(m, range(x, x + 8), range(y, y + 8));

		for (unsigned int i = 0; i < 8; ++i){
			for (unsigned int j = 0; j < 8; ++j){
				value = 0;
				i_pi_16 = i * pi_16;
				j_pi_16 = j * pi_16;
				for (unsigned int y = 0; y < 8; ++y){
					for (unsigned int x = 0; x < 8; ++x){
						value += static_cast<float>(m(x, y) * cos((2.0f * x + 1) * i_pi_16) * cos((2.0f * y + 1) * j_pi_16));
					}
				}

				i == 0 ? c_i = One_Sqrt : c_i = 1;
				j == 0 ? c_j = One_Sqrt : c_j = 1;

				z(i, j) = (0.25f * c_i * c_j * value);
			}
		}
		mr = z;
	}

}