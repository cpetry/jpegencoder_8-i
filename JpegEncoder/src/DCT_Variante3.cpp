/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

#include "DCT_Variante3.h"

#include <thread>

void DCT_Variante3::transform(matrix<float> &m){
	float x0 = 0;
	float x1 = 0;
	float x2 = 0;
	float x3 = 0;
	float x4 = 0;
	float x5 = 0;
	float x6 = 0;
	float x7 = 0;

	float x0n = 0;
	float x1n = 0;
	float x2n = 0;
	float x3n = 0;
	float x4n = 0;
	float x5n = 0;
	float x6n = 0;
	float x7n = 0;

	// fetching a part of original matrix
	for (unsigned int y = 0; y <= m.size2() - 8; y += 8)
	for (unsigned int x = 0; x <= m.size1() - 8; x += 8){

		matrix_range<matrix<float>> mr(m, range(x, x + 8), range(y, y + 8));
		for (int j = 0; j < 2; j++){
			for (int i = 0; i < 8; i++){

				x0 = mr(i, 0);
				x1 = mr(i, 1);
				x2 = mr(i, 2);
				x3 = mr(i, 3);
				x4 = mr(i, 4);
				x5 = mr(i, 5);
				x6 = mr(i, 6);
				x7 = mr(i, 7);


				// first step
				x0n = x0 + x7;
				x1n = x1 + x6;
				x2n = x2 + x5;
				x3n = x3 + x4;
				x4n = x3 - x4;
				x5n = x2 - x5;
				x6n = x1 - x6;
				x7n = x0 - x7;

				x0 = x0n;
				x1 = x1n;
				x2 = x2n;
				x3 = x3n;
				x4 = x4n;
				x5 = x5n;
				x6 = x6n;
				x7 = x7n;

				//second step
				x0n = x0 + x3;
				x1n = x1 + x2;
				x2n = x1 - x2;
				x3n = x0 - x3;
				x4n = -x4 - x5;
				x5n = x5 + x6;
				x6n = x6 + x7;

				x0 = x0n;
				x1 = x1n;
				x2 = x2n;
				x3 = x3n;
				x4 = x4n;
				x5 = x5n;
				x6 = x6n;

				//third step
				x0n = x0 + x1;
				x1n = x0 - x1;
				x2n = x2 + x3;

				x0 = x0n;
				x1 = x1n;
				x2 = x2n;

				//fourth step
				x2n = a1 * x2;
				float special = x4 + x6;
				x4n = -(a2 * x4) - (a5 * special);
				x5n = a3 * x5;
				x6n = (a4 * x6) - (a5 * special);

				x2 = x2n;
				x4 = x4n;
				x5 = x5n;
				x6 = x6n;

				// fifth step
				x2n = x2 + x3;
				x3n = -x2 + x3;
				x5n = x5 + x7;
				x7n = -x5 + x7;

				x2 = x2n;
				x3 = x3n;
				x5 = x5n;
				x7 = x7n;

				// sixth step
				x4n = x4 + x7;
				x5n = x5 + x6;
				x6n = x5 - x6;
				x7n = -x4 + x7;

				x4 = x4n;
				x5 = x5n;
				x6 = x6n;
				x7 = x7n;

				// seventh step
				x0 = s0 * x0;
				x1 = s4 * x1;
				x2 = s2 * x2;
				x3 = s6 * x3;
				x4 = s5 * x4;
				x5 = s1 * x5;
				x6 = s7 * x6;
				x7 = s3 * x7;

				mr(i, 0) = x0;
				mr(i, 1) = x5;
				mr(i, 2) = x2;
				mr(i, 3) = x7;
				mr(i, 4) = x1;
				mr(i, 5) = x4;
				mr(i, 6) = x3;
				mr(i, 7) = x6;

			}
			transpose(mr);	// transpose matrix
		}
	}
}

void DCT_Variante3::transpose(matrix_range<matrix<float>> &mr){
	for (int j = 0; j < 8; j++)
	for (int i = 0; i < j; i++){
			boost::swap(mr(i, j), mr(j, i));
	}
}