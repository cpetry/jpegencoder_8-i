/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "picture.h"

#include <boost/math/special_functions/round.hpp>

using namespace boost::math;

picture::picture(const unsigned int width, const unsigned int height){
    this->width = width;
    this->height = height;
	
    r_y = matrix<BYTE>(width, height);
    g_cb = matrix<BYTE>(width, height);
    b_cr = matrix<BYTE>(width, height);

	colorspace = RGB;

	//  0.299		0.587		0.114 
	// -0.168736   -0.331264    0.5 
	//  0.5		   -0.418688   -0.081312 
	mRGBtoYCbCr = matrix<float>(3, 3);
	mRGBtoYCbCr(0,0) = 0.299f;		mRGBtoYCbCr(0,1) = 0.587f;		mRGBtoYCbCr(0,2) = 0.114f;
	mRGBtoYCbCr(1,0) =-0.168736f;	mRGBtoYCbCr(1,1) =-0.331264f;	mRGBtoYCbCr(1,2) = 0.5f;
	mRGBtoYCbCr(2,0) = 0.5f;		mRGBtoYCbCr(2,1) =-0.418688f;	mRGBtoYCbCr(2,2) =-0.081312f;

	//  1   0         1,402   
	//  1  -0,34414  -0,71414 
	//  1   1,772     0       

	mYCbCrtoRGB = matrix<float>(3, 3);
	mYCbCrtoRGB(0,0) = 1.0f;	mYCbCrtoRGB(0,1) = 0;				mYCbCrtoRGB(0,2) = 1.40200f;
	mYCbCrtoRGB(1,0) = 1.0f;	mYCbCrtoRGB(1,1) =-0.34414f;		mYCbCrtoRGB(1,2) = -0.71414f;
	mYCbCrtoRGB(2,0) = 1.0f;	mYCbCrtoRGB(2,1) = 1.77200f;		mYCbCrtoRGB(2,2) = 0;
}

unsigned int picture::getWidth() { 
    return width; 
}

unsigned int picture::getHeight() {
    return height; 
}

BYTE picture::getR(const unsigned int x, const unsigned int y) {
	if (colorspace == RGB){
		unsigned int vx = std::min(x, width-1);
		unsigned int vy = std::min(y, height-1);
		return r_y(vx, vy);
	}
	else{
	    int Y = getY(x,y);
	    int Cb = getCb(x,y) - 128;
	    int Cr = getCr(x,y) - 128;
        float round = 0.5f;
		float R = mYCbCrtoRGB(0,0) * Y + mYCbCrtoRGB(0,1) * Cb + mYCbCrtoRGB(0,2) * Cr;
        return static_cast<BYTE>(std::max(0, std::min(static_cast<int>(R + round),255))); // clamping: 0 <= R <= 255
	}
}

BYTE picture::getG(const unsigned int x, const unsigned int y) { 
    if (colorspace == RGB){
		unsigned int vx = std::min(x, width-1);
		unsigned int vy = std::min(y, height-1);
		return g_cb(vx, vy);
	}
	else{
	    int Y = getY(x,y);
	    int Cb = getCb(x,y)-128;
	    int Cr = getCr(x,y)-128;
        float round = 0.5f;
        float G = mYCbCrtoRGB(1,0) * Y + mYCbCrtoRGB(1,1) * Cb + mYCbCrtoRGB(1,2) * Cr;
        return static_cast<BYTE>(std::max(0, std::min(static_cast<int>(G + round),255))); // clamping: 0 <= G <= 255
    }
}

BYTE picture::getB(const unsigned int x, const unsigned int y) {
    if (colorspace == RGB){
		unsigned int vx = std::min(x, width-1);
		unsigned int vy = std::min(y, height-1);
		return b_cr(vx, vy);
	}
	else{
	    int Y = getY(x,y);
	    int Cb = getCb(x,y)-128;
	    int Cr = getCr(x,y)-128;
        float round = 0.5f;
        float B = mYCbCrtoRGB(2,0) * Y + mYCbCrtoRGB(2,1) * Cb + mYCbCrtoRGB(2,2) * Cr;
        return static_cast<BYTE>(std::max(0, std::min(static_cast<int>(B + round),255))); // clamping: 0 <= B <= 255
    }
}

int picture::getY(const unsigned int x, const unsigned int y){
	if (colorspace == YCBCR){
		unsigned int vx = std::min(x, width-1);
		unsigned int vy = std::min(y, height-1);
		return r_y(vx, vy);
	}
	else
		return 0;
}
int picture::getCb(const unsigned int x, const unsigned int y){
	if (colorspace == YCBCR){
		unsigned int vx = std::min(x/subsample_scheme.div_hori, width/subsample_scheme.div_hori-1);
		unsigned int vy = std::min(y/subsample_scheme.div_vert, height/subsample_scheme.div_vert-1);
		return g_cb(vx, vy);
	}
	else
		return 0;
}
int picture::getCr(const unsigned int x, const unsigned int y){
	if (colorspace == YCBCR){
		unsigned int vx = std::min(x/subsample_scheme.div_hori, width/subsample_scheme.div_hori-1);
		unsigned int vy = std::min(y/subsample_scheme.div_vert, height/subsample_scheme.div_vert-1);
		return b_cr(vx, vy);
	}
	else
		return 0;
}

void picture::changeColorSpaceToYCbCr(){
	if (this->colorspace != YCBCR){
		for (unsigned int x = 0; x < width; x++)
			for (unsigned int y = 0; y < height; y++){
				BYTE R = getR(x,y);
				BYTE G = getG(x,y);
				BYTE B = getB(x,y);
				float Y =  (mRGBtoYCbCr(0, 0) * R) + (mRGBtoYCbCr(0, 1) * G) + (mRGBtoYCbCr(0, 2) * B) - 128;
				float Cb = (mRGBtoYCbCr(1, 0) * R) + (mRGBtoYCbCr(1, 1) * G) + (mRGBtoYCbCr(1, 2) * B) + 128 - 128;
				float Cr = (mRGBtoYCbCr(2, 0) * R) + (mRGBtoYCbCr(2, 1) * G) + (mRGBtoYCbCr(2, 2) * B) + 128 - 128;
				setR_Y (x, y, (int)round(Y) );
				setG_Cb(x, y, (int)round(Cb));
				setB_Cr(x, y, (int)round(Cr));
			
			}
		this->colorspace = YCBCR;
	}
}

void picture::subsampleCb(subSamplingScheme sss){
	matrix<int> new_cb(width / sss.div_hori, height / sss.div_vert);
	for (unsigned int x = 0; x < width / sss.div_hori; x++){
		for (unsigned int y = 0; y < height / sss.div_vert; y++){
			int value = 0;

			// averaging
			if (sss.averaging){
				for (unsigned int dx = 0; dx < sss.div_hori; dx++)
				for (unsigned int dy = 0; dy < sss.div_vert; dy++)
					value += getCb(x*sss.div_hori + dx, y*sss.div_vert + dy);

				value /= (int)(sss.div_hori * sss.div_vert);
			}
			else
				value = getCb(x*sss.div_hori, y*sss.div_vert);

			new_cb(x, y) = value;
		}
	}
	g_cb = new_cb;
}

void picture::subsampleCr(subSamplingScheme sss){
	matrix<int> new_cr(width / sss.div_hori, height / sss.div_vert);
	for (unsigned int x = 0; x < width / sss.div_hori; x++){
		for (unsigned int y = 0; y < height / sss.div_vert; y++){
			int value = 0;

			// averaging
			if (sss.averaging){
				for (unsigned int dx = 0; dx < sss.div_hori; dx++)
				for (unsigned int dy = 0; dy < sss.div_vert; dy++)
					value += getCr(x*sss.div_hori + dx, y*sss.div_vert + dy);

				value /= (int)(sss.div_hori * sss.div_vert);
			}
			else
				value = getCr(x*sss.div_hori, y*sss.div_vert);

			new_cr(x, y) = value;
		}
	}
	b_cr = new_cr;
}

void picture::subsampleColorChannels(subSamplingScheme sss){
	if (subsample_scheme.scheme != _444
		|| this->width <= 8
		|| (this->width <= 16 && sss.div_hori > 2)
		|| ((this->width / 8) % sss.div_hori != 0))
		return;
	
	subsampleCb(sss);
	subsampleCr(sss);
	this->subsample_scheme = sss;
}

void picture::setR_Y(const int x, const int y, const int value) {
    r_y(x,y) = value; 
}

void picture::setG_Cb(const int x, const int y, const int value) { 
    g_cb(x,y) = value; 
}

void picture::setB_Cr(const int x, const int y, const int value) { 
    b_cr(x,y) = value; 
}

void picture::setColor(const int x, const int y, const int r_y, const int g_cb, const int b_cr){
    setR_Y(x, y, r_y);
    setG_Cb(x, y, g_cb);
    setB_Cr(x, y, b_cr);
}

picture::COLORSPACE picture::getColorSpace(){ 
	return this->colorspace;
}

matrix<int> picture::getMatrixY(){
	if (colorspace != YCBCR){
		this->changeColorSpaceToYCbCr();
	}
	return this->r_y;
}

matrix<int> picture::getMatrixCb(){
	if (colorspace != YCBCR){
		this->changeColorSpaceToYCbCr();
	}
	return this->g_cb;
}

matrix<int> picture::getMatrixCr(){
	if (colorspace != YCBCR){
		this->changeColorSpaceToYCbCr();
	}
	return this->b_cr;
}

subSamplingScheme picture::getSubSamplingScheme(){
	return this->subsample_scheme;
}
/*
void picture::setColor(const unsigned int x, const unsigned int y, const std::vector<unsigned int> color){
    this->r(x,y) = color.at(0);
    this->g(x,y) = color.at(1);
    this->b(x,y) = color.at(2);
}*/