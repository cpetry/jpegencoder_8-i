/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

#include "DCT_Variante2.h"

#include <thread>

#include <boost\numeric\ublas\matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp> // matrix_range

matrix<float> DCT_Variante2::matrix_A = matrix<float>(8, 8);
float DCT_Variante2::c0_0 = 1.0f / static_cast<float>(sqrt(2.0f));
float DCT_Variante2::c0_1 = 1.0f;
float DCT_Variante2::sqrt2N = static_cast<float>(sqrt(0.25f));
float DCT_Variante2::pi2N = static_cast<float>(M_PI / 16.0f);

float DCT_Variante2::matrix_values[8][8] = { { c0_0 * sqrt2N * cos(1 * 0 * pi2N), c0_1 * sqrt2N * cos(1 * 1 * pi2N),
	c0_1 * sqrt2N * cos(1 * 2 * pi2N),	c0_1 * sqrt2N * cos(1 * 3 * pi2N),	c0_1 * sqrt2N * cos(1 * 4 * pi2N),
	c0_1 * sqrt2N * cos(1 * 5 * pi2N),	c0_1 * sqrt2N * cos(1 * 6 * pi2N),	c0_1 * sqrt2N * cos(1 * 7 * pi2N) },
	{ c0_0 * sqrt2N * cos(3 * 0 * pi2N),c0_1 * sqrt2N * cos(3 * 1 * pi2N),	c0_1 * sqrt2N * cos(3 * 2 * pi2N),
	c0_1 * sqrt2N * cos(3 * 3 * pi2N),	c0_1 * sqrt2N * cos(3 * 4 * pi2N),	c0_1 * sqrt2N * cos(3 * 5 * pi2N),
	c0_1 * sqrt2N * cos(3 * 6 * pi2N),	c0_1 * sqrt2N * cos(3 * 7 * pi2N) },
	{ c0_0 * sqrt2N * cos(5 * 0 * pi2N),c0_1 * sqrt2N * cos(5 * 1 * pi2N),	c0_1 * sqrt2N * cos(5 * 2 * pi2N),
	c0_1 * sqrt2N * cos(5 * 3 * pi2N),	c0_1 * sqrt2N * cos(5 * 4 * pi2N),	c0_1 * sqrt2N * cos(5 * 5 * pi2N),
	c0_1 * sqrt2N * cos(5 * 6 * pi2N),	c0_1 * sqrt2N * cos(5 * 7 * pi2N) },
	{ c0_0 * sqrt2N * cos(7 * 0 * pi2N),c0_1 * sqrt2N * cos(7 * 1 * pi2N),	c0_1 * sqrt2N * cos(7 * 2 * pi2N),
	c0_1 * sqrt2N * cos(7 * 3 * pi2N),	c0_1 * sqrt2N * cos(7 * 4 * pi2N),	c0_1 * sqrt2N * cos(7 * 5 * pi2N),
	c0_1 * sqrt2N * cos(7 * 6 * pi2N),	c0_1 * sqrt2N * cos(7 * 7 * pi2N) },
	{ c0_0 * sqrt2N * cos(9 * 0 * pi2N),c0_1 * sqrt2N * cos(9 * 1 * pi2N),	c0_1 * sqrt2N * cos(9 * 2 * pi2N),
	c0_1 * sqrt2N * cos(9 * 3 * pi2N),	c0_1 * sqrt2N * cos(9 * 4 * pi2N),	c0_1 * sqrt2N * cos(9 * 5 * pi2N),
	c0_1 * sqrt2N * cos(9 * 6 * pi2N),	c0_1 * sqrt2N * cos(9 * 7 * pi2N) },
	{ c0_0 * sqrt2N * cos(11 * 0 * pi2N),	c0_1 * sqrt2N * cos(11 * 1 * pi2N),	c0_1 * sqrt2N * cos(11 * 2 * pi2N),
	c0_1 * sqrt2N * cos(11 * 3 * pi2N),		c0_1 * sqrt2N * cos(11 * 4 * pi2N),	c0_1 * sqrt2N * cos(11 * 5 * pi2N),
	c0_1 * sqrt2N * cos(11 * 6 * pi2N),		c0_1 * sqrt2N * cos(11 * 7 * pi2N) },
	{ c0_0 * sqrt2N * cos(13 * 0 * pi2N),	c0_1 * sqrt2N * cos(13 * 1 * pi2N),	c0_1 * sqrt2N * cos(13 * 2 * pi2N),
	c0_1 * sqrt2N * cos(13 * 3 * pi2N),		c0_1 * sqrt2N * cos(13 * 4 * pi2N),	c0_1 * sqrt2N * cos(13 * 5 * pi2N),
	c0_1 * sqrt2N * cos(13 * 6 * pi2N),		c0_1 * sqrt2N * cos(13 * 7 * pi2N) },
	{ c0_0 * sqrt2N * cos(15 * 0 * pi2N),	c0_1 * sqrt2N * cos(15 * 1 * pi2N),	c0_1 * sqrt2N * cos(15 * 2 * pi2N),
	c0_1 * sqrt2N * cos(15 * 3 * pi2N),		c0_1 * sqrt2N * cos(15 * 4 * pi2N),	c0_1 * sqrt2N * cos(15 * 5 * pi2N),
	c0_1 * sqrt2N * cos(15 * 6 * pi2N),		c0_1 * sqrt2N * cos(15 * 7 * pi2N) }
};

matrix<float> DCT_Variante2::z(8, 8);

void DCT_Variante2::transform(matrix<float> &m){
	for (int y = 0; y < 8; y++)
	for (int x = 0; x < 8; x++)
		matrix_A(x, y) = matrix_values[x][y];

	std::thread t0, t1, t2, t3;

	for (unsigned int y = 0; y <= m.size2() - 8; y += 8)
	for (unsigned int x = 0; x <= m.size1() - 8; x += 8){
		matrix_range<matrix<float>> mr(m, range(x, x + 8), range(y, y + 8));
		z = prod<matrix<float>>(mr, matrix_A);
		z = prod(trans(matrix_A), z);
		mr = z;
	}
}
