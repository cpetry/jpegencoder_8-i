/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "picture.h"
#include "ppmFileThread.h"

#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <vector>
#include <string>

void ppmFileThread::run(){

	// register ppmFileThread::result so it can be used in main_gui 
	qRegisterMetaType<ppmFileThread::result>("ppmFileThread::result");

    /* expensive or blocking operation  */
	result correct = readFileBytes(file, pic, stepsize);
		emit signal_readingPPM(correct);
	canceled = false;
}

ppmFileThread::result ppmFileThread::readFileBytes(std::string file, picture &pic, unsigned int stepsize){
	std::ifstream input;
    
	std::vector<std::string> dimensions;
    std::vector<std::string> color;

	const int size = 256;

	input.open(file);
    if(input.is_open()){
		
		char format[2];
		input.read(format, 2);

		if (format[0] != 'P' && format[1] != '3'){
			return result::FAILED;
		}
		ppmFileThread::ignoreSeperators(&input);
		ppmFileThread::ignoreComments(&input);
		ppmFileThread::ignoreSeperators(&input);
		int width = ppmFileThread::readInt(&input);
		ppmFileThread::ignoreSeperators(&input);
		int height = ppmFileThread::readInt(&input);
		ppmFileThread::ignoreSeperators(&input);

		int absolute_width = (width % stepsize != 0)	? ((width / stepsize) + 1) * stepsize	: width;
		int absolute_height = (height % stepsize != 0)	? ((height / stepsize) + 1) * stepsize	: height;

		int maxbrightness = ppmFileThread::readInt(&input);
		ppmFileThread::ignoreSeperators(&input);

		if (maxbrightness != 255)
			return result::FAILED;

		long filesizeread = 0;
		const int chunksize = 256;

		pic = picture(absolute_width, absolute_height);
        int pix_pos = 0;
		int max_pix_pos = width * height - 1;
		int percentage = 0;

		char color[3];
		int colorpos = 0;
		int colValue = 0;
		int colorCount = 0;

			
		// reading line per line
		while(input){
			//std::string line;
			//getline(input, line);
			char chunk[chunksize];
			input.read(chunk, sizeof(chunk));
			filesizeread += chunksize;
			for (char c : chunk){

				// Calculating percentage and sending it to gui
				float accurate_percentage = pix_pos * 1.0f / (width*1.0f*height) * 100;
				if (accurate_percentage > percentage ){
					percentage++;
					emit this->signal_sendPercentage(percentage);
					if (this->canceled == true)
						return result::CANCELED;
				}


				if (c == ' ' || c == '\n'){
					if (colorpos != 0){
						if (colorCount == 0){
							pic.setR_Y(pix_pos % width, pix_pos / width, boost::lexical_cast<int>(color, colorpos));
							colorCount++;
						}
						else if (colorCount == 1){
							pic.setG_Cb(pix_pos % width, pix_pos / width, boost::lexical_cast<int>(color, colorpos));
							colorCount++;
						}
						else if (colorCount == 2){
							pic.setB_Cr(pix_pos % width, pix_pos / width, boost::lexical_cast<int>(color, colorpos));
							colorCount = 0;
							if (max_pix_pos == pix_pos)
								break;
							pix_pos++;
						}
					}
					colorpos = 0;
				}
				else{
					color[colorpos] = c;
					colorpos++;
				}
			} // for
		}; // while input


		// repeating pixel
		// right
		for (int x = width; x < absolute_width; x++)
			for (int y = 0; y < height; y++){
				pic.setColor(x, y, pic.getR(width-1, y), pic.getG(width-1, y), pic.getB(width-1, y));
			}
		
		// bottom
		for (int y = height; y < absolute_height; y++)
			for (int x = 0; x < width; x++){
				pic.setColor(x, y, pic.getR(x, height-1), pic.getG(x, height-1), pic.getB(x, height-1));
			}

		// bottomright corner
		for (int y = height; y < absolute_height; y++)
			for (int x = width; x < absolute_width; x++){
				pic.setColor(x, y, pic.getR(width-1, height-1), pic.getG(width-1, height-1), pic.getB(width-1, height-1));
			}

		return result::FINISHED;
	} // if input.open()
	return result::FAILED;
}

void ppmFileThread::slot_cancelLoading(){
	canceled = true;
}

int ppmFileThread::readInt(std::ifstream *input){
	char buffer[4];
	char next = input->peek();
	int pos = 0;
	while(next != ' ' && next != '\n'){
		input->get(buffer[pos++]);
		next = input->peek();
	};
	int retValue = boost::lexical_cast<int>(buffer, pos);
	return retValue;
}

void ppmFileThread::ignoreSeperators(std::ifstream *input){
	char next = input->peek();
	// ignoring all whitespaces
	while((next == ' ' || next == '\n')){
		input->ignore(1);
		next = input->peek();
	};
}

void ppmFileThread::ignoreComments(std::ifstream *input){
	char next = input->peek();
	// ignoring all whitespaces
	while(next == '#'){
		while(next != '\n'){
			input->ignore(1);
			next = input->peek();
		};
	};
}