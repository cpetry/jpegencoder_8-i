/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "picture.h"
#include "encodeThread.h"
#include "jpegSegments.h"

#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <vector>
#include <string>

void encodeThread::run(){
	// register ppmFileThread::result so it can be used in main_gui 
	qRegisterMetaType<encodeThread::result>("encodeThread::result");

    pic.changeColorSpaceToYCbCr();
	emit signal_sendPercentage(5);

    pic.subsampleColorChannels(this->subsampling);
	emit signal_sendPercentage(20);

    jpegSegments jpeg;
	jpeg.writeSegments(pic, "temp.jpg", q_table);
	emit signal_sendPercentage(100);

	emit signal_encoding(result::FINISHED);
}

void encodeThread::slot_cancelLoading(){
	canceled = true;
}
