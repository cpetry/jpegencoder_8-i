/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

#include "encoding.h"

#include <bitset>
#include <omp.h>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp> // matrix_range

#include "huffmanCode.h"
#include "DCT_Variante1.h"
#include "DCT_Variante2.h"
#include "DCT_Variante3.h"
#include "Quantization.h"
#include "encodeThread.h"

using namespace boost::numeric::ublas;

std::vector<int> encoding::createDCdeltas(const matrix<int> &m, SUBSAMPLINGSCHEME sss){
	std::vector<int> dc_deltas;
	int dc_old = 0;

	std::vector<std::pair<int, int>> list = encoding::createCoordList(m, sss);

	for (std::pair<int, int> coord : list){
		dc_deltas.push_back(m(coord.first * 8, coord.second * 8) - dc_old);
		dc_old = m(coord.first * 8, coord.second * 8);
	}
	return dc_deltas;
}

std::vector<std::vector<std::pair<int, int>>> encoding::createACpairs(matrix<int> &m, SUBSAMPLINGSCHEME sss){
	std::vector<std::vector<std::pair<int, int>>> ac_pairs;
	std::vector<std::pair<int, int>> list = encoding::createCoordList(m, sss);
	for (std::pair<int, int> coord : list){
		matrix_range<matrix<int>> mr(m, range(coord.first * 8, coord.first * 8 + 8), range(coord.second * 8, coord.second * 8 + 8));
		std::vector<int> zigzag = zigzagMatrix(mr);
		if (zigzag.size() > 0)
			zigzag.erase(zigzag.begin()); // first one is dc -> erase it
		ac_pairs.push_back(createZeroNumberPairs(zigzag));
	}
	return ac_pairs;
}

std::vector<int> encoding::zigzagMatrix(const matrix<int> &m){
	int x = 0, y = 0;
	bool direction = false; // downwards;
	std::vector<int> q_coeffzz;
	q_coeffzz.push_back(m(0, 0)); // first element

	while (x != 7 || y != 7){
		if (y == 7 && x % 2 == 0){
			x++;
			direction = true;
		}
		else if (x == 7 && y == 0){
			x--;
			y++;
			direction = false;
		}
		else if (x == 7 && y % 2 == 1){
			y++;
			direction = false;
		}
		else if (x % 2 == 0 && y == 0){
			x++;
			direction = false;
		}
		else if (y % 2 == 1 && x == 0){
			y++;
			direction = true;
		}
		else{
			if (direction){
				x++;
				y--;
			}
			else{
				x--;
				y++;
			}
		}
		q_coeffzz.push_back(m(y, x));
	}

	return q_coeffzz;
}

std::vector<std::pair<int, int>> encoding::createZeroNumberPairs(std::vector<int> &coeffs){
	int zero_counter = 0;
	std::vector<std::pair<int, int>> runlength;
	bool zeros_at_end = false;

	if (coeffs.size() > 0 && coeffs.back() == 0){
		zeros_at_end = true;
		while (coeffs.size() > 0 && coeffs.back() == 0)
			coeffs.pop_back();
	}

	for (int c : coeffs){
		if (c == 0){
			zero_counter++;
			if (zero_counter == 16){
				runlength.push_back(std::pair<int, int>(15, 0));
				zero_counter = 0;
			}
		}
		else{
			runlength.push_back(std::pair<int, int>(zero_counter, c));
			zero_counter = 0;
		}
	}

	// EOB only if 0s are at the end!
	if (zeros_at_end)
		runlength.push_back(std::pair<int, int>(0, 0));

	return runlength;
}

std::vector<bitstream> encoding::createBlockstreams(const std::vector<int> &dc_deltas, const std::vector<std::vector<std::pair<int, int>>> &ac_pairs, HuffMap &hm_dc, HuffMap &hm_ac){
	std::vector<bitstream> blockstreams(dc_deltas.size());

#pragma omp parallel for
	for (int block = 0; block < (int)dc_deltas.size(); block++){
		bitstream bs;

		// Create DC_Stream
		int dc_category = encoding::getCategory(dc_deltas[block]);
		std::vector<bool> dc_cat_bits = hm_dc[dc_category];
		std::vector<bool> dc_number_bits = encoding::getBitCode(dc_deltas[block]);
		
		for (auto i1 = dc_cat_bits.begin(); i1 != dc_cat_bits.end(); ++i1){
			bs.push_bit(*i1);
		}

		for (auto i2 = dc_number_bits.begin(); i2 != dc_number_bits.end(); ++i2){
			bs.push_bit(*i2);
		}


		for (unsigned int i = 0; i < ac_pairs[block].size(); i++){

			// get byte of huffman and write it to bitstream
			int cat_pair = ac_pairs[block][i].first * 16 + getCategory(ac_pairs[block][i].second);
			std::vector<bool> element = hm_ac[cat_pair];
			for (auto i1 = element.begin(); i1 != element.end(); ++i1){
				bs.push_bit(*i1);
			}

			// write bitcode of number to bistream
			std::vector<bool> bitcode = getBitCode(ac_pairs[block][i].second);
			for (auto i2 = bitcode.begin(); i2 != bitcode.end(); ++i2){
				bs.push_bit(*i2);
			}

		}
		blockstreams[block] = bs;
	}
	return blockstreams;
}

void encoding::createYBitstreamsAndTrees(const matrix<int> &m, SUBSAMPLINGSCHEME sss, Quantization::Q_Table q_table, HuffMap &dc_y_tree, HuffMap &ac_y_tree, std::vector<bitstream> &y_blocks){
	matrix<float> r(m);
	DCT_Variante3::transform(r);
	matrix<int> y = Quantization::quantize(r, q_table, Quantization::LUMINANCE);

	std::vector<int> dc_deltas = createDCdeltas(y, sss);
	std::vector<int> dc_categories;
	for (int d : dc_deltas)
		dc_categories.push_back(getCategory(d));
	std::map<int, int> freqDC_Y = huffmanCode::generateFrequencies(dc_categories);

	std::vector<std::vector<std::pair<int, int>>> ac_pairs = createACpairs(y, sss);
	std::vector<int> to_encode;
	for (std::vector<std::pair<int, int>> v : ac_pairs){
		for (std::pair<int, int> p : v){
			to_encode.push_back(p.first * 16 + getCategory(p.second));
		}
	}
	std::map<int, int> freqAC_Y = huffmanCode::generateFrequencies(to_encode);

	dc_y_tree = huffmanCode::createHuffmanCodes(freqDC_Y, true, true, 15);
	ac_y_tree = huffmanCode::createHuffmanCodes(freqAC_Y, true, true, 15);

	y_blocks = createBlockstreams(dc_deltas, ac_pairs, dc_y_tree, ac_y_tree);
}

void encoding::createCbCrBitstreamsAndTrees(const matrix<int> &cb, const matrix<int> &cr, Quantization::Q_Table q_table, HuffMap &dc_cbcr_tree, HuffMap &ac_cbcr_tree, std::vector<bitstream> &cb_blocks, std::vector<bitstream> &cr_blocks){
	matrix<float> r(cb);
	DCT_Variante3::transform(r);
	matrix<int> cbq = Quantization::quantize(r, q_table, Quantization::CHROMINANCE);

	matrix<float> m(cr);
	DCT_Variante3::transform(m);
	matrix<int> crq = Quantization::quantize(m, q_table, Quantization::CHROMINANCE);

	std::vector<int> dc_cb_deltas = encoding::createDCdeltas(cbq);
	std::vector<int> dc_cr_deltas = encoding::createDCdeltas(crq);

	std::vector<int> dc_categories;
	for (int d : dc_cb_deltas)
		dc_categories.push_back(encoding::getCategory(d));

	for (int d : dc_cr_deltas)
		dc_categories.push_back(encoding::getCategory(d));

	std::map<int, int> freqDC_CbCr = huffmanCode::generateFrequencies(dc_categories);

	std::vector<std::vector<std::pair<int, int>>> ac_cb_pairs = encoding::createACpairs(cbq);
	std::vector<std::vector<std::pair<int, int>>> ac_cr_pairs = encoding::createACpairs(crq);
	std::vector<int> to_encode;
	for (std::vector<std::pair<int, int>> v : ac_cb_pairs){
		for (std::pair<int, int> p : v)
			to_encode.push_back(p.first * 16 + encoding::getCategory(p.second));
	}
	for (std::vector<std::pair<int, int>> v : ac_cr_pairs){
		for (std::pair<int, int> p : v)
			to_encode.push_back(p.first * 16 + encoding::getCategory(p.second));
	}
	std::map<int, int> freqAC_CbCr = huffmanCode::generateFrequencies(to_encode);

	dc_cbcr_tree = huffmanCode::createHuffmanCodes(freqDC_CbCr, true, true, 15);
	ac_cbcr_tree = huffmanCode::createHuffmanCodes(freqAC_CbCr, true, true, 15);

	cb_blocks = encoding::createBlockstreams(dc_cb_deltas, ac_cb_pairs, dc_cbcr_tree, ac_cbcr_tree);
	cr_blocks = encoding::createBlockstreams(dc_cr_deltas, ac_cr_pairs, dc_cbcr_tree, ac_cbcr_tree);
}

std::vector<bool> encoding::getBitCode(const int &number){
	if (number == 0)
		return std::vector<bool>();

	std::vector<bool> code;
	int n = number;
	int category = getCategory(n);

	if (n < 0){
		n += pow(2, category) - 1;
	}
	
	int digits = category;
	while (n > 0 || digits > 0){
		code.push_back(n % 2);
		n /= 2;
		digits--;
	}
	
	std::reverse(code.begin(), code.end());
	return code;
}

int encoding::getCategory(const int &number){
	int category = 0;
	if (number != 0)
		category = (int)(log(abs(number)) / log(2)) + 1;
	return category;
}

int encoding::getIntegerOfBitCode(const std::vector<bool> &bits){
	int n=0;
	for (auto b = bits.begin(); b != bits.end(); ++b){
		n = n << 1;
		if (*b)
			n += 1;
	}
	return n;
}

std::vector<std::pair<int, int>> encoding::createCoordList(const matrix<int> &m, SUBSAMPLINGSCHEME sss){
	std::vector<std::pair<int, int>> list_8x8blocks;
	std::vector<int> list_pos;
	unsigned int pos = 0;
	while (pos < m.size1() * m.size2() / 64){
		list_pos.push_back(pos++);
		if (sss == SUBSAMPLINGSCHEME::_422){
			list_pos.push_back(pos++);
		}

		else if (sss == SUBSAMPLINGSCHEME::_411){
			list_pos.push_back(pos++);
			list_pos.push_back(pos++);
			list_pos.push_back(pos++);
		}
		else if (sss == SUBSAMPLINGSCHEME::_420){
			list_pos.push_back(pos);							// x + 1
			list_pos.push_back(pos - 1 + (m.size1() / 8));		// y + 1
			list_pos.push_back(pos +  (m.size1() / 8));			// y + 1, x + 1
			pos++;
			if (pos % (m.size1() / 8) == 0)
				pos += (m.size1() / 8);
		}
	}
	for (unsigned int i = 0; i < list_pos.size(); i++)
		list_8x8blocks.push_back(std::pair<int, int>(list_pos[i] % (m.size1() / 8), list_pos[i] / (m.size1() / 8)));

	return list_8x8blocks;
}
