/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "main_gui.h"
#include <QtGui/QWindow>
#include <QtWidgets/QFileDialog>

#include "ppmFileThread.h"
#include "encodeThread.h"
#include <chrono>
#include <QtGui\QImageReader>
#include <QtCore\QDebug>

main_GUI::main_GUI(){
	// Loads the ui_GUI.ui file and set it to the UI
	ui.setupUi(this);

	// Sets the window title
	this->setWindowTitle("Jpeg Encoder");

	//At first disable Load and Encode button
	this->ui.btn_loadPPM->setDisabled(true);
	this->ui.btn_Encode->setDisabled(true);

	leftView.setParent(ui.leftPicture);
	leftView.setOtherView(&rightView);
	leftView.resize(ui.leftPicture->size());
    leftView.show();

	rightView.setParent(ui.rightPicture);
	rightView.setOtherView(&leftView);
	rightView.resize(ui.rightPicture->size());
    rightView.show();

	// Disable checkboxes for scaling images
	this->ui.checkBox_scaleRightImage->setDisabled(true);
	
	this->ui.ComboBox_QTable->addItem("ITUT");
	this->ui.ComboBox_QTable->addItem("Photoshop 00");
	this->ui.ComboBox_QTable->addItem("Photoshop 04");
	this->ui.ComboBox_QTable->addItem("Photoshop 08");
	this->ui.ComboBox_QTable->addItem("Photoshop 12");
	this->ui.ComboBox_QTable->addItem("Example");
	this->ui.ComboBox_QTable->addItem("MinimalCompression");

	this->ui.comboBox_stepsize->addItem("8");
	this->ui.comboBox_stepsize->addItem("16");
	this->ui.comboBox_stepsize->setCurrentIndex(1); // 16 selected at first
	
	slot_qtableComboboxChanged("ITUT");

	connect(ui.btn_choose, &QPushButton::clicked, this, &main_GUI::slot_chooseFile);
	connect(ui.lineEdit_filename, &QLineEdit::editingFinished, this, &main_GUI::slot_editingFilenameFinished);
	connect(ui.ComboBox_QTable, static_cast<void (QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged), this, &main_GUI::slot_qtableComboboxChanged);
	connect(ui.btn_loadPPM, &QPushButton::clicked, this, &main_GUI::slot_startLoadingPPMFile);
	connect(ui.checkBox_scaleRightImage, &QCheckBox::toggled, this, &main_GUI::slot_scaleImagesToggled);
	connect(ui.btn_Encode, &QPushButton::clicked, this, &main_GUI::slot_startEncodingPicture);

	QDir solution = QDir::current();
	solution.cdUp();
    solution.cdUp();
	this->ui.lineEdit_filename->setText(QString(solution.absolutePath() + "/_resources/16x16.ppm"));
	slot_editingFilenameFinished();
}

void main_GUI::slot_chooseFile(){
	QDir res = QDir::current().absolutePath() + "/_resources";
	if (!res.exists()){
		res = QDir::current();
		res.cdUp();
		res.cdUp();
		res = res.absolutePath() + "/_resources";
	}
	QString path = res.absolutePath();

	QString file = QFileDialog::getOpenFileName(this,
		tr("Open Image"), 
		path,
		tr("Image Files (*.ppm)"),
		(QString*)0, QFileDialog::ReadOnly);

	if (!file.isEmpty()){
		ui.lineEdit_filename->setText(file);
		slot_editingFilenameFinished();
	}
}

void main_GUI::slot_editingFilenameFinished(){
	this->filename = ui.lineEdit_filename->text();
	// check if file even exists
	if (QFile::exists(this->filename) && this->filename.endsWith(".ppm")){
		// clear Message on StatusBar
		this->ui.btn_loadPPM->setEnabled(true);
		ui.statusBar->showMessage("");
	}
	else if(!this->filename.isEmpty())
	{
		this->ui.btn_loadPPM->setDisabled(true);
		ui.statusBar->showMessage("File not found!");
	}
	else if(!this->filename.endsWith(".ppm")){
		this->ui.btn_loadPPM->setDisabled(true);
		ui.statusBar->showMessage("Wrong file format!");
	}
}

void main_GUI::slot_startLoadingPPMFile(){
	if (ui.btn_loadPPM->text() == "Load"){
		ui.statusBar->showMessage("Loading...");
		loader_begin = std::chrono::steady_clock::now();
		this->rightView.clear();
		this->leftView.clear();
		int stepsize = ui.comboBox_stepsize->currentText().toInt();
		this->loader = new ppmFileThread(this->filename.toStdString(), stepsize);
		connect(loader, &ppmFileThread::signal_sendPercentage, this, &main_GUI::slot_updatePercentageLeft);
		connect(loader, &ppmFileThread::signal_readingPPM, this, &main_GUI::slot_getLoadingPPMResult);
		connect(ui.btn_loadPPM, &QPushButton::clicked, loader, &ppmFileThread::slot_cancelLoading);
        connect(loader, &ppmFileThread::finished, loader, &QObject::deleteLater);
        this->ui.checkBox_scaleRightImage->setEnabled(true);
		this->loader->start();
		ui.btn_loadPPM->setText("Cancel");

		int filesize = QFile(this->filename).size();
		QString size;
		if ((filesize > 1024 * 1024))
			size = QString::number(filesize / 1024.0 / 1024.0, 'f', 2) + " MB";
		else if (filesize > 1024)
			size = QString::number(filesize / 1024.0, 'f', 2) + " KB";
		else
			size = QString::number(filesize) + " B";
		ui.lineEdit_filesize_load->setText(size);
	}
}

void main_GUI::slot_startEncodingPicture(){
    if (ui.btn_Encode->text() == "Encode"){
	    ui.statusBar->showMessage("Encoding...");
		encoder_begin = std::chrono::steady_clock::now();

	    this->rightView.clear();
        SUBSAMPLINGSCHEME sub_scheme = subSamplingScheme::stringToEnum(ui.ComboBox_Subsampling->currentText().toStdString());
		Quantization::Q_Table q_table = Quantization::stringToEnum(ui.ComboBox_QTable->currentText().toStdString());
        bool sub_average = ui.checkBox_SubsamplingAverage->isChecked();
		this->encoder = new encodeThread(this->pic, subSamplingScheme(sub_scheme, sub_average), q_table);
		connect(encoder, &encodeThread::signal_encoding, this, &main_GUI::slot_getEncodingResult);
		connect(encoder, &encodeThread::signal_sendPercentage, this, &main_GUI::slot_updatePercentageRight);

	    this->encoder->start();
		
		ui.btn_Encode->setText("Cancel");
    }
}

void main_GUI::slot_updatePercentageLeft(int percentage){
	ui.progressBar_left->setValue(percentage);
}

void main_GUI::slot_updatePercentageRight(int percentage){
	ui.progressBar_right->setValue(percentage);
}

void main_GUI::slot_getLoadingPPMResult(ppmFileThread::result r){
	if (r == ppmFileThread::result::FINISHED){
		
		// create and show image
		this->pic = loader->getPicture();
		
		left_image = QImage(pic.getWidth(),  pic.getHeight(), QImage::Format_RGB888);
		for (unsigned int y = 0; y < pic.getHeight(); y++)
			for (unsigned int x = 0; x < pic.getWidth(); x++)
				left_image.setPixel(x, y, qRgb(pic.getR(x, y), pic.getG(x, y), pic.getB(x, y)));
	
		left_pixmap = QPixmap::fromImage(left_image);
		left_original_size = QSize(pic.getWidth(), pic.getHeight());

		this->leftView.setPixmap(left_pixmap);
		this->leftView.show();

		// always scale when image was loaded
		slot_scaleImagesToggled(true);
	
		this->ui.btn_Encode->setEnabled(true);
		this->ui.lineEdit_dimensionsX->setText(QString::number(pic.getWidth()));
		this->ui.lineEdit_dimensionsY->setText(QString::number(pic.getHeight()));

		

		// Add content to dropdown subsampling
		// only if modes really are available !
		this->ui.ComboBox_Subsampling->clear();
		this->ui.ComboBox_Subsampling->addItem("444");

		if (pic.getWidth() / 8 % 2 == 0)
			this->ui.ComboBox_Subsampling->addItem("422");
		
		if (pic.getWidth() / 8 % 4 == 0)
			this->ui.ComboBox_Subsampling->addItem("411");

		if (pic.getWidth() / 8 % 2 == 0 && pic.getHeight() / 8 % 2 == 0)
			this->ui.ComboBox_Subsampling->addItem("420");
	}
	else if (r == ppmFileThread::result::FAILED){
		ui.statusBar->showMessage("Error loading File!");
	}
	else if (r == ppmFileThread::result::CANCELED){
		ui.statusBar->showMessage("Loading canceled!");
	}

	// Reset button to "load"
	ui.btn_loadPPM->setText("Load");
	connect(ui.btn_loadPPM, &QPushButton::clicked, this, &main_GUI::slot_startLoadingPPMFile);
	
	// Calculate duration of loading the picture
	auto end = std::chrono::steady_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - loader_begin);
	QString duration_load;
	duration_load.sprintf("Loading finished! %d ms", duration);

	ui.statusBar->showMessage(duration_load);

	// if picture wasn't loaded completely -> set to 0%
	if (ui.progressBar_left->value() != 100){
		ui.progressBar_left->setValue(0);
	}
}

void main_GUI::slot_getEncodingResult(encodeThread::result r){
	if (r == encodeThread::result::FINISHED){
		
		//qDebug() << QImageReader::supportedImageFormats();

		QString p = QDir::currentPath() + "/temp.jpg";
		right_image = QImage(p);
		right_pixmap = QPixmap::fromImage(right_image);
		right_original_size = QSize(left_pixmap.width(), left_pixmap.height());

		this->rightView.setPixmap(right_pixmap);
		this->rightView.show();

		// check if scaled image checkBox is checked
		slot_scaleImagesToggled(ui.checkBox_scaleRightImage->isChecked());
		//this->rightView.moveToLastPosition();

		this->ui.btn_Encode->setEnabled(true);

		// set filesize
		int filesize = QFile(p).size();
		QString size;
		if ((filesize > 1024 * 1024))
			size = QString::number(filesize / 1024.0 / 1024.0, 'f', 2) + " MB";
		else if (filesize > 1024)
			size = QString::number(filesize / 1024.0, 'f', 2) + " KB";
		else
			size = QString::number(filesize) + " B";
		ui.lineEdit_filesize_encode->setText(size);
	}
	else if (r == encodeThread::result::FAILED){
		ui.statusBar->showMessage("Error encoding File!");
	}
	else if (r == encodeThread::result::CANCELED){
		ui.statusBar->showMessage("Encoding canceled!");
	}

	// Reset button to "Encode"
	ui.btn_Encode->setText("Encode");
    //connect(ui.btn_Encode, &QPushButton::clicked, this, &main_GUI::slot_encodeLeftPicture);
	
	// Calculate duration of loading the picture
	auto end = std::chrono::steady_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - encoder_begin);
	QString duration_load;
	duration_load.sprintf("Encoding finished! %d ms", duration);

	ui.statusBar->showMessage(duration_load);

	// if picture wasn't loaded completely -> set to 0%
	if (ui.progressBar_right->value() != 100){
		ui.progressBar_right->setValue(0);
	}
}

void main_GUI::slot_scaleImagesToggled(bool checked){
	if (!right_image.isNull())
		this->rightView.scaleToFit(checked);
	if (!left_image.isNull())
		this->leftView.scaleToFit(checked);
}

void main_GUI::slot_qtableComboboxChanged(const QString &text){
	std::string s;
	if (text == "ITUT")
		s = Quantization::Q_tableToString(Quantization::Q_Table::ITUT);
	else if (text == "Photoshop 00")
		s = Quantization::Q_tableToString(Quantization::Q_Table::PS00);
	else if (text == "Photoshop 04")
		s = Quantization::Q_tableToString(Quantization::Q_Table::PS04);
	else if (text == "Photoshop 08")
		s = Quantization::Q_tableToString(Quantization::Q_Table::PS08);
	else if (text == "Photoshop 12")
		s = Quantization::Q_tableToString(Quantization::Q_Table::PS12);
	else if (text == "Example")
		s = Quantization::Q_tableToString(Quantization::Q_Table::EXAMPLE);
	else if (text == "MinimalCompression")
		s = Quantization::Q_tableToString(Quantization::Q_Table::NOCOMPRESSION);
	else
		s = "Quantization table not found!";

	this->ui.ComboBox_QTable->setToolTip(QString::fromStdString(s));
}