/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "subSamplingScheme.h"

subSamplingScheme::subSamplingScheme(SUBSAMPLINGSCHEME sss, bool averaging){
	this->scheme = sss;
	this->averaging = averaging;
	switch(sss){
		case SUBSAMPLINGSCHEME::_444:
			this->div_hori = 1;
			this->div_vert = 1;
		break;
		case SUBSAMPLINGSCHEME::_422:
			this->div_hori = 2;
			this->div_vert = 1;
		break;
		case SUBSAMPLINGSCHEME::_411:
			this->div_hori = 4;
			this->div_vert = 1;
		break;
		case SUBSAMPLINGSCHEME::_420:
			this->div_hori = 2;
			this->div_vert = 2;
		break;
	};
}

const SUBSAMPLINGSCHEME subSamplingScheme::stringToEnum(std::string String){
    
    if(String == "444")
        return _444;
    else if(String == "422")
        return _422;
    else if(String == "411")
        return _411;
    else if(String == "420")
        return _420;
	else
		return _444;
}