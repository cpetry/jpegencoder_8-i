#include "Quantization.h"

#include <boost/numeric/ublas/matrix_proxy.hpp> // matrix_range

using namespace boost::numeric::ublas;

/**
* ITUT - CCITT Rec. T.81 (1992 E) page 143:
* ...These are based on psychovisual thresholding and
* are derived empirically using luminance and chrominance and 2:1 horizontal subsampling.
* These tables are provided as examples only and are not necessarily suitable for any particular application.
* These quantization values have been used with good results on 8 - bit per sample luminance and chrominance images...
*/
int Quantization::q_table_itut_lum_values[8][8] = {
	{ 16, 11, 10, 16, 24,   40,  51,  61 },
	{ 12, 12, 14, 19, 26,   58,  60,  55 },
	{ 14, 13, 16, 24, 40,   57,  69,  56 },
	{ 14, 17, 22, 29, 51,   87,  80,  62 },
	{ 18, 22, 37, 56, 68,  109, 103,  77 },
	{ 24, 35, 55, 64, 81,  104, 113,  92 },
	{ 49, 64, 78, 87, 103, 121, 120, 101 },
	{ 72, 92, 95, 98, 112, 100, 103,  99 }
};

int Quantization::q_table_itut_chrom_values[8][8] = {
	{ 17, 18, 24, 47, 99, 99, 99, 99 },
	{ 18, 21, 26, 66, 99, 99, 99, 99 },
	{ 24, 26, 56, 99, 99, 99, 99, 99 },
	{ 47, 66, 99, 99, 99, 99, 99, 99 },
	{ 99, 99, 99, 99, 99, 99, 99, 99 },
	{ 99, 99, 99, 99, 99, 99, 99, 99 },
	{ 99, 99, 99, 99, 99, 99, 99, 99 },
	{ 99, 99, 99, 99, 99, 99, 99, 99 }
};

//
int Quantization::q_table_PS00_lum_values[8][8] = {
	{ 32, 33, 51, 81, 66, 39, 34, 17 },
	{ 33, 36, 48, 47, 28, 23, 12, 12 },
	{ 51, 48, 47, 28, 23, 12, 12, 12 },
	{ 81, 47, 8,  23, 12, 12, 12, 12 },
	{ 66, 28, 23, 12, 12, 12, 12, 12 },
	{ 39, 23, 12, 12, 12, 12, 12, 12 },
	{ 34, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 }
};

int Quantization::q_table_PS00_chrom_values[8][8] = {
	{ 34, 51, 52, 34, 20, 20, 17, 17 },
	{ 51, 38, 24, 14, 14, 12, 12, 12 },
	{ 52, 24, 14, 14, 12, 12, 12, 12 },
	{ 34, 14, 14, 12, 12, 12, 12, 12 },
	{ 20, 14, 12, 12, 12, 12, 12, 12 },
	{ 20, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 } 
};
int Quantization::q_table_PS04_lum_values[8][8] = {
	{ 16, 11, 17, 27, 34, 39, 34, 17 },
	{ 11, 12, 16, 26, 28, 23, 12, 12 },
	{ 17, 16, 21, 28, 23, 12, 12, 12 },
	{ 27, 26, 28, 23, 12, 12, 12, 12 },
	{ 34, 28, 23, 12, 12, 12, 12, 12 },
	{ 39, 23, 12, 12, 12, 12, 12, 12 },
	{ 34, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 }
};
int Quantization::q_table_PS04_chrom_values[8][8] = {
	{ 17, 17, 22, 34, 20, 20, 17, 17 },
	{ 17, 19, 22, 14, 14, 12, 12, 12 },
	{ 22, 22, 14, 14, 12, 12, 12, 12 },
	{ 34, 14, 14, 12, 12, 12, 12, 12 },
	{ 20, 14, 12, 12, 12, 12, 12, 12 },
	{ 20, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 }
};

int Quantization::q_table_PS08_lum_values[8][8] = {
	{  6,  4,  7, 11, 14, 17, 22, 17 },
	{  4,  5,  6, 10, 14, 19, 12, 12 },
	{  7,  6,  8, 14, 19, 12, 12, 12 },
	{ 11, 10, 14, 19, 12, 12, 12, 12 },
	{ 14, 14, 19, 12, 12, 12, 12, 12 },
	{ 17, 19, 12, 12, 12, 12, 12, 12 },
	{ 22, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 }
};

int Quantization::q_table_PS08_chrom_values[8][8] = {
	{  7,  9, 19, 34, 20, 20, 17, 17 },
	{  9, 12, 19, 14, 14, 12, 12, 12 },
	{ 19, 19, 14, 14, 12, 12, 12, 12 },
	{ 34, 14, 14, 12, 12, 12, 12, 12 },
	{ 20, 14, 12, 12, 12, 12, 12, 12 },
	{ 20, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 },
	{ 17, 12, 12, 12, 12, 12, 12, 12 }
};

int Quantization::q_table_PS12_lum_values[8][8] = {
	{ 1, 1, 1, 1, 1, 1, 1, 2 },
	{ 1, 1, 1, 1, 1, 1, 1, 2 },
	{ 1, 1, 1, 1, 1, 1, 2, 2 },
	{ 1, 1, 1, 1, 1, 2, 2, 3 },
	{ 1, 1, 1, 1, 2, 2, 3, 3 },
	{ 1, 1, 1, 2, 2, 3, 3, 3 },
	{ 1, 1, 2, 2, 3, 3, 3, 3 },
	{ 2, 2, 2, 3, 3, 3, 3, 3 },
};

int Quantization::q_table_PS12_chrom_values[8][8] = {
	{ 1, 1, 1, 2, 3, 3, 3, 3 },
	{ 1, 1, 1, 2, 3, 3, 3, 3 },
	{ 1, 1, 2, 3, 3, 3, 3, 3 },
	{ 2, 2, 3, 3, 3, 3, 3, 3 },
	{ 3, 3, 3, 3, 3, 3, 3, 3 },
	{ 3, 3, 3, 3, 3, 3, 3, 3 },
	{ 3, 3, 3, 3, 3, 3, 3, 3 },
	{ 3, 3, 3, 3, 3, 3, 3, 3 }
};

int Quantization::q_table_example_values[8][8] = {
	{ 50, 50, 50, 50, 50, 50, 50, 50 },
	{ 50, 50, 50, 50, 50, 50, 50, 50 },
	{ 50, 50, 50, 50, 50, 50, 50, 50 },
	{ 50, 50, 50, 50, 50, 50, 50, 50 },
	{ 50, 50, 50, 50, 50, 50, 50, 50 },
	{ 50, 50, 50, 50, 50, 50, 50, 50 },
	{ 50, 50, 50, 50, 50, 50, 50, 50 },
	{ 50, 50, 50, 50, 50, 50, 50, 50 }
};

int Quantization::q_table_no_compression[8][8] = {
	{ 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1 }
};


void Quantization::initTable(matrix<int> &m, int(&values)[8][8]){
	for (int y = 0; y < 8; y++)
	for (int x = 0; x < 8; x++)
		m(x, y) = values[x][y];
}

matrix<int> Quantization::getQTable(Q_Table tbl, Q_Channel channel){
	matrix<int> q_table(8, 8);

	// choosing quantization matrix
	switch (tbl){

		// ITUT tables
		case ITUT:
			switch (channel){
				case LUMINANCE:		initTable(q_table, q_table_itut_lum_values);	break;
				case CHROMINANCE:	initTable(q_table, q_table_itut_chrom_values);	break;
			}
			break;

		// Photoshop tables
		case PS00:
			switch (channel){
				case LUMINANCE:		initTable(q_table, q_table_PS00_lum_values);	break;
				case CHROMINANCE:	initTable(q_table, q_table_PS00_chrom_values);	break;
			}
			break;

		// Photoshop tables
		case PS04:
			switch (channel){
				case LUMINANCE:		initTable(q_table, q_table_PS04_lum_values);	break;
				case CHROMINANCE:	initTable(q_table, q_table_PS04_chrom_values);	break;
			}
			break;

		// Photoshop tables
		case PS08:
			switch (channel){
				case LUMINANCE:		initTable(q_table, q_table_PS08_lum_values);	break;
				case CHROMINANCE:	initTable(q_table, q_table_PS08_chrom_values);	break;
			}
			break;

		// Photoshop tables
		case PS12:
			switch (channel){
				case LUMINANCE:		initTable(q_table, q_table_PS12_lum_values);	break;
				case CHROMINANCE:	initTable(q_table, q_table_PS12_chrom_values);	break;
			}
			break;

		// example tables
		case EXAMPLE:
			switch (channel){
				case LUMINANCE:		initTable(q_table, q_table_example_values);	break;
				case CHROMINANCE:	initTable(q_table, q_table_example_values);	break;
			}
			break;

		// compression by 1
		case NOCOMPRESSION:
			switch (channel){
				case LUMINANCE:		initTable(q_table, q_table_no_compression);	break;
				case CHROMINANCE:	initTable(q_table, q_table_no_compression);	break;
			}
			break;
	}
	return q_table;
}

matrix<int> Quantization::quantize(matrix<float> &m, Q_Table tbl, Q_Channel channel){
	matrix<int> q_table(8, 8);

	q_table = getQTable(tbl, channel);

	matrix<int> r(m.size1(), m.size2());
	// quantize matrix
	for (unsigned int x = 0; x <= m.size1() - 8; x += 8)
	for (unsigned int y = 0; y <= m.size2() - 8; y += 8)	{
		matrix_range<matrix<float>> mr(m, range(x, x + 8), range(y, y + 8));
		matrix_range<matrix<int>>  rmr(r, range(x, x + 8), range(y, y + 8));
		
		for (int j = 0; j < 8; j++)
		for (int i = 0; i < 8; i++){
			rmr(j, i) = (int)round(mr(i, j) / q_table(i, j)); // swapping j and i on purpose!
		}
	}

	return r;
}

Quantization::Q_Table Quantization::stringToEnum(std::string q){
	if (q == "ITUT")
		return Q_Table::ITUT;
	else if (q == "Photoshop 00")
		return Q_Table::PS00;
	else if (q == "Photoshop 04")
		return Q_Table::PS04;
	else if (q == "Photoshop 08")
		return Q_Table::PS08;
	else if (q == "Photoshop 12")
		return Q_Table::PS12;
	else if (q == "Example")
		return Q_Table::EXAMPLE;
	else if (q == "MinimalCompression")
		return Q_Table::NOCOMPRESSION;
	else
		return Q_Table::NOCOMPRESSION;
}

std::string Quantization::Q_tableToString(Q_Table tbl){
	std::string s;
	Q_Channel channel = LUMINANCE;
	matrix<int> m = getQTable(tbl, channel);
	s += "Luminance:";
	s += '\n';
	char number[10];
	for (int y = 0; y < 8; y++){
		for (int x = 0; x < 8; x++){
			sprintf_s(number, "%d", m(x, y));
			s += number;
			s += "\t ";
		}
		s += '\n';
	}

	s += '\n';

	channel = CHROMINANCE;
	m = getQTable(tbl, channel);
	s += "Chrominance:";
	s += '\n';

	for (int y = 0; y < 8; y++){
		for (int x = 0; x < 8; x++){
			sprintf_s(number, "%d", m(x, y));
			s += number;
			s += "\t ";
		}
		s += '\n';
	}
	return s;
}