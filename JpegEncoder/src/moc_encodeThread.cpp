/****************************************************************************
** Meta object code from reading C++ file 'encodeThread.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../header/encodeThread.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'encodeThread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_encodeThread_t {
    QByteArrayData data[8];
    char stringdata[106];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_encodeThread_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_encodeThread_t qt_meta_stringdata_encodeThread = {
    {
QT_MOC_LITERAL(0, 0, 12),
QT_MOC_LITERAL(1, 13, 21),
QT_MOC_LITERAL(2, 35, 0),
QT_MOC_LITERAL(3, 36, 10),
QT_MOC_LITERAL(4, 47, 15),
QT_MOC_LITERAL(5, 63, 20),
QT_MOC_LITERAL(6, 84, 1),
QT_MOC_LITERAL(7, 86, 18)
    },
    "encodeThread\0signal_sendPercentage\0\0"
    "percentage\0signal_encoding\0"
    "encodeThread::result\0r\0slot_cancelLoading\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_encodeThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x05,
       4,    1,   32,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
       7,    0,   35,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, 0x80000000 | 5,    6,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void encodeThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        encodeThread *_t = static_cast<encodeThread *>(_o);
        switch (_id) {
        case 0: _t->signal_sendPercentage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->signal_encoding((*reinterpret_cast< encodeThread::result(*)>(_a[1]))); break;
        case 2: _t->slot_cancelLoading(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (encodeThread::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&encodeThread::signal_sendPercentage)) {
                *result = 0;
            }
        }
        {
            typedef void (encodeThread::*_t)(encodeThread::result );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&encodeThread::signal_encoding)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject encodeThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_encodeThread.data,
      qt_meta_data_encodeThread,  qt_static_metacall, 0, 0}
};


const QMetaObject *encodeThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *encodeThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_encodeThread.stringdata))
        return static_cast<void*>(const_cast< encodeThread*>(this));
    return QThread::qt_metacast(_clname);
}

int encodeThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void encodeThread::signal_sendPercentage(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void encodeThread::signal_encoding(encodeThread::result _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
