/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include <string>
#pragma once

/** 
 *	444:
 *  xxxx
 *  xxxx
 *
 *	422:
 *	xoxo
 *	xoxo
 * 
 *	411:
 *	xooo
 *	xooo
 *  
 *	420:
 *  xo xo
 *  oo oo
 *  xo xo
 *  oo oo
 *  
 */
enum SUBSAMPLINGSCHEME { _444, _422, _411, _420};

/**
 * @brief	Scheme for subsampling colorchannels
 */
class subSamplingScheme{
public:

	/**
	 * @brief	Creates a subSamplingScheme.
	 * @param	SUBSAMPLINGSCHEME	scheme for subsamlping
	 * @param	bool	averaging values (yes/no)
	 */
	subSamplingScheme(SUBSAMPLINGSCHEME sss = _444, bool averaging = false);

	/**
	 * @brief	Gets the scheme out of a string. 
	 *			"444" would be SUBSAMPLINGSCHEME::_444
	 * @param	string				text that gets converted
	 * @return	SUBSAMPLINGSCHEME	scheme for subsampling
	 */
	static const SUBSAMPLINGSCHEME stringToEnum(std::string String);

	/** division factors, horizontal and vertical*/
	unsigned int div_hori, div_vert;

	/** subsampling with averaging yes/no */
	bool averaging;

	/** subsampling scheme */
	SUBSAMPLINGSCHEME scheme;

	

    
};