/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */
#pragma once

#include "picture.h"

#include <QtCore/QThread>
#include <string>
#include <boost/optional/optional.hpp>

class ppmFileThread : public QThread{
	Q_OBJECT
public:
	/** enum for the result */
	enum result{ FINISHED, CANCELED, FAILED };

public:
	/**
	 * @brief	Creates a ppmFileThread and sets the file that will be read.
	 * @param	std::string		path of file being read.
	 */
	ppmFileThread(std::string file = "", int stepsize = 16) : file(file), stepsize(stepsize){};

	/**
	 * @brief	main function of the thread.
	 *			Executes the reading of the ppm file.
	 */
	void run();

	/**
	 * @brief	Gets the picture that was read.
	 */
	picture getPicture(){ return pic; }

	/**
	 * @brief	Reads the file specified into the picture. 
	 * @param	string			path of file being read.
	 * @param	picture			picture that gets set while reading.
	 * @param	unsigned int	stepsize that determines the size of the blocks inside the picture.
	 */
	ppmFileThread::result readFileBytes(std::string file, picture &pic, unsigned int stepsize);

	/**
	 * @brief	Small function to ignore seperators. Slow but easier to read/understand.
	 */
	static void ignoreSeperators(std::ifstream* input);

	/**
	 * @brief	Small function to read in an integer.  Slow but easier to read/understand.
	 */
	static int readInt(std::ifstream* input);

	static void ignoreComments(std::ifstream *input);

public slots:
	/**
	 * @brief	Slot for canceling the loading of a picture.
	 */
	void slot_cancelLoading();

signals:

	/**
	 * @brief	signals the finished percentage of reading the file to the main_gui.
	 */
    void signal_sendPercentage(int percentage);

	/**
	 * @brief	signals the result of reading the file to the main_gui.
	 */
	void signal_readingPPM(ppmFileThread::result r);

private:
	std::string file;
	picture pic;
	bool canceled;
	int stepsize;
};