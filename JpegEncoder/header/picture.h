﻿/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */
#pragma once
#pragma warning( disable : 4996 )

#include <boost\numeric\ublas\matrix.hpp>

#include "subSamplingScheme.h"

using namespace boost::numeric::ublas;

typedef unsigned char BYTE;

/**
 * 
 */
class picture{


public:
	enum COLORSPACE{ RGB, YCBCR };
	
    /**
     * @brief constructor
     * @param   unsigned int    width
     * @param   unsigned int    height
     * @param   unsigned int    stepsize
     */
    picture(const unsigned int width = 1, const unsigned int height = 1);

    unsigned int getWidth();
    unsigned int getHeight();
	
	/**
	 * @brief	Gets the value of the red channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @return	BYTE	color value 0-255 of red
	 */
    BYTE getR(const unsigned int x, const unsigned int y);

	/**
	 * @brief	Gets the value of the green channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @return	BYTE	color value 0-255 of green
	 */
    BYTE getG(const unsigned int x, const unsigned int y);

	/**
	 * @brief	Gets the value of the blue channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @return	BYTE	color value 0-255 of blue
	 */
    BYTE getB(const unsigned int x, const unsigned int y);

	/**
	 * @brief	Gets the value of the Y channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @return	BYTE	value 0-255 of Y
	 */
	int getY(const unsigned int x, const unsigned int y);

	/**
	 * @brief	Gets the value of the Cb channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @return	BYTE	value 0-255 of Cb
	 */
	int getCb(const unsigned int x, const unsigned int y);

	/**
	 * @brief	Gets the value of the Cr channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @return	BYTE	value 0-255 of Cr
	 */
	int getCr(const unsigned int x, const unsigned int y);

	/**
	 * @brief	Sets the value of the red (or Y) channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @param	unsigned int	value 0-255 of red/y
	 */
    void setR_Y(const  int x, const  int y, const  int value);

	/**
	 * @brief	Sets the value of the green (or Cb) channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @param	unsigned int	value 0-255 of green/cb
	 */
    void setG_Cb(const  int x, const  int y, const  int value);

	/**
	 * @brief	Sets the value of the blue (or Cr) channel at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @param	unsigned int	value 0-255 of blue/cr
	 */
    void setB_Cr(const  int x, const  int y, const  int value);

	/**
	 * @brief	Sets the color of all three channels at its position x, y.
	 * @param	unsigned int	coordinate x
	 * @param	unsigned int	coordinate y
	 * @param	unsigned int	value 0-255 of red/y
	 * @param	unsigned int	value 0-255 of green/cb
	 * @param	unsigned int	value 0-255 of blue/cr
	 */
	void setColor(const int x, const int y, const int r, const int g, const int b);

	/**
	 * @brief	Gets the current colorspace the channels are in.
	 */
	COLORSPACE getColorSpace();

	/**
	 * @brief	Changes the colorspace to YCbCr.
	 *			The values of the channels are calculated 
	 *			and overwritten according to the matrices for JPG-converting.
	 */
	void changeColorSpaceToYCbCr();

	/**
	 * @brief	Changes the color values and the subsampling-scheme (see subSamplingScheme.h)
	 *			Only the relevant positions according to the scheme are overwritten.
	 */
	void subsampleColorChannels(subSamplingScheme sss);


	matrix<int> getMatrixY();
	matrix<int> getMatrixCb();
	matrix<int> getMatrixCr();

	subSamplingScheme getSubSamplingScheme();
	
private:
	/** color channels of the picture. At first rgb, after changing colorspace ycbcr*/
	matrix<int> r_y, g_cb, b_cr;

	/** matrix for converting from RGB to YCBCR */
	matrix<float> mRGBtoYCbCr;

	/** matrix for converting from YCBCR to RGB */
	matrix<float> mYCbCrtoRGB;

	/** width and height of the picture */
    unsigned int width, height;

	/** scheme of how to subsample the Cb and Cr channel*/
	subSamplingScheme subsample_scheme;

	/** colorspace the picture is in (RGB or YCBCR) */
	COLORSPACE colorspace;

	/**
	 * @brief	Calculates the new values of the Cb channel according to the subSamplingScheme.
	 * @param	subSamplingScheme	The scheme of how to subsample the channel
	 */
	void subsampleCb(subSamplingScheme sss);

	/**
	 * @brief	Calculates the new values of the Cr channel according to the subSamplingScheme.
	 * @param	subSamplingScheme	The scheme of how to subsample the channel
	 */
	void subsampleCr(subSamplingScheme sss);
};