/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

#pragma once

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/math/special_functions/round.hpp>

using namespace boost::numeric::ublas;
using namespace boost::math;

class Quantization{

public:
	enum Q_Channel { LUMINANCE, CHROMINANCE };
	enum Q_Table { ITUT, PS00, PS04, PS08, PS12, EXAMPLE, NOCOMPRESSION };

	static Q_Table stringToEnum(std::string q);

	static matrix<int> getQTable(Q_Table tbl = Q_Table::ITUT, Q_Channel channel = Q_Channel::LUMINANCE);
	static matrix<int> getQTable_ITUT_chrominance();
	static matrix<int> quantize(matrix<float> &m, Q_Table tbl = ITUT, Q_Channel channel = LUMINANCE);

	static std::string Quantization::Q_tableToString(Q_Table tbl);

private:
	static int q_table_itut_lum_values[8][8];
	static int q_table_itut_chrom_values[8][8];
	static int q_table_PS00_lum_values[8][8];
	static int q_table_PS00_chrom_values[8][8];
	static int q_table_PS04_lum_values[8][8];
	static int q_table_PS04_chrom_values[8][8];
	static int q_table_PS08_lum_values[8][8];
	static int q_table_PS08_chrom_values[8][8];
	static int q_table_PS12_lum_values[8][8];
	static int q_table_PS12_chrom_values[8][8];
	static int q_table_example_values[8][8];
	static int q_table_no_compression[8][8];

	static void initTable(matrix<int> &m, int(&values)[8][8]);
};