/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include <iostream>
#include <vector>
#include <cstdint>
#include <algorithm>

#include "picture.h"

#pragma once
#pragma warning( disable : 4996 )

typedef unsigned char BYTE;
typedef unsigned int __w64 bit_size;

class bitstream {
private:
	void alloc_space();
public:
	/** has only 3 bits! */
	unsigned int bits_used : 3;

	bitstream() : bits_used(0) { }
	std::vector<BYTE> storage;

	/**
	* @brief	Pushes one bit to the stream and increments bits_used (has range [0, 7])
	* @parameter	bool	bit
	*/
    void push_bit(bool bit);

	/**
	* @brief	Pushes a byte in form of an unsigned char to the bitstream
	* @parameter	unsigned char	byte
	*/
    void push_byte(unsigned char byte);
	
	/**
	* @brief	Appends a whole stream to the caller
	* @parameter	bitstream	stream
	*/
	void push_stream(bitstream bs);

	/**
	* @brief	Appends a whole stream to the caller.
	*			Also searches for 0xFF bytes and appends extra 0x00 byte! (error handling jpeg)
	* @parameter	bitstream	stream
	*/
	void push_sos_datastream(bitstream bs);

	/**
	* @brief	Returns an array of the BYTE sequence
	*/
	BYTE *get_array();

	/**
	* @brief	Returns the count of bits or "size" of the stream)
	* @return	bit_size (int __w64)	size
	*/
    bit_size size() const;

	/**
	* @brief	Overrides operator[]. Returns the bit on position pos.
	* @param	bit_size	position of bit
	* @return	bool	value of bit
	*/
    bool operator[](bit_size pos) const;

	/**
	* @brief	Returns a BYTE at byte_pos.
	* @param	unsigned int	byte position
	* @return	BYTE	value of byte
	*/
	BYTE getByteAt(unsigned int byte_pos);

	/**
	* @brief	Writes bitstream to file specified.
	* @param	const char*		relative path to file
	*/
	void writeToFile(const char* file);
	
	/**
	* @brief	Reads bitstream from file specified.
	* @param	const char*		relative path to file
	*/
	void readFromFile(char* file);

	/**
	* @brief	Fills the started byte with 1s until it has 8 bits.
	*			Example: 0110 11  -> 0110 1111
	* @param	bool		bit value to fill with
	*/
	void fillByte(bool bit = true);
};