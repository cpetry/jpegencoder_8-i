/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/
#pragma once



#include <boost\numeric\ublas\matrix.hpp>

using namespace boost::numeric::ublas;

class iDCT{
public:
	// insert 8x8 matrix -> get back 8x8 matrix :)
	static void transform(matrix<float> &Y);
};