/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

#pragma once

#define _USE_MATH_DEFINES 
#include <math.h>

#include <boost\numeric\ublas\matrix.hpp>

using namespace boost::numeric::ublas;

class DCT_Variante2
{
public:
	/**
	* @brief	Transforms the given matrix into a DCT matrix.
	* @param	matrix<float> &m
	*/
	static void transform(matrix<float> &m);

private:
	static matrix<float> matrix_A;
	static float c0_0;
	static float c0_1;
	static float sqrt2N;
	static float pi2N;
	
	static float matrix_values[8][8];
	static matrix<float> z;
};

