/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */
#pragma once

#include "bitstream.h"
#include "picture.h"
#include "huffmanCode.h"
#include <string>

#include "encoding.h"
#include "Quantization.h"

class jpegSegments {
class encodeThread;

public:
	/**
	* @brief	Writes all the segments needed for a jpeg file and saves the file to the filename specified (relative path)
	* @param	picture		picture that should be encoded
	* @param	std::string		filename for the jpeg file that gets saved
	* @param	Quantizazion::Q_Table		quantization table
	*/
	void writeSegments(picture &pic, std::string filename, Quantization::Q_Table q_table = Quantization::Q_Table::PS00);

	/** Test functions */
	bitstream* const getBitstream(){ return &bs; }
	void SOI_test(){ SOI(); };
	void EOI_test(){ EOI(); };
	void APP0_test(){ APP0(); };
	void SOF0_test(picture &pic){ SOF0(pic); };
	void DHT_test(std::vector<HuffMap> &trees){ DHT(trees); }
	void SOS_test(std::vector<bitstream> &y_bs, std::vector<bitstream> &cb_bs, std::vector<bitstream> &cr_bs, picture &pic){ SOS(y_bs, cb_bs, cr_bs, pic); }
	void DQT_test(Quantization::Q_Table q_table){ DQT(q_table); }

private:
    bitstream bs;
    
	/**
	* @brief	Writes the SOI Segment.
	*/
    void SOI();
    
	/**
	* @brief	Writes the EOI Segment.
	*/
	void EOI();

	/**
	* @brief	Writes the APP0 Segment.
	*/
    void APP0();

	/**
	* @brief	Writes the SOF0 Segment.
				Needs the width, height and subsampling values (inside picture reference)
	* @param	picture		picture that should be encoded to jpg.
	*/
	void SOF0(picture &pic);

	/**
	* @brief	Writes the DHT Segment.
				Needs all the huffman trees (DC/AC of Y and CBCR channels).
	* @param	picture		picture that should be encoded to jpg.
	*/
	void DHT(std::vector<HuffMap> &trees);
	
	/**
	* @brief	Writes the SOS Segment.
				Needs all bitstreams created for y, cb and cr channels.
				Width, height and subsampling scheme is needed too.
	* @param	std::vector<bitstream>		y channel bitstreams
	* @param	std::vector<bitstream>		cb and cr channel bitstreams
	* @param	picture		picture that should be encoded to jpg.
	*/
	void SOS(std::vector<bitstream> &y_bs, std::vector<bitstream> &cb_bs, std::vector<bitstream> &cr_bs, picture &pic);

	/**
	* @brief	Writes the DQT Segment.
	*			Needs the quantization table that should be used.
	* @param	Quantizazion::Q_Table		quantization table
	*/
	void DQT(Quantization::Q_Table q_table);

	/**
	* @brief	Creates a 4 bit vector of a number.
	* @param	int		number
	* @return	std::vector<bool>	bitCode
	*/
	static std::vector<bool> create4BitVector(int number);
};