/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */
#pragma once

#include "picture.h"

#include <QtCore/QThread>
#include <string>
#include <boost/optional/optional.hpp>

#include "Quantization.h"

class encodeThread : public QThread{
	Q_OBJECT
public:
	/** enum for the result */
	enum result{ FINISHED, CANCELED, FAILED };

	/**
	 * @brief	Creates a encodeThread and sets the file that will be read.
	 * @param	std::string		path of file being read.
	 */
    encodeThread(picture pic, subSamplingScheme subsampling, Quantization::Q_Table q_table) : pic(pic), subsampling(subsampling), q_table(q_table){};

	/**
	 * @brief	main function of the thread.
	 *			Executes the reading of the ppm file.
	 */
	void run();

	/**
	 * @brief	Gets the picture that was read.
	 */
	picture getPicture(){ return pic; }

public slots:
	/**
	 * @brief	Slot for canceling the loading of a picture.
	 */
	void slot_cancelLoading();

signals:

	/**
	 * @brief	signals the finished percentage of reading the file to the main_gui.
	 */
    void signal_sendPercentage(int percentage);

	/**
	 * @brief	signals the result of reading the file to the main_gui.
	 */
	void signal_encoding(encodeThread::result r);

private:
    subSamplingScheme subsampling;
	Quantization::Q_Table q_table;
	std::string file;
	picture pic;
	bool canceled;
};