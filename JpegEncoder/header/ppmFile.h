/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */
#pragma once

#include "picture.h"

class ppmFile{
public:

	/**
	 * @brief	Reads the file specified into the picture. 
	 * @param	string			path of file being read.
	 * @param	picture			picture that gets set while reading.
	 * @param	unsigned int	stepsize that determines the size of the blocks inside the picture.
	 */
	static void ppmFile::readFileBytes(std::string file, picture &pic, unsigned int stepsize);

	/**
	 * @brief	Writes the file specified into the picture. 
	 * @param	string			path of file being read.
	 * @param	picture			picture that gets set while reading.
	 */
    static void ppmFile::writeFileBytes(std::string file, picture &pic);

	/**
	 * @brief	Small function to ignore seperators. Slow but easier to read/understand.
	 */
	static void ignoreSeperators(std::ifstream* input);

	/**
	 * @brief	Small function to read in an integer.  Slow but easier to read/understand.
	 */
	static int readInt(std::ifstream* input);

	static void ignoreComments(std::ifstream *input);
};