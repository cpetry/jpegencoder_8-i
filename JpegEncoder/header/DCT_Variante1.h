/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/
#define _USE_MATH_DEFINES 
#include <math.h>

#include <boost\numeric\ublas\matrix.hpp>

using namespace boost::numeric::ublas;

typedef unsigned char BYTE;

class DCT_Variante1{

public:
	/**
	* @brief	Transforms the given matrix into a DCT matrix.
	* @param	matrix<float> &m
	*/
	static void transform(matrix<float> &m);

private:
	static float One_Sqrt;
	static float pi_16;
	static float c_i;
	static float c_j;
	static matrix<float> z;
	static float value;
	static float i_pi_16;
	static float j_pi_16;
};