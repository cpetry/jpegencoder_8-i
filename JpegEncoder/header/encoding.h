/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

#pragma once

#include <boost/numeric/ublas/matrix.hpp>

#include "bitstream.h"
#include "huffmanCode.h"
#include "Quantization.h"

using namespace boost::numeric::ublas;

class encoding{
public:

	/**
	* @brief	Creates the DC delta values of the matrix given.
	*			The difference between the first value of every 8x8 block and the value before (dc -1 = 0)
	* @param	const matrix<int>	matrix of whole picture
	* @return	std::vector<int>	vector of dc values
	*/
	static std::vector<int> createDCdeltas(const matrix<int> &m, SUBSAMPLINGSCHEME sss = _444);
	
	/**
	* @brief	Creates the AC pairs of the matrix given.
	*			For every block a list of pairs is being created. The pair contains the number of 0s and the value.
	* @param	const matrix<int>	matrix of whole picture
	* @return	std::vector<std::vector<std::pair<int, int>		List of vectors of pairs
	*/
	static std::vector<std::vector<std::pair<int, int>>> encoding::createACpairs(matrix<int> &m, SUBSAMPLINGSCHEME sss = _444);

	/**
	* @brief	Creates a vector of int values of a matrix.
	*			The vector contains the values in a zigzag form specified by the jpeg specification.
	*/
	static std::vector<int> zigzagMatrix(const matrix<int> &m);

	/**
	* @brief	Creates the Zero number pairs of the given vector of DCT values.
	*			A list of pairs is being created where the pairs contain the number of 0s and the values.
	* @param	std::vector <int>	dct coefficients
	* @return	std::vector<std::vector<std::pair<int, int>		List of vectors of pairs
	*/
	static std::vector<std::pair<int, int>> createZeroNumberPairs(std::vector<int> &coeffs);

	/**
	* @brief	Creates a blockstream of the dc deltas, ac deltas.
	*			The huffman trees are needed to encode the category of dc differences and ac values.
	* @param	const std::vector<int>	dc deltas
	* @param	const std::vector<std::vector<std::pair<int,int>>>	ac pairs for each block
	* @param	huffMap		dc huffmap for category of differences
	* @param	huffMap		ac huffmap for category of values
	*/
	static std::vector<bitstream> encoding::createBlockstreams(const std::vector<int> &dc_deltas, const std::vector<std::vector<std::pair<int, int>>> &ac_pairs, HuffMap &hm_dc, HuffMap &hm_ac);

	/**
	* @brief	Returns the category specified by jpeg specification of a given number (negative and positive)
	* @param	const int	number
	* @param	int		category of number
	*/
	static int getCategory(const int &number);

	/**
	* @brief	Returns the bitcode specified by jpeg specification of a given number (category).
	*			The std::vector<bool> construct is used because it is faster as bitstream implementation. 
	* @param	const int	number
	* @param	std::vector<int>	bitcode of number
	*/
	static std::vector<bool> getBitCode(const int &number);

	/**
	* @brief	Returns the number representation of a bitcode.
	* @param	const std::vector<bool>		bitcode
	* @return	int		number
	*/
	static int getIntegerOfBitCode(const std::vector<bool> &bits);

	/**
	* @brief	Creates bitstreams and the trees for the y channel.
				The given matrix is the whole y channel of the picture.
				The Q_Table specifies the Quantization table that should be used.
				Both, the dc and the ac huffman tree are created in the process.
	* @param	const matrix<int>	matrix of y channel.
	* @param	Quantization::Q_Table		Quantization table that should be used.
	* @param	Huffmap		dc huffmap of dc difference categorys
	* @param	Huffmap		ac huffmap of ac values
	* @param	std::vector<bitstream>	streams of every block of the y channel
	*/
	static void createYBitstreamsAndTrees(const matrix<int> &m, SUBSAMPLINGSCHEME sss, Quantization::Q_Table q_table, HuffMap &dc_tree, HuffMap &actree, std::vector<bitstream> &y_blocks);
	
	/**
	* @brief	Creates bitstreams and the trees for the cb,cr channels.
	The given matrices are the whole cb,cr channels of the picture.
	The Q_Table specifies the Quantization table that should be used.
	Both, the dc and the ac huffman tree are created in the process.
	* @param	const matrix<int>	matrix of y channel.
	* @param	Quantization::Q_Table		Quantization table that should be used.
	* @param	Huffmap		dc huffmap of dc difference categorys
	* @param	Huffmap		ac huffmap of ac values
	* @param	std::vector<bitstream>	streams of every block of the cb channel
	* @param	std::vector<bitstream>	streams of every block of the cr channel
	*/
	static void createCbCrBitstreamsAndTrees(const matrix<int> &cb, const matrix<int> &cr, Quantization::Q_Table q_table, HuffMap &dc_cbcr_tree, HuffMap &ac_cbcr_tree, std::vector<bitstream> &cb_blocks, std::vector<bitstream> &cr_blocks);

private:
	static std::vector<std::pair<int, int>> createCoordList(const matrix<int> &m, SUBSAMPLINGSCHEME sss);
};