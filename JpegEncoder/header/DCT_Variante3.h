/**
* @Copyright   2013
* @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
* @FHWS
*/

#pragma once

#define _USE_MATH_DEFINES 
#include <math.h>

#include <boost\numeric\ublas\matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp> // matrix_range

using namespace boost::numeric::ublas;

typedef unsigned char BYTE;

static const float pi_2n = (float)M_PI / 16.0f;
static const float c1 = static_cast<const float>(cos(1.0f * pi_2n));
static const float c2 = static_cast<const float>(cos(2.0f * pi_2n));
static const float c3 = static_cast<const float>(cos(3.0f * pi_2n));
static const float c4 = static_cast<const float>(cos(4.0f * pi_2n));
static const float c5 = static_cast<const float>(cos(5.0f * pi_2n));
static const float c6 = static_cast<const float>(cos(6.0f * pi_2n));
static const float c7 = static_cast<const float>(cos(7.0f * pi_2n));

static const float s0 = 1.0f / (2.0f * sqrt(2.0f));
static const float s1 = 1.0f / (4.0f * c1);
static const float s2 = 1.0f / (4.0f * c2);
static const float s3 = 1.0f / (4.0f * c3);
static const float s4 = 1.0f / (4.0f * c4);
static const float s5 = 1.0f / (4.0f * c5);
static const float s6 = 1.0f / (4.0f * c6);
static const float s7 = 1.0f / (4.0f * c7);

static const float a1 = c4;
static const float a2 = c2 - c6;
static const float a3 = c4;
static const float a4 = c6 + c2;
static const float a5 = c6;

class DCT_Variante3{

public:
	/**
	* @brief	Transforms the given matrix into a DCT matrix.
	* @param	matrix<float> &m
	*/
	static void transform(matrix<float> &m);
	
	/**
	* @brief	Transposes the matrix_range
	* @param	matrix_range<matrix<float>> &mr
	*/
	static void transpose(matrix_range<matrix<float>> &mr);
};