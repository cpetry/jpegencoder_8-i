/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include <string>

/**
 * @brief	Encoderclass for encoding a picture to JPG
 */
class Encoder{
public:
	/**
	 * @brief encodes a <file> with subsampling parameters.
	 * @param	string	path to file that gets encoded.
	 * @param	string	scheme for subsampling.
	 * @param	bool	averageing subsample pixels (yes/no)
	 */
	static void encode(std::string file, std::string subsampling, bool averaging);
	static void perfTest();
};