/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include "bitstream.h"
#include "picture.h"
#include <string>
#include <map>

#pragma once

typedef std::vector<bool> HuffVect;
typedef std::map<char, HuffVect> HuffMap;
typedef std::vector<std::pair<int, std::vector<char>>> HuffmanPairVector;

class InternalNode;

/**
 * @class	Node
 * @brief	Defines a Node of a huffman tree.
 *			Has two static methods to create a Tree and to generate the code off an existing tree
 */
class Node{

public:
	/**
	 * @brief	Creates a tree based on an array of frequencies.
	 *			The position represents the codevalue.  (Example: "frequencies[0] == 5" =>  0 : codevalue)
	 *			The value represents the frequency.		(Example: "frequencies[0] == 5" =>  5 : frequency)
	 */
	static Node* createTree(std::map<int, int> frequencies, int value_range = 256);

	/**
	 * @brief	Generates the code recursively starting at "prefix".
	 *			The dynamic_cast tests if the Node is a Leaf or an InternalNode
	 *			and handles them accordingly.
	 */
	static void generate(Node* node, const HuffVect& prefix, HuffMap& code);

	/**
	 * @brief	returns the frequency.
	 */
	int getFrequency();

	void setFrequency(int f);

	const int getHeight();
	
	const int getNumberOfElements();

	/**
	 * @brief	virtual destructor needed for dynamic_cast (polymorphism)
	 */
    virtual ~Node();

	InternalNode* getParent();
	void setParent(InternalNode* parent);


protected:
	/**
	 * @brief	constructor with frequency value "f"
	 */
	Node(int f = 0);
    

	/** frequency */
   	int f;
	InternalNode* parent;
};

/**
 * @class	InternalNode
 * @brief	Can return left and right child node.
 */
class InternalNode : public Node{
public:
	Node* getLeft(); 
	Node* getRight();
	void setRight(Node* new_right){ this->right = new_right; if (right) right->setParent(this); }
	void setLeft(Node* new_left){ this->left = new_left; if (left) left->setParent(this); }

	InternalNode(Node* i0, Node* i1);
	~InternalNode();

private:
	Node* left;
	Node* right;
};

/**
 * @class	Leaf
 * @brief	One end of the tree.
 *			Has the codevalue and its frequency.
 */
class Leaf : public Node{
private:
	const int i;

public:
	Leaf(int f, int i);
    const int getValue() const;
};

/**
 * @brief	A Compare function needed to create tree (priority_queue)
 */
struct NodeCompare{
	bool operator()(Node* leftNode, Node* rightNode) const;
};

struct pairCompare{
	bool operator()(const std::pair<int, Node*>& firstElem, const std::pair<int, Node*>& secondElem);	
};

//bool pairCompare(const std::pair<int, Node*>& firstElem, const std::pair<int, Node*>& secondElem);



class huffmanCode{

public:
    /**
	 * Exercise 3b: trees grow right side
	 */
	static void growRight(Node* root);

	static void growRightPert(Node* root);
	static std::vector<Node*> createChildList(std::vector<Node*> parent_list);
	static void searchAndSwapWithInternalNode(std::vector<Node*> parent_list, std::vector<Node*> &child_list, int pos);
	/**
	 * Exercise 3c: swap
	 */
	static void eleminateOneStar(Node* root);
	/**
	 * Exercise 3d: length limitation
	 * @brief Creates two lists.
	 *		A delete list and a free space list.
	 *		The delete list has all the leaves that have to be cut out of the tree.
	 *		The free space list has all positions where still some space is free for other leaves.
	 *		When Attaching the leaves of the delete list to the free space list, even more free space could be
	 *		inserted. The free space list is ordered by height descending (because all free spaces at the bottom should
	 *		be filled first)
	 */
	static void lengthLimit(Node* root, int max_depth);

	/**
	 *	@brief	Creates a huffman code on whatever values you have (char*, int*, etc).
	 *			Length of the array has to be specified!
	 *  @param	int		length of the array "values"
	 */
	template<typename T>
	static void generateFrequencies(T values, int length, std::map<int, int> &frequencies){
		if (length == 0)
			return;

		for (int i = 0; i < length; i++){
			frequencies[static_cast<int>(values[i])]++;
		}
	}

	static std::map<int, int> generateFrequencies(std::vector<int> values){
		std::map<int, int> frequencies;

		for (unsigned int i = 0; i < values.size(); i++){
			frequencies[values[i]]++;
		}
		return frequencies;
	}

	static void generateFrequencies(matrix<int> &r, std::map<int, int> &freqAC, std::map<int, int> &freqDC){
		int dc_old = 0;
		for (unsigned int y = 0; y < r.size1(); y++){
			for (unsigned int x = 0; x < r.size2(); x++){
				if (x % 8 != 0 || y % 8 != 0)
					freqAC[r(x, y)]++;
				else{
					freqDC[dc_old - r(x, y)]++;
					dc_old = r(x, y);
				}

			}
		}
	}

	static HuffMap createHuffmanCodes(std::map<int, int> frequencies, bool grow_right = true, bool eleminate_one_star = true, int length_limit = 0);

	/**
	 * @brief	Creates a vector of an <int, char vector> pair.
	 *			The int is the value of the code, the char vector the bit chars ('0','1'). 
	 */
	static HuffmanPairVector createHuffmanPair(HuffMap h);

};
