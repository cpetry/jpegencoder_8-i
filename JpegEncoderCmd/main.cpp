/**
 * @Copyright   2013
 * @Author  Matthias Karl, Hufnagel Stefan, Petry Christian
 * @FHWS
 */

#include <iostream>

#include "Encoder.h"

int main( int argc, const char* argv[] ){
	// Performance test
	if (argc == 1){
		Encoder::perfTest();
	}
    else if (argc > 1){
        std::string file(argv[1]);
        std::string subsampling = "444";
        bool averaging = false;
		bool performance_test = false;
		if (argc > 2)
			subsampling = std::string(argv[2]);
        if(argc > 3 && std::string(argv[3]) == "a")
            averaging = true;
		Encoder::encode(file, subsampling, averaging);
    }
	// standard pic encode
    else{
        Encoder::encode("../../_resources/brian_kernighan512.ppm", "444", false);
    }

	std::cout << "Press Enter to exit!";
	getchar();

    return 0;
}